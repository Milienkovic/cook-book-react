import React, {Fragment, useEffect} from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import Home from "./ui/containers/HomePage/Home";
import AdminPanel from "./ui/containers/AdminPanel/AdminPanel";
import CategoriesBoard from "./ui/containers/CategoryBoard/CategoriesBoard";
import EditCategory from "./ui/components/category/EditCategory";
import CreateCategory from "./ui/components/category/CreateCategory";
import AppUrls from "./utils/urls/appUrls";
import Login from "./ui/components/auth/login/Login";
import Registration from "./ui/components/auth/registration/Registration";
import SendPasswordToken from "./ui/components/auth/password/SendPasswordToken";
import VerifyAccount from "./ui/components/auth/registration/VerifyAccount";
import {authSuccess, exchangeAuthToken, exchangePassPrivilegeAuthToken, logout} from "./store/actions/auth/authActions";
import NewVerificationToken from "./ui/components/auth/registration/NewVerificationToken";
import UpdatePassword from "./ui/components/auth/password/UpdatePassword";
import {connect} from "react-redux";
import Tokens from "./utils/tokens";
import AdminRoute from "./ui/components/commons/routes/AdminRoute";
import AuthenticatedRoute from "./ui/components/commons/routes/AuthenticatedRoute";
import ConfirmPasswordReset from "./ui/components/auth/password/ConfirmPasswordReset";
import CategoryPreview from "./ui/components/category/CategoryPreview/CategoryPreview";
import UserBoard from "./ui/containers/UserBoard/UserBoard";
import PersonalData from "./ui/components/user/current/PersonalData";
import MainNavigation from "./ui/components/commons/navigation/MainNavigation";

import './App.css';
import UserRecipes from "./ui/containers/UserRecipesPage/UserRecipes";
import CategoryRecipes from "./ui/components/recipes/CategoryRecipes/CategoryRecipes";
import EditRecipe from "./ui/components/recipes/EditRecipe/EditRecipe";
import RecipeMultipageForm from "./ui/components/recipes/RecipeMultipageForm";
import Ingredients from "./ui/components/ingredients/Ingredients";
import Steps from "./ui/components/steps/Steps";
import RecipePage from "./ui/containers/RecipePage/RecipePage";

const App = props => {
    const token = Tokens.getAuthToken();

    const _getNewAuthToken = (decodedToken) => {
        const refreshToken = Tokens.getRefreshToken();
        if (refreshToken) {
            if (decodedToken.roles.includes("CHANGE_PASSWORD_PRIVILEGE")) {
                props.onExchangePassPrivilegeAuthToken(refreshToken);
            } else {
                props.onExchangeAuthToken(refreshToken);
            }
        } else {
            //user is logged in until auth token is valid, even without refresh token present
            props.onLogout();
        }
    };

    const _updateAuthOnRefresh = () => {
        const token = Tokens.getAuthToken();
        const decodedToken = Tokens.getDecodedAuthToken();
        if (Tokens.isAuthTokenExpired()) {
            _getNewAuthToken(decodedToken)
        } else {
            props.onRefresh(token, decodedToken.exp, decodedToken.roles);
        }
    };

    useEffect(() => {
        if (token)
            _updateAuthOnRefresh();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);


    // eslint-disable-next-line no-unused-vars
    const anonymousOnlyRoutes = () => {
        return (
            <Fragment>
                <Route exact path={AppUrls.HOME} component={Home}/>
                <Route exact path={AppUrls.LOGIN} component={Login}/>
                <Route exact path={AppUrls.REGISTRATION} component={Registration}/>
                <Route exact path={AppUrls.REQUEST_RESET_PASSWORD_TOKEN} component={SendPasswordToken}/>
                <Route exact path={AppUrls.CONFIRM_PASSWORD_RESET_REQUEST} component={ConfirmPasswordReset}/>
                <Route exact path={AppUrls.REQUEST_NEW_VERIFICATION_TOKEN} component={NewVerificationToken}/>
                <Route exact path={AppUrls.VERIFY_ACCOUNT} component={VerifyAccount}/>
                <Route exact path={AppUrls.CHANGE_PASSWORD_REQUEST} component={ConfirmPasswordReset}/>
            </Fragment>
        )
    };
    // eslint-disable-next-line no-unused-vars
    const authenticatedOnlyRoutes = () => {
        return (
            <Fragment>
                <AuthenticatedRoute exact path={AppUrls.LOGOUT} component={Login}/>
            </Fragment>
        )
    };
    //eslint-disable-next-line no-unused-vars
    const hasCPRoleOnlyRoutes = () => {
        return (
            <Fragment>
                <Route exact path={AppUrls.UPDATE_PASSWORD_URL} component={UpdatePassword}/>
            </Fragment>
        )
    }
    // eslint-disable-next-line no-unused-vars
    const hasUserRoleOnlyRoutes = () => {
    }
    // eslint-disable-next-line no-unused-vars
    const adminOnlyRoutes = () => {
        return (
            <Fragment>
                <AdminRoute exact path={AppUrls.ADMIN_PANEL} component={AdminPanel}/>
                <AdminRoute exact path={AppUrls.CATEGORIES_BOARD} component={CategoriesBoard}/>
                <AdminRoute exact path={AppUrls.EDIT_CATEGORY} component={EditCategory}/>
                <AdminRoute exact path={AppUrls.CREATE_CATEGORY} component={CreateCategory}/>
            </Fragment>
        )
    };

    return (
        <Fragment>
            <MainNavigation/>
            <main className="App">
                <Switch>
                    <Route exact path={AppUrls.HOME} component={Home}/>
                    <Route exact path={'/test'} component={CategoryPreview}/>
                    <AdminRoute exact path={AppUrls.ADMIN_PANEL} component={AdminPanel}/>
                    <AdminRoute exact path={AppUrls.CATEGORIES_BOARD} component={CategoriesBoard}/>
                    <AdminRoute exact path={AppUrls.EDIT_CATEGORY} component={EditCategory}/>
                    <AdminRoute exact path={AppUrls.CREATE_CATEGORY} component={CreateCategory}/>
                    <AdminRoute exact path={AppUrls.USERS_BOARD} component={UserBoard}/>
                    <Route exact path={AppUrls.REGISTRATION} component={Registration}/>
                    <Route exact path={AppUrls.REQUEST_RESET_PASSWORD_TOKEN} component={SendPasswordToken}/>
                    <Route exact path={AppUrls.CONFIRM_PASSWORD_RESET_REQUEST} component={ConfirmPasswordReset}/>
                    <Route exact path={AppUrls.VERIFY_ACCOUNT} component={VerifyAccount}/>
                    <Route exact path={AppUrls.LOGIN} component={Login}/>
                    <AuthenticatedRoute exact path={AppUrls.LOGOUT} component={Login}/>
                    <Route exact path={AppUrls.REQUEST_NEW_VERIFICATION_TOKEN} component={NewVerificationToken}/>
                    <Route exact path={AppUrls.CHANGE_PASSWORD_REQUEST} component={ConfirmPasswordReset}/>
                    <AuthenticatedRoute exact path={AppUrls.UPDATE_PASSWORD} component={UpdatePassword}/>
                    <AuthenticatedRoute exact path={AppUrls.USER_RECIPES} component={UserRecipes}/>
                    <AuthenticatedRoute exact path={AppUrls.CURRENT_USER} component={PersonalData}/>
                    <AuthenticatedRoute exact path={AppUrls.CREATE_RECIPE} component={RecipeMultipageForm}/>
                    <AuthenticatedRoute exact path={AppUrls.EDIT_RECIPE} component={EditRecipe}/>
                    <AuthenticatedRoute exact path={AppUrls.RECIPE_INGREDIENTS} component={Ingredients}/>
                    <AuthenticatedRoute exact path={AppUrls.RECIPE_STEPS} component={Steps}/>
                    <Route exact path={AppUrls.RECIPE_VIEW} component={RecipePage}/>
                    <Route exact path={AppUrls.CATEGORY_RECIPES} component={CategoryRecipes}/>
                    <Redirect to={AppUrls.HOME}/>
                </Switch>
            </main>
        </Fragment>
    );
}

const mapStateToProps = state => {
    return {
        authenticated: !!state.auth.token,
        authToken: state.auth.token
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onRefresh: (token, expiresIn, roles) => dispatch(authSuccess(token, expiresIn, roles)),
        onExchangeAuthToken: (token) => dispatch(exchangeAuthToken(token)),
        onExchangePassPrivilegeAuthToken: (token) => dispatch(exchangePassPrivilegeAuthToken(token)),
        onLogout: () => dispatch(logout())
    }
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
