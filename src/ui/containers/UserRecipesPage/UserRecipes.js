import React, {Fragment, useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';
import RecipePreview from "../../components/recipes/RecipePreview/RecipePreview";
import {deleteRecipe, fetchUserRecipes} from "../../../store/actions/recipes/recipesActions";
import Spinner from "../../components/commons/spinner/Spinner";
import Svgs from "../../../utils/svgs/svgs";
import Modal from "../../components/commons/modal/Modal";
import Button from "../../components/commons/form/form/input/Button/Button";
import AppUrls from "../../../utils/urls/appUrls";

import './UserRecipes.css';

const UserRecipes = () => {
    const [recipeId, setRecipeId] = useState(null);
    const [displayModal, setDisplayModal] = useState(false);
    const [updating, setUpdating] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();
    const {loading, recipes, isManipulated} = useSelector(state => ({
        loading: state.recipes.loading,
        recipes: state.recipes.mineCollection,
        isManipulated: state.recipes.manipulated
    }));

    useEffect(() => {
        if (!recipes || isManipulated)
            dispatch(fetchUserRecipes());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [recipes, isManipulated]);

    useEffect(() => {
        if (updating && recipeId)
            history.push(AppUrls.EDIT_RECIPE_URL(recipeId));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [updating, recipeId]);

    useEffect(() => {
        if (deleting && recipeId) {
            dispatch(deleteRecipe(recipeId));
        }
    }, [deleting, dispatch, recipeId]);

    const recipeClickedHandler = (recipeId) => {
        setRecipeId(recipeId);
    }

    const deleteRecipeButtonClickedHandler = () => {
        setDisplayModal(true);
    }

    const editRecipeButtonClickedHandler = () => {
        setUpdating(true);
    }

    const deleteRecipeConfirmModalClickHandler = () => {
        setDeleting(true);
        setDisplayModal(false);
    }

    const closeDeleteRecipeModalHandler = () => {
        setDisplayModal(false);
    }

    const modalActions = <Fragment>
        <Button
            danger
            onClick={deleteRecipeConfirmModalClickHandler}
            type={'button'}
        >Delete</Button>
        <Button dark
                type={'button'}
                onClick={closeDeleteRecipeModalHandler}
        >Cancel</Button>
    </Fragment>

    const renderDeleteRecipeModal = () => {
        return (
            <Modal header={"Delete Recipe!"}
                   danger
                   onSubmit={deleteRecipeConfirmModalClickHandler}
                   footer={modalActions}
                   displayModal={displayModal}
                   closeModal={closeDeleteRecipeModalHandler}>
                Are you sure you want to delete this recipe?
            </Modal>
        )
    }

    const actions = <div className={'user--recipes__actions'}>
        <i className={'action--edit'} onClick={editRecipeButtonClickedHandler}>{Svgs.pencilIcon}</i>
        <i className={'action--delete'} onClick={deleteRecipeButtonClickedHandler}>{Svgs.deleteIcon}</i>
    </div>

    const renderRecipes = () => {
        if (loading || !recipes) {
            return <Spinner display={loading}/>
        }

        if (recipes && Object.values(recipes).length === 0) {
            return <span>ADD RECIPES</span>
        }

        return (
            <div className={'user--recipes__container'}>
                <div className={'user--recipes__body'}>
                    {Object.values(recipes).map(recipe => (
                        <div className={'user--recipes__item'} key={recipe.id}>
                            <RecipePreview recipe={recipe}
                                           actions={actions}
                                           enableActions
                                           onRecipeClick={recipeClickedHandler}/>
                        </div>
                    ))}
                </div>
                <div className={'user--recipes__footer'}>

                </div>
            </div>
        );
    }
    return (
        <Fragment>
            {renderRecipes()}
            {renderDeleteRecipeModal()}
        </Fragment>
    );
};

export default UserRecipes;