import React, {useEffect, useRef} from 'react';
import {Link} from "react-router-dom";
import {scrollTo} from "../../../utils/commons";
import AppUrls from "../../../utils/urls/appUrls";

import './AdminPanel.css';

const AdminPanel = () => {
    const panelRef = useRef();

    useEffect(() => {
        scrollTo(panelRef);
    }, []);
    return (
        <div className="admin-panel__container no-select" ref={panelRef}>
            <div className="admin-panel__card">
                <div className="admin-panel__card__header">
                    <h3 className='admin-panel__card__title'>Admin portal</h3>
                </div>
                <div className="admin-panel__card__body">
                    <h4 className="admin-panel__card__text">
                        <Link to={AppUrls.USERS_BOARD} className="nav-link ">Users Board</Link></h4>
                    <h4 className="admin-panel__card__text">
                        <Link to={AppUrls.CATEGORIES_BOARD} className="nav-link ">Categories Board</Link></h4>
                </div>
                <div className="admin-panel__card__footer">
                    <i className=''><i className={'fas fa-info-circle'}> <span>Available admin operations</span></i></i>
                </div>
            </div>
        </div>
    );
};

export default AdminPanel;