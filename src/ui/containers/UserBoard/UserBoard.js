import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchUsers} from "../../../store/actions/users/usersActions";
import SimpleTable from "../../components/commons/table/SimpleTable";
import Spinner from "../../components/commons/spinner/Spinner";
import Dropdown from "../../components/commons/form/form/input/dropdown/Dropdown";
import Pagination from "../../components/commons/pagination/Pagination";
import Paginations from "../../../utils/paginations";

import './UserBoard.css';

const UserBoard = () => {
    const [itemsPerPage, setItemsPerPage] = useState(5);
    const [currentPage, setCurrentPage] = useState(1);
    const dispatch = useDispatch();
    const {users, loading, isManipulated} = useSelector(state => ({
        users: state.users.collection,
        loading: state.users.loading,
        isManipulated: state.users.manipulated
    }));

    useEffect(() => {
        if (!users || (users && isManipulated))
            dispatch(fetchUsers());
    }, [dispatch, isManipulated, users]);

    const setPage = page => {
        setCurrentPage(page);
    }

    const onDropdownChange = event => {
        setItemsPerPage(event.target.value);
    }

    const isBanned = user => {
        if (user.ban.perma) {
            return true;
        }
        return user.ban.bannedUntil > Date.now();
    }

    const renderUserTable = () => {
        const columns = ['First Name', 'Last Name', 'Email', 'Banned'];
        const usersList = Paginations.pageItems(currentPage, itemsPerPage, Object.values(users));

        return (
            <SimpleTable columns={columns}>
                <tbody>
                {usersList.map(user =>
                    <tr key={user.id}>
                        <td data-th={'First Name'}>
                            {user.firstName}
                        </td>
                        <td data-th={'Last Name'}>
                            {user.lastName}
                        </td>
                        <td data-th={'Email'}>
                            {user.email}
                        </td>
                        <td data-th={'Banned'}>
                            {`${isBanned(user)}`}
                        </td>
                    </tr>
                )}
                </tbody>
            </SimpleTable>
        )
    }

    const renderPagination = () => {
        return (
            <div className={'user--board__pagination--items'}>
                <Dropdown
                    dropdowns={[5, 10, 15, 20]}
                    name={'itemsPerPage'}
                    onDropdownChange={onDropdownChange}/>
                <Pagination itemsPerPage={itemsPerPage}
                            itemsNumber={Object.values(users).length}
                            currentPage={currentPage}
                            setCurrentPage={setPage}/>
            </div>
        )
    }

    if (!users || loading) {
        return <Spinner display={loading}/>
    }

    return (
        <div>
            {renderUserTable()}
            {renderPagination()}
        </div>
    );
}

export default UserBoard;