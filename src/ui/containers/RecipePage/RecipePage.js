import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Spinner from "../../components/commons/spinner/Spinner";
import {fetchRecipe, rateRecipe} from "../../../store/actions/recipes/recipesActions";
import {useParams, useHistory} from 'react-router-dom';
import RatingBar from "../../components/commons/RatingBar/RatingBar";
import RateContainer from "../../components/commons/RatingBar/RateContainer";
import AppUrls from "../../../utils/urls/appUrls";

import './RecipePage.css';

const RecipePage = (props) => {
    const recipeId = useParams().recipeId;
    const categoryId = useParams().categoryId;
    const history = useHistory();
    const dispatch = useDispatch();

    const {recipe, loading} = useSelector(state => ({
        recipe: state.recipes.data,
        loading: state.recipes.loading
    }));
    const [rated, setRated] = useState(false);
    const [userRating, setUserRating] = useState();
    const [displayTip, setDisplayTip] = useState(false);

    useEffect(() => {
        console.log(userRating);
    }, [userRating]);

    useEffect(() => {
        dispatch(fetchRecipe(recipeId, true));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        console.log(recipe);
    }, [recipe]);

    const userRatingHandler = rating => {
        setUserRating(rating);
    }
    const rateHandler = () => {
        if (userRating) {
            dispatch(rateRecipe(recipeId, userRating, history, AppUrls.RECIPE_VIEW_URL(categoryId, recipeId)));
            setDisplayTip(false);
        }
    }

    if (loading) {
        return <Spinner display={loading}/>
    }

    if (recipe && !loading) {
        return (
            <div className={'recipe--page__container'}>
                <div className={'recipe--page__inner--container'}>
                    <div className={'recipe--page__info'}>
                        <img src={recipe.imageUrl} width={'100%'}/>
                        <div className={'recipe--page__info--ingredients--container'}>
                            <h3>Ingredients:</h3>
                            {recipe.ingredients.map(ingredient => (
                                <div className={'recipe--page__info--ingredients--item'} key={ingredient.id}>
                                    <span>{ingredient.name}</span>
                                    <span>{ingredient.amount} {ingredient.unitOfMeasurement}</span>
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className={'recipe--page__details'}>
                        <span><h2>{recipe.name}</h2>
                            <h5>{recipe.complexity}</h5></span>
                        <span>{recipe.rating === 0 ? 'Not rated yet' : recipe.rating.toFixed(2)}</span>
                        <span className={'rate--recipe__rate--container'}>
                            <RateContainer
                                onChange={setUserRating}
                                onClick={() => setDisplayTip(true)}
                                onButtonClick={rateHandler}
                                display={displayTip}
                                close={() => setDisplayTip(false)}>
                                <RatingBar onChange={userRatingHandler} onClick={() => setDisplayTip(true)}/>
                            </RateContainer>

                            </span>
                        <h3>Preparation instructions: </h3>
                        <div className={'recipe--page__info--steps--container'}>
                            {recipe.steps.map(step => (
                                <div className={'recipe--page__info--steps--item'} key={step.id}>
                                    <span>{step.stepNo}</span>
                                    <span>{step.description}</span>
                                    <span>{step.mandatory}</span>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    return (
        <div>
            recipe page
        </div>
    );
};

export default RecipePage;