import React, {Fragment, useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Spinner from "../../components/commons/spinner/Spinner";
import CategoryInfo from "../../components/category/CategoryInfo/CategoryInfo";
import {Link} from "react-router-dom";
import AppUrls from "../../../utils/urls/appUrls";
import CategoryTable from "../../components/category/CategoryTable/CategoryTable";
import Modal from "../../components/commons/modal/Modal";
import {deleteCategories, deleteCategory, fetchCategories, fetchCategory} from "../../../store/actions";
import Pagination from "../../components/commons/pagination/Pagination";
import Dropdown from "../../components/commons/form/form/input/dropdown/Dropdown";
import Button from "../../components/commons/form/form/input/Button/Button";
import SearchBar from "../../components/commons/SearchBar/SearchBar";
import {usePagination} from "../../components/commons/hooks/usePagination";

import './CategoryBoard.css';

const CategoriesBoard = () => {
    const [deleting, setDeleting] = useState(false);
    const [multipleDeleting, setMultipleDeleting] = useState(false);
    const [displayModal, setDisplayModal] = useState(false);
    const [allChecked, setAllChecked] = useState(false);
    const [markedForDelete, setMarkedForDelete] = useState([]);
    const [resetCheckboxes, setResetCheckboxes] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(5);
    const [searchParam, setSearchParam] = useState('');

    const dispatch = useDispatch();
    const {categories, error, loading, category, isManipulated} = useSelector(state => ({
        categories: state.categories.collection,
        error: state.categories.error,
        loading: state.categories.loading,
        category: state.categories.data,
        isManipulated: state.categories.manipulated
    }));

    const searchMatchCollection = Object.values(categories).filter(value => value.name.includes(searchParam));
    const pageItems = usePagination(currentPage, itemsPerPage, searchMatchCollection);

    useEffect(() => {
    }, [searchParam]);

    useEffect(() => {
        if (!categories || (categories && isManipulated))
            dispatch(fetchCategories());
    }, [categories, dispatch, isManipulated]);

    const onSearchInputChange = event => {
        const {value} = event.target;
        setSearchParam(value);
    }

    const onCancelSearch = () => {
        setSearchParam('');
    }

    const onCategoryShowClick = id => {
        setDisplayModal(true);
        if (!category || (category && category.id !== +id))
            dispatch(fetchCategory(id));
    };

    const onCategoryShowCancelClick = () => {
        setDisplayModal(false);
    };

    const onCategoryDeleteClick = (categoryId) => {
        if (!category || (category && category.id !== +categoryId)) {
            dispatch(fetchCategory(categoryId));
            setDeleting(true);
        }
    };

    const onCategoryDeleteCancelClick = () => {
        setDeleting(false);
    };

    const onCategoryDeleteConfirmClick = () => {
        dispatch(deleteCategory(category.id));
        setDeleting(false);
    };

    const onCategoriesDeleteClick = () => {
        setMultipleDeleting(true);
        setResetCheckboxes(false);
    };

    const onCategoriesDeleteCancelClick = () => {
        setMultipleDeleting(false);
        setMarkedForDelete([]);
        setResetCheckboxes(true);
        setAllChecked(false);
    };

    const onCategoriesDeleteConfirmClick = () => {
        if (markedForDelete.length > 0) {
            const indices = {"indices": markedForDelete};
            dispatch(deleteCategories(indices));
        }
        setMultipleDeleting(false);
        setMarkedForDelete([]);
        setResetCheckboxes(true);
        setAllChecked(false);
    };

    const onCheckboxChange = event => {
        const {name, value, checked} = event.target;

        if (name === "checkAll") {
            setAllChecked(checked);
            if (checked) {
                setMarkedForDelete(Object.keys(categories));
            } else {
                setMarkedForDelete([]);
            }
        } else {
            if (checked && !markedForDelete.includes(value)) {
                setMarkedForDelete([...markedForDelete, value]);
            } else if (!checked && markedForDelete.includes(value)) {
                const index = markedForDelete.indexOf(value);
                setMarkedForDelete(markedForDelete.filter((el, i) => i !== index));
            }
        }
    };

    const onDropdownChange = event => {
        setItemsPerPage(event.target.value);
    }

    const renderCategoryInfoModal = () => {
        return (
            <Modal
                displayModal={displayModal}
                closeModal={onCategoryShowCancelClick}
                info
                header={category.name}
                footer={<Button type={'button'} className='actions--button actions--button__success__inverse'
                                onClick={onCategoryShowCancelClick} style={{width: '5rem'}}>OK</Button>}
            >
                <CategoryInfo category={category}/>
            </Modal>
        )
    };

    const renderDeleteCategoryModal = () => {
        const actions =
            <Fragment>
                <button className='actions--button actions--button__danger__inverse'
                        onClick={onCategoryDeleteConfirmClick}
                >Delete
                </button>
                < button className='actions--button actions--button__dark__inverse'
                         onClick={onCategoryDeleteCancelClick}> Cancel
                </button>
            </Fragment>;

        return (
            <Modal
                className={'no-select'}
                displayModal={deleting}
                closeModal={onCategoryDeleteCancelClick}
                danger
                header={'Delete Category!'}
                footer={actions}>
                Are you sure you want to delete {category ? category.name : 'this'} category?
            </Modal>
        )
    };

    const renderMultipleCategoriesDeleteModal = () => {
        const actions =
            <Fragment>
                <button className='actions--button actions--button__danger__inverse'
                        onClick={onCategoriesDeleteConfirmClick}
                >Delete
                </button>
                <button className='actions--button actions--button__dark__inverse'
                        onClick={onCategoriesDeleteCancelClick}> Cancel
                </button>
            </Fragment>;

        return (
            <Modal
                header={'Delete Categories!'}
                footer={actions}
                danger
                className={'no-select'}
                displayModal={multipleDeleting}
                closeModal={onCategoriesDeleteCancelClick}
            >
                {`Are you sure you want to delete ${markedForDelete.length} 
                       ${markedForDelete.length > 1 ? "categories" : "category"}`}
            </Modal>

        )
    };

    const renderActions = (categoryId) => {
        return (<Fragment>
            <span className={'actions'}>
                <i id='categoryPreview' className="fas fa-eye actions--button actions--button__info__inverse"
                   onClick={() => onCategoryShowClick(categoryId)}/>
                <Link to={AppUrls.EDIT_CATEGORY_APP_URL(categoryId)}>
                    <i className="far fa-edit actions--button actions--button__warning__inverse"/>
                </Link>
                {!markedForDelete.length &&
                <i className="far fa-trash-alt actions--button actions--button__danger__inverse"
                   onClick={() => onCategoryDeleteClick(categoryId)}/>}
                                                    </span>
        </Fragment>)
    }

    const renderTable = () => {
        return (
            <Fragment>
                <div className={'header__actions'}>
                    <SearchBar
                        searchHandler={onSearchInputChange}
                        cancelSearch={onCancelSearch}
                        initialValue={searchParam}/>

                    {markedForDelete.length > 0 &&
                    <Button
                        id='deleteSelected'
                        type="button"
                        danger
                        onClick={onCategoriesDeleteClick}
                    >Delete Selected</Button>}
                    <Button to={AppUrls.CREATE_CATEGORY} primary className="" id='add-category'>Add
                        Category</Button>
                </div>

                {searchMatchCollection.length > 0 ? <CategoryTable categories={pageItems}
                                                                   onCheckboxChange={onCheckboxChange}
                                                                   allChecked={allChecked}
                                                                   resetCheckboxes={resetCheckboxes}
                                                                   actions={renderActions}
                /> : <div className={'no--content'}><span>NO CATEGORIES FOUND</span></div>}
                {searchMatchCollection.length > 0 && renderPagination(searchMatchCollection.length)}
            </Fragment>
        )
    };

    const renderPagination = (itemsNumber) => {
        return <div className={'pagination-items'}>
            <Dropdown name={'itemsPerPage'}
                      dropdowns={[5, 10, 15, 20]}
                      onDropdownChange={onDropdownChange}
            />
            <Pagination itemsNumber={itemsNumber}
                        itemsPerPage={itemsPerPage}
                        currentPage={currentPage}
                        setCurrentPage={setCurrentPage}/>
        </div>
    }
    const renderError = () => {
        if (error) {
            return (<h1 className={'text-white'}>"Something went wrong"</h1>)
        }
    };


    if ((!categories && !error)) {
        return (<Spinner display={loading}/>)
    }

    if (!categories && error) {
        return (
            <div className={'container mt-lg-5'}><h1 className={'text-white'}>Something went wrong please try again
                later</h1></div>)
    }

    return (
        <Fragment>
            {renderError()}
            {renderDeleteCategoryModal()}
            {renderMultipleCategoriesDeleteModal()}
            {category && renderCategoryInfoModal()}
            {renderTable()}
        </Fragment>
    );
}

export default CategoriesBoard;
