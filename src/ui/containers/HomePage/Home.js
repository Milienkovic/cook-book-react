import React, {Fragment, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {fetchCategories} from "../../../store/actions";
import Spinner from "../../components/commons/spinner/Spinner";
import CategoryPreview from "../../components/category/CategoryPreview/CategoryPreview";
import AppUrls from "../../../utils/urls/appUrls";
import {Link} from "react-router-dom";

import './HomePage.css';

const Home = () => {
    const dispatch = useDispatch();
    const {categories, loading} = useSelector(state=>({
        categories: state.categories.collection,
        loading: state.categories.loading
    }));
    useEffect(() => {
        if (!categories) {
            dispatch(fetchCategories());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const renderCategories = () => {
        if (loading) {
            return <Spinner display={loading}/>
        }
        if (!loading && !categories){
            return <span>No categories available. Please try later</span>
        }

        if (categories) {
            return (
                <Fragment>
                    <div className='page--wrapper'>
                        {Object.values(categories).map(category =>
                            <Link to={AppUrls.CATEGORY_RECIPES_URL(category.id)} key={category.id}>
                                <div className='page__row'>
                                    <div className='page__column'>
                                        <CategoryPreview category={category}/>
                                    </div>
                                </div>
                            </Link>)}
                    </div>
                </Fragment>
            )
        }
    }

    return (
        <div>
            {renderCategories()}
        </div>
    );
};


export default Home;