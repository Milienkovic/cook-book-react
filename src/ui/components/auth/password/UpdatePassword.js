import React, {Fragment, useEffect, useRef, useState} from 'react';
import {Link} from "react-router-dom";
import AppUrls from "../../../../utils/urls/appUrls";
import { useDispatch, useSelector} from "react-redux";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import {logout, updatePassword} from "../../../../store/actions/auth/authActions";
import {scrollTo} from "../../../../utils/commons";
import Spinner from "../../commons/spinner/Spinner";
import Modal from "../../commons/modal/Modal";
import Button from "../../commons/form/form/input/Button/Button";
import Input from "../../commons/form/form/input/simple/Input";
import {useForm} from "../../commons/hooks/useForm";
import {useHistory} from 'react-router-dom';
import './../login/Auth.css';
import {useQueries} from "../../commons/hooks/useQueries";

const UpdatePassword = props => {
    const [formState, inputHandler] = useForm({
        password: {
            value: '',
            isValid: true
        },
        confirmPassword: {
            value: '',
            isValid: true
        }
    }, true);

    const dispatch = useDispatch();
    const history = useHistory();
    const token = useQueries(window.location.href, 'token');
    const {error, fieldErrors, loading, message} = useSelector(state=>({
        error: state.auth.error,
        fieldErrors: state.auth.fieldErrors,
        loading: state.auth.loading,
        message: state.auth.message
    }));
    const [displayModal, setDisplayModal] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const updatePassRef = useRef();

    useEffect(() => {
        return () => {
           dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (!loading)
            scrollTo(updatePassRef);
        if (message && submitted) {
            setDisplayModal(true);
        }
    }, [loading, message, submitted]);

    const onSubmit = event => {
        event.preventDefault();
        const {password, confirmPassword} = formState.inputs;
        if (token) {
            setSubmitted(true);
           dispatch(updatePassword({password: password.value, confirmPassword: confirmPassword.value}, token));
        }
    }

    const onMessageConfirmClick = () => {
        setDisplayModal(false);
        dispatch(logout());
        history.push(AppUrls.LOGIN);
    }

    const renderContent = () => {
        if (loading) {
            return <Spinner display={loading}/>
        }
        return (
            <Fragment>
                <div className="auth-container no-select" id='topContainer'
                     ref={updatePassRef}>
                    <div className="auth-card">
                        <div className="auth-card__header">
                            <h3>Reset Password</h3>
                            {error &&
                            <div className='auth__error'><i className="fas fa-info-circle">
                                    <span className='text-warning'>
                                        {' ' + error}
                                    </span>
                            </i></div>
                            }
                        </div>
                        <div className="auth-card__body ">
                            <form className={'auth-form'} onSubmit={onSubmit}>
                                <Input
                                    required
                                    id='password'
                                    element={'input'}
                                    type={'password'}
                                    name='password'
                                    label='Password'
                                    initalValue={formState.inputs.password.value}
                                    onInput={inputHandler}
                                    fieldError={fieldErrors && fieldErrors.password && fieldErrors.password}
                                    icon={'fas fa-key'}
                                />
                                <Input
                                    required
                                    id='confirmPassword'
                                    element={'input'}
                                    type={'password'}
                                    name='confirmPassword'
                                    label='Confirm password'
                                    initialValue={formState.inputs.confirmPassword.value}
                                    onInput={inputHandler}
                                    fieldError={fieldErrors && fieldErrors.confirmPassword && fieldErrors.confirmPassword}
                                    icon={'fas fa-key'}
                                />

                                <Button type={'submit'}>Change Password</Button>
                            </form>
                        </div>
                        <div className="auth-card__footer">
                            <div className="links">
                                Back to <Link to={AppUrls.LOGIN}>Login</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal displayModal={displayModal}
                       header={'Password Changed!'}
                       footer={<Button onClick={onMessageConfirmClick} success>OK</Button>}>
                    {props.message}
                </Modal>
            </Fragment>
        )
    }
    return (
        renderContent()
    );
}

export default UpdatePassword;