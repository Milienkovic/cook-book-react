import React, {Fragment, useEffect, useRef, useState} from 'react';
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import Modal from "../../commons/modal/Modal";
import {scrollTo} from "../../../../utils/commons";
import {changePasswordRequest} from "../../../../store/actions/auth/authActions";
import AppUrls from "../../../../utils/urls/appUrls";
import Button from "../../commons/form/form/input/Button/Button";
import {useQueries} from "../../commons/hooks/useQueries";

import './../login/Auth.css';

const ConfirmPasswordReset = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const id = useQueries(window.location.href, 'id');
    const token = useQueries(window.location.href, 'token');
    const {message, error, loading} = useSelector(state => ({
        loading: state.auth.loading,
        message: state.auth.message,
        error: state.auth.error
    }))
    const [showModal, setShowModal] = useState(false);

    const confirmPassResetRef = useRef();

    useEffect(() => {
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (!loading) {
            scrollTo(confirmPassResetRef);
        }
        if (message) {
            setShowModal(true);
        }
    }, [message, loading]);

    const onSubmit = event => {
        event.preventDefault();
        if (id && token) {
            dispatch(changePasswordRequest(id, token));
        }
    }

    const onMessageConfirmClick = () => {
        setShowModal(false);
        token ? history.push(AppUrls.UPDATE_PASSWORD_URL(token)) : history.push(AppUrls.HOME);
    }

    return (
        <Fragment>
            <div className="auth-container no-select" id="topContainer" ref={confirmPassResetRef}>

                <div className="auth-card" style={{maxWidth: '400px'}}>
                    <div className="auth-card__header">
                        <h3>Change Password</h3>
                        <div className='auth__info'><i className="fas fa-info-circle">
                                    <span>
                                        {error ? error + ' Please request new token!'
                                            : ' Click on the button to change your password'}
                                    </span>
                        </i></div>
                    </div>
                    <div className="auth-card__body ">
                        {!error && <form onSubmit={onSubmit}>
                            <div className="">
                                <Button disabled={!!message} type={'submit'}>Change Password</Button>
                            </div>
                        </form>}
                    </div>
                    <div className="auth-card__footer">
                        {error && <div className="links">
                            <Button to={AppUrls.REQUEST_RESET_PASSWORD_TOKEN}>Request new token</Button>
                        </div>}
                        <div className=" links">
                            Back to <Link to={AppUrls.LOGIN}>Login</Link>
                        </div>
                    </div>
                </div>
            </div>
            <Modal displayModal={showModal}
                   header={'Click button to change password!'}
                   footer={<Button onClick={onMessageConfirmClick} success>OK</Button>}>
                {message}
            </Modal>
        </Fragment>
    );
};

export default ConfirmPasswordReset;