import React, {Fragment, useEffect, useRef, useState} from 'react';
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import Modal from "../../commons/modal/Modal";
import {requestNewPassword} from "../../../../store/actions/auth/authActions";
import AppUrls from "../../../../utils/urls/appUrls";
import Spinner from "../../commons/spinner/Spinner";
import {scrollTo} from "../../../../utils/commons";
import Button from "../../commons/form/form/input/Button/Button";
import {useForm} from "../../commons/hooks/useForm";
import Input from "../../commons/form/form/input/simple/Input";

import '../login/Auth.css';

const SendPasswordToken = () => {
    const [formState, inputHandler] = useForm({
        email: {
            email: '',
            isValid: true
        }
    }, true);
    const history = useHistory();
    const dispatch = useDispatch();
    const {error, fieldErrors, loading, message} = useSelector(state => ({
        error: state.auth.error,
        fieldErrors: state.auth.fieldErrors,
        loading: state.auth.loading,
        message: state.auth.message,
    }));
    const [display, setDisplay] = useState(false);
    const sendPassRef = useRef();


    useEffect(() => {
        return () => {
            dispatch(purgeErrors())
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (message) {
            setDisplay(true);
        }
        if (!loading) {
            scrollTo(sendPassRef);
        }
    }, [message, loading]);

    const onSubmit = event => {
        event.preventDefault();
        const {email} = formState.inputs;
        dispatch(requestNewPassword(email.value));
    };

    const onMessageConfirmClick = () => {
        setDisplay(false);
        history.push(AppUrls.LOGIN);
    };

    const renderContent = () => {
        if (loading) {
            return <Spinner display={loading}/>
        }

        return (
            <Fragment>
                <div className="auth-container no-select" id='topContainer' ref={sendPassRef}>
                    <div className="auth-card" style={{maxWidth: '400px'}}>
                        <div className="auth-card__header">
                            <h3 className='mt-lg-3'>Request new password</h3>
                            <div className="d-flex justify-content-end social_icon">
                                <span><i className="fab fa-facebook-square"/></span>
                                <span><i className="fab fa-google-plus-square"/></span>
                                <span><i className="fab fa-twitter-square"/></span>
                            </div>

                            {error ?
                                <div className="auth__error">
                                    <i className="fas fa-info-circle">
                                        <span> Something went wrong. Try different e-mail</span></i>
                                </div> : null}
                        </div>
                        <div className="auth-card__body ">
                            <form className={'auth-form'} onSubmit={onSubmit}>
                                <Input
                                    required
                                    id='email'
                                    element={'input'}
                                    type={'email'}
                                    name='email'
                                    label='Email'
                                    initialValue={formState.inputs.email.value}
                                    onInput={inputHandler}
                                    fieldError={fieldErrors && fieldErrors.email && fieldErrors.email}
                                />
                                <div className="">
                                    <Button
                                        type={'submit'}
                                        disabled={!!message}>
                                        Send Email</Button>
                                </div>
                            </form>
                        </div>
                        <div className="auth-card__footer">
                            <div className="links">
                                Back to <Link to={AppUrls.LOGIN}>Login</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal displayModal={display}
                       header={'New Password Token Sent!'}
                       footer={<Button onClick={onMessageConfirmClick} success>OK</Button>}>
                    {message}
                </Modal>
            </Fragment>
        )
    }
    return (
        <Fragment>
            {renderContent()}
        </Fragment>
    );
};

export default SendPasswordToken;