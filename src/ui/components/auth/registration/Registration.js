import React, {Fragment, useEffect, useRef, useState} from 'react';
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {scrollTo} from "../../../../utils/commons";
import {registration} from "../../../../store/actions/auth/authActions";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import Modal from "../../commons/modal/Modal";
import Spinner from "../../commons/spinner/Spinner";
import AppUrls from "../../../../utils/urls/appUrls";
import Button from "../../commons/form/form/input/Button/Button";
import {useForm} from "../../commons/hooks/useForm";
import Input from "../../commons/form/form/input/simple/Input";
import '../login/Auth.css';

const Registration = () => {
    const [formState, inputHandler] = useForm({
        email: {
            value: '',
            isValid: true
        },
        password: {
            value: '',
            isValid: true
        },
        confirmPassword: {
            value: '',
            isValid: true
        }
    }, true);

    const dispatch = useDispatch();
    const history = useHistory();
    const {loading, error, fieldErrors, message} = useSelector(state => ({
        loading: state.auth.loading,
        error: state.auth.error,
        fieldErrors: state.auth.fieldErrors,
        message: state.auth.message,
        isRegistered: !!state.auth.message
    }));

    const [showModal, setShowModal] = useState(false);
    const regRef = useRef();

    useEffect(() => {
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (!loading)
            scrollTo(regRef);
        if (message) {
            setShowModal(true)
        }
    }, [loading, message]);

    const onSubmit = event => {
        event.preventDefault();
        const {email, password, confirmPassword} = formState.inputs;
        const registrationData = {email: email.value, password: password.value, confirmPassword: confirmPassword.value};
        dispatch(registration(registrationData));
    };

    const onMessageConfirmClick = () => {
        setShowModal(false);
        history.push(AppUrls.LOGIN);
    };

    const getConfirmPasswordError = () => {
        if (fieldErrors) {
            if (fieldErrors.confirmPassword) {
                return fieldErrors.confirmPassword;
            } else if (fieldErrors.userRegistrationRequest) {
                return fieldErrors.userRegistrationRequest;
            } else {
                return '';
            }
        } else return '';
    };

    const renderError = () => {
        if (error) {
            return <div className="auth__error"><label>{error}</label></div>
        }
    };

    const renderContent = () => {
        const confirmPasswordError = getConfirmPasswordError();
        if (loading) {
            return <Spinner display={loading}/>
        }

        return (
            <Fragment>
                <div className="auth-container no-select" ref={regRef} id='topContainer'>
                    <div className="auth-card">
                        <div className="auth-card__header">
                            <h3 className=''>Sign Up</h3>
                            <div className="social_icon">
                                <span><i className="fab fa-facebook-square"/></span>
                                <span><i className="fab fa-google-plus-square"/></span>
                                <span><i className="fab fa-twitter-square"/></span>
                            </div>
                            {renderError()}
                        </div>
                        <div className="auth-card__body">
                            <form className={'auth-form'}
                                  onSubmit={onSubmit}>
                                <Input
                                    id='email'
                                    type={'text'}
                                    element={'input'}
                                    name='email'
                                    label='Email'
                                    initialValue={formState.inputs.email.value}
                                    onInput={inputHandler}
                                    fieldError={fieldErrors && fieldErrors.email && fieldErrors.email}
                                />
                                <Input
                                    id='password'
                                    name='password'
                                    label='Password'
                                    element={'input'}
                                    initialValue={formState.inputs.password.value}
                                    type='password'
                                    onInput={inputHandler}
                                    fieldError={fieldErrors && fieldErrors.password && fieldErrors.password}
                                />
                                <Input
                                    id='confirmPassword'
                                    name='confirmPassword'
                                    element={'input'}
                                    label='Confirm password'
                                    initialValue={formState.inputs.confirmPassword.value}
                                    type='password'
                                    onInput={inputHandler}
                                    fieldError={confirmPasswordError}
                                />
                                <Button type={'submit'} disabled={!!message}>Sign Up</Button>
                            </form>
                        </div>
                        <div className="auth-card__footer">
                            <div className="links">
                                Back to <Link to={AppUrls.LOGIN}>Login</Link>
                            </div>
                            <div className="links">
                                <Link to={AppUrls.REQUEST_RESET_PASSWORD_TOKEN}>Forgot your password?</Link>
                            </div>
                        </div>
                    </div>
                    <Modal displayModal={showModal}
                           header={'Almost Done'}
                           footer={<Button onClick={onMessageConfirmClick}>OK</Button>}>
                        <div style={{textAlign: 'center'}}>{message}</div>
                    </Modal>
                </div>
            </Fragment>
        )
    }
    return (
        <Fragment>
            {renderContent()}
        </Fragment>
    );
}

export default Registration;