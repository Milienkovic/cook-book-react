import React, {Fragment, useEffect, useRef, useState} from 'react';
import {Link, useHistory} from "react-router-dom";

import Modal from "../../commons/modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import {requestNewVerificationToken} from "../../../../store/actions/auth/authActions";
import Spinner from "../../commons/spinner/Spinner";
import {scrollTo} from "../../../../utils/commons";
import AppUrls from "../../../../utils/urls/appUrls";
import Button from "../../commons/form/form/input/Button/Button";
import Input from "../../commons/form/form/input/simple/Input";
import {useForm} from "../../commons/hooks/useForm";
import './../login/Auth.css';

const NewVerificationToken = () => {
    const [formState, inputHandler] = useForm({
        email: {
            value: '',
            isValid: true
        }
    }, true);

    const dispatch = useDispatch();
    const history = useHistory();
    const {loading, error, fieldErrors, message} = useSelector(state => ({
        loading: state.auth.loading,
        error: state.auth.error,
        fieldErrors: state.auth.fieldErrors,
        message: state.auth.message
    }));
    const [display, setDisplay] = useState(false);

    const newVerificationTokenRef = useRef();

    useEffect(() => {
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    useEffect(() => {
        if (!loading) {
            scrollTo(newVerificationTokenRef);
        }
        if (message) {
            setDisplay(true);
        }
    }, [loading, message]);

    const onSubmit = event => {
        event.preventDefault();
        const {email} = formState.inputs;
        if (email) {
            dispatch(requestNewVerificationToken(email.value));
        }
    }

    const onMessageConfirmClick = () => {
        setDisplay(false);
        history.push(AppUrls.LOGIN);
    }

    const renderContent = () => {
        if (loading) {
            return <Spinner display={loading}/>
        }
        return (
            <Fragment>
                <div className="auth-container no-select" id='topContainer'
                     ref={newVerificationTokenRef}>
                    <div className="auth-card">
                        <div className="auth-card__header">
                            <h3>Request new Verification Token</h3>
                            <div className="social_icon">
                                <span><i className="fab fa-facebook-square"/></span>
                                <span><i className="fab fa-google-plus-square"/></span>
                                <span><i className="fab fa-twitter-square"/></span>
                            </div>

                            {error &&
                            <div className="auth__error">
                                <i className="fas fa-info-circle">
                                    <span> Something went wrong. Try different e-mail</span></i>
                            </div>}
                        </div>
                        <div className="auth-card__body ">
                            <form onSubmit={onSubmit}>
                                <Input
                                    addPadding
                                    required
                                    id='email'
                                    element={'input'}
                                    type={'email'}
                                    name='email'
                                    placeholder='email'
                                    initialValue={formState.inputs.email.value}
                                    onInput={inputHandler}
                                    fieldError={fieldErrors && fieldErrors.email && fieldErrors.email}
                                    icon={'fas fa-envelope'}
                                />
                                <Button disabled={!!message} type={'submit'}>Send Email</Button>
                            </form>
                        </div>
                        <div className="auth-card__footer">
                            <div className="links">
                                Back to <Link to={AppUrls.LOGIN}>Login</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal displayModal={display}
                       header={'Request new verification token'}
                       footer={<Button onClick={onMessageConfirmClick} success>OK</Button>}>
                    {message}
                </Modal>
            </Fragment>
        )
    }
    return (
        renderContent()
    );
};

export default NewVerificationToken;