import React, {Fragment, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';
import Modal from "../../commons/modal/Modal";
import {scrollTo} from "../../../../utils/commons";
import {confirmRegistration} from "../../../../store/actions/auth/authActions";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import AppUrls from "../../../../utils/urls/appUrls";
import Button from "../../commons/form/form/input/Button/Button";
import {useQueries} from "../../commons/hooks/useQueries";

import '../login/Auth.css';

const VerifyAccount = () => {
    const [displayModal, setDisplayModal] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();
    const token = useQueries(window.location.href, 'token');
    const {error, message, loading} = useSelector(state => ({
        loading: state.auth.loading,
        error: state.auth.error,
        message: state.auth.message
    }));
    const verifyAccRef = useRef();

    useEffect(() => {
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    useEffect(() => {
        if (!loading)
            scrollTo(verifyAccRef);
        if (message) {
            setDisplayModal(true);
        }
    }, [loading, message])

    const onConfirmRegistrationSubmit = event => {
        event.preventDefault();
        if (token) {
            dispatch(confirmRegistration(token));
        }
    }

    const onMessageConfirmClick = () => {
        setDisplayModal(false);
        history.push(AppUrls.LOGIN);
    }

    const renderContent = () => {
        return (
            <Fragment>
                <div className="auth-container no-select " id='topContainer' ref={verifyAccRef}>
                    <div className="auth-card"
                         style={{maxWidth: '315px'}}>
                        <div className="auth-card__header">
                            <h3>Confirm Registration</h3>
                            <div className='auth__info'>
                                <i className="fas fa-info-circle">
                                    <span>
                                        {error ? error + '  Please request new token!'
                                            : ' Please confirm your registration by clicking on the button'}
                                    </span>
                                </i>
                            </div>
                        </div>

                        <div className="auth-card__body">
                            {!error && <form onSubmit={onConfirmRegistrationSubmit}>
                                <Button disabled={error || message} type={'submit'}>Confirm
                                    Registration</Button>
                            </form>}

                        </div>
                        <div className="auth-card__footer">
                            {error &&
                            <div className="links">
                                <Button to={AppUrls.REQUEST_NEW_VERIFICATION_TOKEN}>Request new token!</Button>
                            </div>}
                        </div>
                    </div>
                </div>
                <Modal displayModal={displayModal}
                       header={'Account Verification Done!'}
                       footer={<Button onClick={onMessageConfirmClick} success>OK</Button>}>
                    <div>{message}</div>
                </Modal>
            </Fragment>
        )
    }
    return (
        <Fragment>
            {renderContent()}
        </Fragment>
    );
};

export default VerifyAccount;