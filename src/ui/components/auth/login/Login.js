import React, {Fragment, useEffect, useRef} from 'react';
import {Link} from "react-router-dom";

import { useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';
import {auth} from "../../../../store/actions/auth/authActions";
import {scrollTo} from "../../../../utils/commons";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import Spinner from "../../commons/spinner/Spinner";
import AppUrls from "../../../../utils/urls/appUrls";
import Button from "../../commons/form/form/input/Button/Button";
import {useForm} from "../../commons/hooks/useForm";
import Input from "../../commons/form/form/input/simple/Input";

import './Auth.css';

const Login = () => {

    const dispatch = useDispatch();
    const {loading, error, fieldErrors, isAuthenticated} = useSelector(state =>({
        loading: state.auth.loading,
        error: state.auth.error,
        fieldErrors: state.auth.fieldErrors,
        isAuthenticated: !!state.auth.token
    }));
    const history = useHistory();
    const [formState, inputHandler] = useForm({
        email: {
            value: '',
            isValid: true
        },
        password: {
            value: '',
            isValid: true
        }
    }, true);

    const loginRef = useRef();

    useEffect(() => {
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (!loading)
            scrollTo(loginRef);
    }, [loading]);

    useEffect(() => {
        if (isAuthenticated) {
            history.push(AppUrls.HOME);
        }
    }, [history, isAuthenticated]);

    const onSubmit = event => {
        event.preventDefault();
        const {email, password} = formState.inputs;
        dispatch(auth(email.value, password.value));
    };


    const renderError = () => {
        if (error) {
            return (
                <div className="auth__error"><label>{error}</label></div>
            )
        }
    };

    const renderContent = () => {
        if (loading) {
            return <Spinner display={loading}/>
        }

        return <Fragment>
            <div className="auth-container no-select" ref={loginRef} id='topContainer'>
                <div className="auth-card">
                    <div className="auth-card__header">
                        <h3>Sign In</h3>
                        <div className="social_icon">
                            <span><i className="fab fa-facebook-square"/></span>
                            <span><i className="fab fa-google-plus-square"/></span>
                            <span><i className="fab fa-twitter-square"/></span>
                        </div>

                        {renderError()}

                    </div>
                    <div className="auth-card__body">
                        <form  className={'auth-form'}
                            onSubmit={onSubmit}>
                            <Input
                                id='email'
                                element={'input'}
                                type={'email'}
                                name='email'
                                label='Email'
                                initialValue={formState.inputs.email.value}
                                onInput={inputHandler}
                                fieldError={fieldErrors && fieldErrors.email && fieldErrors.email}
                            />
                            <Input
                                id='password'
                                element={'input'}
                                name='password'
                                label='Password'
                                initialValue={formState.inputs.password.value}
                                type='password'
                                onInput={inputHandler}
                                fieldError={fieldErrors && fieldErrors.password && fieldErrors.password}
                            />

                            <div className="">
                                <Button type={'submit'}>LOGIN</Button>
                            </div>
                        </form>
                    </div>
                    <div className="auth-card__footer">
                        <div className="links">
                            Don't have an account? <Link to={AppUrls.REGISTRATION}>Sign Up</Link>
                        </div>
                        <div className="links">
                            <Link to={AppUrls.REQUEST_RESET_PASSWORD_TOKEN}>Forgot your password?</Link>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    }

    return (
        <Fragment>
            {renderContent()}
        </Fragment>
    );
}

export default Login;