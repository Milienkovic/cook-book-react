import React, {Fragment, useEffect, useState} from 'react';
import FormSection from "../../commons/form/form/section/FormSection";
import Input from "../../commons/form/form/input/simple/Input";
import {useDispatch, useSelector} from "react-redux";
import {deleteUser, fetchUser, updateUser} from "../../../../store/actions/users/usersActions";
import Tokens from "../../../../utils/tokens";
import Spinner from "../../commons/spinner/Spinner";
import Modal from "../../commons/modal/Modal";
import {useHistory} from "react-router-dom";
import AppUrls from "../../../../utils/urls/appUrls";
import {useForm} from "../../commons/hooks/useForm";
import Button from "../../commons/form/form/input/Button/Button";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import Form from "../../commons/form/form/Form";

import './UserForm.css';

const initialUserInputs = {
    firstName: {value: '', isValid: true},
    lastName: {value: '', isValid: true},
    phone: {value: '', isValid: true},
    email: {value: '', isValid: true},
    country: {value: '', isValid: true},
    city: {value: '', isValid: true},
    street: {value: '', isValid: true},
};

const UserForm = props => {
    const [formState, inputHandler, setFormData] = useForm(initialUserInputs, true);

    const [displayModal, setDisplayModal] = useState(false);

    const dispatch = useDispatch();
    const history = useHistory();
    const {user, loading, token, fieldErrors, error, message, isManipulated} = useSelector(state =>({
        user: state.users.data,
        loading: state.users.loading,
        token: state.auth.token,
        fieldErrors: state.users.fieldErrors,
        error: state.users.error,
        message: state.users.message,
        isManipulated: state.users.manipulated
    }));

    const email = Tokens.decodeAuthToken(token).sub;

    useEffect(() => {
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (!email) {
            history.push(AppUrls.HOME);
        }
        if ((!user) || (user && user.email !== email) || (user && isManipulated)) {
            dispatch(fetchUser(email));
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (user && user.email === email) {
            setFormData({
                ...formState.inputs,
                firstName: {value: user.firstName, isValid: true},
                lastName: {value: user.lastName, isValid: true},
                phone: {value: user.phone, isValid: true},
                email: {value: user.email, isValid: true},
                country: {value: user.address && user.address.country, isValid: true},
                city: {value: user.address && user.address.city, isValid: true},
                street: {value: user.address && user.address.street, isValid: true},
            }, true);
            console.log(formState.inputs)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user, setFormData, email]);

    useEffect(() => {
        if (message && !displayModal)
            setDisplayModal(true);
    }, [displayModal, message]);

    const _mapUserFromState = () => {
        const data = formState.inputs;
        return {
            firstName: data.firstName.value,
            lastName: data.lastName.value,
            phone: data.phone.value,
            email: data.email.value,
            address: {
                country: data.country.value,
                city: data.city.value,
                street: data.street.value
            }
        }
    }

    const onFormSubmit = event => {
        const userData = _mapUserFromState();
        event.preventDefault();
        if (user) {
            dispatch(updateUser(user.id, userData));
        } else {
            history.push(AppUrls.HOME);
        }
    }

    // eslint-disable-next-line no-unused-vars
    const onUserDeleting = () => {
        const email = user.email;
        const emailFromToken = Tokens.decodeAuthToken(token).sub;
        if (email === emailFromToken) {
            dispatch(deleteUser(email));
        }
        //todo add delete button
    }
    const onMessageConfirmClick = () => {
        history.push(AppUrls.HOME);
    }

    const renderUserForm = () => {
        const {firstName, lastName, email, phone, city, country, street} = formState.inputs;
        return (
            <div className={'user--form__container'}>
                <Form
                    title={'User'}
                    subtitle={' edit your data '}
                    onSubmit={onFormSubmit}
                    error={error}
                    buttonText={"Submit"}>
                    <FormSection sectionName={' Basic Info'} isActive>
                        <Input
                            id={'firstName'}
                            label={'First Name'}
                            name={'firstName'}
                            element={'input'}
                            initialValue={firstName.value}
                            type={'text'}
                            fieldError={fieldErrors && fieldErrors.firstName && fieldErrors.firstName}
                            onInput={inputHandler}
                            labelErrorStyle={{color: 'darkred'}}
                        />
                        <Input
                            label={'Last Name'}
                            id={'lastName'}
                            name={'lastName'}
                            element={'input'}
                            initialValue={lastName.value}
                            type={'text'}
                            fieldError={fieldErrors && fieldErrors.lastName && fieldErrors.lastName}
                            onInput={inputHandler}
                            labelErrorStyle={{color: 'darkred'}}
                        />
                        <Input
                            label={'Email'}
                            id={'email'}
                            name={'email'}
                            element={'input'}
                            initialValue={email.value}
                            type={'email'}
                            fieldError={fieldErrors && fieldErrors.email && fieldErrors.email}
                            onInput={inputHandler}
                            labelErrorStyle={{color: 'darkred'}}
                        />
                        <Input
                            label={'Phone'}
                            id={'phone'}
                            name={'phone'}
                            element={'input'}
                            initialValue={phone.value}
                            type={'tel'}
                            fieldError={fieldErrors && fieldErrors.phone && fieldErrors.phone}
                            onInput={inputHandler}
                            labelErrorStyle={{color: 'darkred'}}
                        />
                    </FormSection>

                    <FormSection sectionName={'Address'} isToggable>
                        <Input
                            label={'Country'}
                            id={'country'}
                            name={'country'}
                            element={'input'}
                            initialValue={country.value}
                            type={'text'}
                            fieldError={fieldErrors && fieldErrors.country && fieldErrors.country}
                            onInput={inputHandler}
                            labelErrorStyle={{color: 'darkred'}}
                        />
                        <Input
                            label={'City'}
                            id={'city'}
                            name={'city'}
                            element={'input'}
                            initialValue={city.value}
                            type={'text'}
                            fieldError={fieldErrors && fieldErrors.city && fieldErrors.city}
                            onInput={inputHandler}
                            labelErrorStyle={{color: 'darkred'}}
                        />
                        <Input
                            label={'Street'}
                            id={'street'}
                            name={'street'}
                            element={'input'}
                            initialValue={street.value}
                            type={'text'}
                            fieldError={fieldErrors && fieldErrors.street && fieldErrors.street}
                            onInput={inputHandler}
                            labelErrorStyle={{color: 'darkred'}}
                        />
                    </FormSection>
                </Form></div>
        )
    }

    if (loading || (!user && props.updating) || (user && formState.inputs === initialUserInputs)) {
        return <Spinner display={loading}/>
    }

    return (
        <Fragment>
            {renderUserForm()}
            <Modal displayModal={displayModal} header={'Data changed successfully!'}
                   footer={<Button onClick={onMessageConfirmClick} success>OK</Button>}>
                {message}
            </Modal>

        </Fragment>
    );
}

export default UserForm;