import React from 'react';
import UserForm from "../UserForm/UserForm";

import './PersonalData.css';

const PersonalData = props => {
    return (
        <div className={'personal--data__container'}>
            <UserForm updating/>
        </div>
    );
}

export default PersonalData;