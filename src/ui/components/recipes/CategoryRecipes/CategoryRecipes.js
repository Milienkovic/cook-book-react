import React, {Fragment, useEffect} from 'react';
import {fetchCategoryRecipes} from "../../../../store/actions/recipes/recipesActions";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from 'react-router-dom';
import Spinner from "../../commons/spinner/Spinner";
import Button from "../../commons/form/form/input/Button/Button";
import RecipePreview from "../RecipePreview/RecipePreview";
import AppUrls from "../../../../utils/urls/appUrls";

import './CategoryRecipes.css';

const CategoryRecipes = () => {

    const categoryId = useParams().id;
    const dispatch = useDispatch();
    const {recipes, loading, isAuth} = useSelector(state => ({
        recipes: state.recipes.collection,
        loading: state.recipes.loading,
        isAuth: !!state.auth.token,
        isManipulated: state.recipes.manipulated
    }));

    useEffect(() => {
        dispatch(fetchCategoryRecipes(categoryId));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const renderRecipes = () => {
        if (loading || !recipes) {
            return <Spinner display={loading}/>
        }
        const recipesList = Object.values(recipes);
        return <div className={'category--recipes__container'}>
            {isAuth &&
            <Button primary type={'button'} className='recipe__add--button' to={AppUrls.CREATE_RECIPE_URL(categoryId)}>Create
                Recipe</Button>}
            {recipes && recipesList.length === 0 && <span style={{alignSelf: 'center'}}>No recipes found</span>}
            {recipes && recipesList.length > 0 &&
            <div className={'category--recipes__body'}>
                {Object.values(recipes).map(recipe => (
                    <div key={recipe.id} className={'category--recipes__item'}
                         style={recipesList.length === 1 ? {flex: '1'} : undefined}>
                        <RecipePreview recipe={recipe}/>
                    </div>
                ))}
            </div>}
        </div>
    }

    return (
        <Fragment>
            {renderRecipes()}
        </Fragment>
    );
};

export default CategoryRecipes;