import React from 'react';
import RecipeMultipageForm from "../RecipeMultipageForm";

import './EditRecipe.css';

const EditRecipe = () => {
    return (
        <div className={'edit--recipe__container'}>
            <RecipeMultipageForm updating/>

        </div>
    );
};

export default EditRecipe;