/* eslint-disable no-unused-vars */
import React, {useEffect, useState} from 'react';
import Button from "../commons/form/form/input/Button/Button";
import Ingredients from "../ingredients/Ingredients";
import Steps from "../steps/Steps";
import CreateRecipe from "./CreateRecipe/CreateRecipe";
import RecipeForm from "./RecipeForm/RecipeForm";
import {useHistory, useParams} from 'react-router-dom';
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {addRecipe, updateRecipe} from "../../../store/actions/recipes/recipesActions";
import Spinner from "../commons/spinner/Spinner";
import ProgressBar from "../commons/form/upload/progressBar/ProgressBar";
import AppUrls from "../../../utils/urls/appUrls";

import './RecipeMultiPageForm.css';

const RecipeMultipageForm = props => {
    const {loading, uploading, uploadProgress, recipeMessage, ingredientsMessage, stepsMessage} = useSelector(state => ({
        loading: state.recipes.loading || state.steps.loading || state.ingredients.loading,
        uploading: state.files.uploading,
        uploadProgress: state.files.uploadProgress,
        recipeMessage: state.recipes.message,
        ingredientsMessage: state.ingredients.message,
        stepsMessage: state.steps.message,
    }), shallowEqual);

    const [step, setStep] = useState(1);
    const [recipe, setRecipe] = useState();
    const [ingredients, setIngredients] = useState([]);
    const [steps, setSteps] = useState([]);
    const categoryId = useParams().categoryId;
    const history = useHistory();
    const dispatch = useDispatch();
    const recipeId = useParams().recipeId;
    const {updating} = props;
    const [ingredientsFetched, setIngredientsFetched] = useState(false);
    const [stepsFetched, setStepsFetched] = useState(false);

    const nextHandler = () => {
        setStep(prevState => prevState + 1);
    }

    const previousHandler = () => {
        setStep(prevState => {
            if (prevState > 1)
                return prevState - 1;
            else
                return prevState;
        });
    }


    useEffect(() => {
        const isSuccess = () => {
            if (recipeMessage && !ingredientsMessage && !stepsMessage) {
                return recipeMessage.success;
            } else if (recipeMessage && ingredientsMessage && !stepsMessage) {
                return recipeMessage.success && ingredientsMessage.success;
            } else if (recipeMessage && !ingredientsMessage && stepsMessage) {
                return recipeMessage.success && stepsMessage.success;
            } else if (recipeMessage && ingredientsMessage && stepsMessage) {
                return recipeMessage.success && ingredientsMessage.success && stepsMessage.success;
            }
            return false;
        }
        if (isSuccess()) {
            history.push(AppUrls.HOME);
        }

    }, [history, ingredientsMessage, recipeMessage, stepsMessage]);

    const submitRecipeDataHandler = () => {
        let ingredientsData;
        let stepsData;

        if (updating) {
            let newId = 0;
            let stepId;
            let ingredientId;
            stepsData = steps.map(step => {
                if (!step.hasOwnProperty('id')) {
                    newId -= 1;
                    stepId = newId;
                } else {
                    stepId = step.id.value;
                }
                return {
                    id: stepId,
                    stepNo: step.stepNumber.value,
                    description: step.description.value,
                    mandatory: step.mandatory.value
                }
            });

            ingredientsData = ingredients.map(ingredient => {
                if (!ingredient.hasOwnProperty('id')) {
                    newId -= 1;
                    ingredientId = newId;
                } else {
                    ingredientId = ingredient.id.value;
                }
                return {
                    id: ingredientId,
                    name: ingredient.name.value,
                    amount: ingredient.amount.value,
                    unitOfMeasurement: ingredient.uom.value
                };
            });
        } else {
            ingredientsData = ingredients.map(ingredient => {
                return {
                    name: ingredient.name.value,
                    amount: ingredient.amount.value,
                    unitOfMeasurement: ingredient.uom.value
                };
            });

            stepsData = steps.map(step => {
                return {
                    stepNo: step.stepNumber.value,
                    description: step.description.value,
                    mandatory: step.mandatory.value
                }
            });
        }

        const recipeData = {
            name: recipe.name.value,
            complexity: recipe.complexity.value,
            image: recipe.image.value,
            visible: recipe.isPublic.value,
            ingredients: ingredientsData,
            steps: stepsData
        }
        if (updating) {
            console.log(ingredientsData)
            dispatch(updateRecipe(recipeId, recipeData, history));
        } else {
            dispatch(addRecipe(categoryId, recipeData, history));
        }
    }

    const ingredientsSubmitHandler = (ingredients) => {
        setIngredients(ingredients);
    }

    const stepsSubmitHandler = steps => {
        setSteps(steps);
    }

    const recipeSubmitHandler = (recipe) => {
        setRecipe(recipe);
    }

    const renderForm = () => {
        switch (step) {
            case 1:
                return <RecipeForm
                    creating={!updating}
                    manipulating={!!recipe}
                    onSubmit={recipeSubmitHandler}
                    initValue={recipe}
                    updating={updating}
                />;
            case 2:
                return <Ingredients ingredients={ingredients}
                                    updating={updating}
                                    onFetch={setIngredientsFetched}
                                    fetched={ingredientsFetched}
                                    onIngredientsChange={ingredientsSubmitHandler}/>;
            case 3:
                return <Steps steps={steps}
                              updating={updating}
                              fetched={stepsFetched}
                              onFetch={setStepsFetched}
                              onStepsChange={stepsSubmitHandler}/>;
            default:
                return <CreateRecipe/>;
        }
    }

    const getTitle = () => {
        switch (step) {
            case 1:
                return "Basic Recipe Info";
            case 2:
                return 'Recipe Ingredients';
            case 3:
                return 'Preparation Steps';
            default:
                return 'Basic Recipe Info';
        }
    }

    if ((loading && !uploading)) {
        return <Spinner display={loading}/>
    }

    if (uploading) {
        return <ProgressBar text={'Uploading...'}
                            display={uploading}
                            percentage={uploadProgress}
        />
    }

    return (
        <div className={'multipage--form__container'}>
            <div className={'multipage--form__inner--container'}>
                <h1 className={'multipage--form__title'}>{getTitle()}</h1>
                {renderForm()}
                <div className={'multipage--form__actions'}>
                    {step > 1 && <Button onClick={previousHandler} type={'button'}>Back</Button>}
                    {step < 3 && <Button onClick={nextHandler} type={'button'}>Next</Button>}
                    {step === 3 && <Button success type={'submit'} onClick={submitRecipeDataHandler}>Submit</Button>}
                </div>
            </div>
        </div>
    );
};

export default RecipeMultipageForm;