import React, {Fragment, useEffect, useState} from 'react';
import {useForm} from "../../commons/hooks/useForm";
import {useParams} from 'react-router-dom';
import Input from "../../commons/form/form/input/simple/Input";
import {Complexity} from "../../../../utils/enums/complexity";
import {fetchRecipe} from "../../../../store/actions/recipes/recipesActions";
import {useDispatch, useSelector} from "react-redux";
import Spinner from "../../commons/spinner/Spinner";
import Button from "../../commons/form/form/input/Button/Button";

import './RecipeForm.css';

const initialFormInputs = {
    name: {
        value: '',
        isValid: true
    },
    complexity: {
        value: 'EASY',
        isValid: true
    },
    image: {
        value: null,
        isValid: true,
    },
    isPublic: {
        value: false,
        isValid: true
    }
}

const RecipeForm = props => {
    const {updating, creating, initValue, manipulating} = props;
    const [formState, inputHandler, setFormData] = useForm(manipulating ? initValue : initialFormInputs, true);
    const [submitted, setSubmitted] = useState(false);
    const {recipe, loading, fieldErrors, fileError} = useSelector(state => ({
        recipe: state.recipes.data,
        loading: state.recipes.loading,
        fieldErrors: state.recipes.fieldErrors,
        fileErrors: state.files.error
    }));
    const dispatch = useDispatch();
    const recipeId = useParams().recipeId;
    const dropdowns = Complexity.complexities;

    useEffect(() => {
        if ((updating && !recipe) || (updating && recipe && +recipeId !== recipe.id)) {
            dispatch(fetchRecipe(recipeId));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setSubmitted(false);
    }, []);

    useEffect(() => {
        if (submitted) {
            props.onSubmit(recipe);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [submitted]);

    useEffect(() => {
        if (recipe && updating && +recipeId === recipe.id) {
            setFormData({
                ...formState.inputs,
                name: {value: recipe.name, isValid: true},
                complexity: {value: recipe.complexity, isValid: true},
                isPublic: {value: recipe.visible, isValid: true},
            }, true);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [recipe, updating, fetchRecipe, setFormData, recipeId]);

    const onImageAdded = image => {
        setFormData({
            ...formState.inputs,
            image: {
                value: image,
                isValid: true
            }
        })
    }

    const onImageRemoved = () => {
        setFormData({
            ...formState.inputs,
            image: {
                value: null,
                isValid: true
            }
        }, true)
    }

    const toggleIsPublic = () => {
        setFormData({
            ...formState.inputs,
            isPublic: {
                value: !formState.inputs.isPublic.value,
                isValid: true
            }
        })
    }

    const onSubmit = (event) => {
        event.preventDefault();
        setSubmitted(true);
    }

    useEffect(() => {
        props.onSubmit(formState.inputs);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [formState.inputs, props.onSubmit]);

    const renderForm = () => {
        if (loading || (!recipe && updating)) {
            return <Spinner display={loading}/>
        }

        if ((updating && !loading && formState.inputs !== initialFormInputs) || creating) {
            return (
                <div className={'recipe--form__container'}>
                    <form onSubmit={onSubmit}
                          className={'recipe--form'}>
                        <Input
                            id={'name'}
                            element={'input'}
                            name={'name'}
                            type={'text'}
                            label={'Recipe Name'}
                            initialValue={formState.inputs.name.value}
                            fieldError={fieldErrors && fieldErrors.name}
                            onInput={inputHandler}/>
                        <Input
                            id={'complexity'}
                            name={'complexity'}
                            label={'Recipe Complexity'}
                            type={'text'}
                            initialValue={formState.inputs.complexity.value}
                            element={'dropdown'}
                            dropdowns={dropdowns}
                            fieldError={fieldErrors && fieldErrors.complexity}
                            onInput={inputHandler}/>
                        <Input
                            id={'image'}
                            name={'image'}
                            label={'Upload Image'}
                            element={'image'}
                            text={updating && !recipe.imageUrl && 'Please upload image'}
                            onImageAdded={onImageAdded}
                            onImageRemoved={onImageRemoved}
                            initialValue={formState.inputs.image.value}
                            file={formState.inputs.image.value}
                            onInput={inputHandler}
                            fieldError={fileError}
                            backgroundImage={updating && recipe.imageUrl && recipe.imageUrl}
                        />
                        <Input
                            id={'isPublic'}
                            name={'isPublic'}
                            initialValue={formState.inputs.isPublic.value}
                            element={'checkmark'}
                            label={'Visibility: '}
                            onInput={inputHandler}
                            content={'Check to make recipe public'}
                            onChange={toggleIsPublic}
                            fieldError={fieldErrors && fieldErrors.visible}
                        />
                        <Button>Submit</Button>
                    </form>
                </div>
            );
        }
    }

    return (
        <Fragment>
            {renderForm()}
        </Fragment>
    );
};

export default RecipeForm;