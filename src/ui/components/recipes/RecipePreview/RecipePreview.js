/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import placeholder from "../../../../assets/images/placeholder.jpg";
import Tooltip from "../../commons/tooltip/Tooltip";
import Svgs from "../../../../utils/svgs/svgs";
import {Complexity} from "../../../../utils/enums/complexity";
import {useHistory, useParams} from 'react-router-dom';
import AppUrls from "../../../../utils/urls/appUrls";

import './RecipePreview.css';

const RecipePreview = (props) => {
    const {recipe} = props;
    const categoryId = useParams().id;
    const history = useHistory();

    const clickHandler = () => {
        history.push(AppUrls.RECIPE_VIEW_URL(categoryId, recipe.id));
    }

    return (
        <div className={'recipe--preview__container'} onClick={clickHandler}>
            <div className={'recipe--preview__body'}>
                <img src={recipe.imageUrl ? recipe.imageUrl : placeholder} width={'100%'}
                     height={'100%'} alt={'recipe\'s image'}/>
                {props.enableActions &&
                <div className={'recipe--preview__actions--container'} onClick={() => props.onRecipeClick(recipe.id)}>
                    {props.actions}
                </div>}
            </div>
            <div className={'recipe--preview__footer'}>
                                <span className={`recipe--preview__footer__recipe--name 
                                    ${recipe.visible ? 'recipe__public' : 'recipe__private'}`}
                                >{!recipe.visible &&
                                <Tooltip
                                    hoveringText={'This recipe is visible only to you!'}>
                                    <i className={'recipe__locked'}>{Svgs.lockedIcon}</i></Tooltip>} {recipe.name}</span>
                <span className={'recipe--preview__footer__recipe--complexity'}
                      style={Complexity.complexityColor(recipe.complexity)}
                >{recipe.complexity.replace('_', ' ')}</span>
            </div>
        </div>
    );
};

export default RecipePreview;