import React from 'react';
import RecipeForm from "../RecipeForm/RecipeForm";

import './CreateRecipe.css';

const CreateRecipe = () => {
    return (
        <div className={'create--recipe__container'}>
            <RecipeForm creating/>
        </div>
    );
};

export default CreateRecipe;