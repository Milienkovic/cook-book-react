import React, {useEffect} from 'react';
import CategoryForm from "./CategoryForm/CategoryForm";
import {useDispatch} from "react-redux";
import {purgeErrors} from "../../../store/actions/general/generalActions";

import './CreateCategory.css';

const EditCategory = (props) => {
    const dispatch = useDispatch();
    useEffect(() => {
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className={'create--category__container'}>
            <CategoryForm
                {...props}
                title={'Edit category'}
                updating
            />
        </div>
    );
};

export default EditCategory;