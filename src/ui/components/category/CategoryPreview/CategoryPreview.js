import React from 'react';
import './CategoryPreview.css';

const preview = ({category}) => {
    return (
        <div className={'preview--container'}>
            <div className={'preview--body'} style={{backgroundImage: `url(${category.imageUrl})`}}>
                <div className={'preview--footer'}>
                    <span className={'preview--footer__title'}>{category.name}</span>
                    <span className={'preview--footer__info'}>{category.recipesCount}</span>
                </div>
            </div>
        </div>
    );
};

export default preview;