import React, {useEffect} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {addCategory, fetchCategory, updateCategory} from "../../../../store/actions";
import Spinner from "../../commons/spinner/Spinner";
import ProgressBar from "../../commons/form/upload/progressBar/ProgressBar";
import Input from "../../commons/form/form/input/simple/Input";
import {useForm} from "../../commons/hooks/useForm";
import Form from "../../commons/form/form/Form";

import './CategoryForm.css';

const initCategoryInputs = {
    name: {
        value: '',
        isValid: true
    },
    description: {
        value: '',
        isValid: true
    },
    image: {
        value: null,
        isValid: true
    }
};

const CategoryForm = props => {
    const [formState, inputHandler, setFormData] = useForm(initCategoryInputs, true);

    const history = useHistory();
    const id = useParams().id;
    const dispatch = useDispatch();
    const {updating, creating} = props;

    const {category, loading, error, fieldErrors, fileError, uploadProgress, uploading} = useSelector(state => ({
        category: state.categories.data,
        loading: state.categories.loading,
        error: state.categories.error,
        fieldErrors: state.categories.fieldErrors,
        fileError: state.files.error,
        uploadProgress: state.files.uploadProgress,
        uploading: state.files.uploading,
    }))
    useEffect(() => {
        if ((updating && !category) || (category && category.id !== +id)) {
            dispatch(fetchCategory(id));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (updating && category && category.id === +id) {
            setFormData({
                ...formState.inputs,
                name: {
                    value: category.name,
                    isValid: true
                },
                description: {
                    value: category.description,
                    isValid: true
                }
            }, true);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [category, id, setFormData, updating]);

    const onImageAdded = image => {
        setFormData({
            ...formState.inputs,
            image: {
                value: image,
                isValid: true
            }
        }, true);
    }

    const onImageRemoved = () => {
        setFormData({
            ...formState.inputs,
            image: {
                value: null,
                isValid: true
            }
        }, true);
    }

    const onFormSubmit = event => {
        event.preventDefault();
        const {name, description, image} = formState.inputs;
        const category = {name: name.value, description: description.value, image: image.value};
        if (props.updating) {
            dispatch(updateCategory(id, category, history));
        } else {
            dispatch(addCategory(category, history));
        }
    };

    if ((loading && !uploading) || (updating && !category) || (formState.inputs === initCategoryInputs && updating)) {
        return (<Spinner display={loading}/>)
    }

    if (uploading) {
        return <ProgressBar text={'Uploading...'}
                            percentage={uploadProgress}
                            display={uploading}/>
    }

    if ((updating && formState.inputs !== initCategoryInputs) || creating) {
        return (
            <div className={'category--form__container'}>
                <Form title={props.title ? props.title : 'Category'}
                      subtitle={updating && category.name}
                      onSubmit={onFormSubmit}
                      error={error}
                      buttonText={updating ? 'Edit Category' : 'Create Category'}>
                    <Input
                        element={'input'}
                        label={'Category Name'}
                        id='name'
                        name='name'
                        initialValue={formState.inputs.name.value}
                        type="text"
                        fieldError={fieldErrors && fieldErrors.name && fieldErrors.name}
                        onInput={inputHandler}
                    />
                    <Input
                        element={'textarea'}
                        label={'Category Description'}
                        id='description'
                        name='description'
                        initialValue={formState.inputs.description.value}
                        fieldError={fieldErrors && fieldErrors.description && fieldErrors.description}
                        onInput={inputHandler}
                    />
                    <Input
                        id={'image'}
                        name={'image'}
                        element={'image'}
                        label={'Upload Image'}
                        text={category && !category.imageUrl && 'Please provide image for this category!'}
                        fieldError={fileError}
                        onImageAdded={onImageAdded}
                        file={formState.inputs.image.value}
                        onImageRemoved={onImageRemoved}
                        initialValue={formState.inputs.image.value}
                        onInput={inputHandler}
                        backgroundImage={category && updating && category.imageUrl}
                    />
                </Form>
            </div>
        );
    }
}

export default CategoryForm;