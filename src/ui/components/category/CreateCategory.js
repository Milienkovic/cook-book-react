import React, {useEffect} from 'react';
import CategoryForm from "./CategoryForm/CategoryForm";
import {useDispatch} from "react-redux";
import {purgeErrors} from "../../../store/actions/general/generalActions";

import './CreateCategory.css';

const CreateCategory = (props) => {
    const dispatch = useDispatch();
    useEffect(() => {
        return ()=>{
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className={'create--category__container'}>
            <CategoryForm
                {...props}
                title={'Create New Category'}
                creating
            />
        </div>
    );
};

export default CreateCategory;