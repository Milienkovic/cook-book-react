import React, {Fragment} from 'react';
import './CategoryInfo.css';
import placeholderImage from "./../../../../assets/images/placeholder.jpg"
import {useSelector} from "react-redux";
import Spinner from "../../commons/spinner/Spinner";

const CategoryInfo = (props) => {
    const {loading} = useSelector(state =>({
        loading: state.categories.loading
    }));

    const renderContent = () => {
        if (loading)
            return <Spinner display={loading}/>

        return (
            <Fragment>
                <div className="category--info__container">
                    <div className="category--info__card">
                        <div className="category--info__body">
                            <strong className="">Description:</strong>
                            <h6 className="category--info__text">
                                {props.category.description ? props.category.description : "Please provide description for this category"}
                            </h6>
                        </div>

                        <img className="category--info__img"
                             alt="Thumbnail [200x250]"
                             src={props.category.imageUrl ? props.category.imageUrl : placeholderImage}
                             width='200px'
                             height='250px'/>
                    </div>
                </div>
            </Fragment>
        )
    }
    return (
        renderContent()
    );
};

export default CategoryInfo;