import React, {Fragment, useRef, useState} from 'react';
import SimpleTable from "../../commons/table/SimpleTable";
import Modal from "../../commons/modal/Modal";
import Tooltip from "../../commons/tooltip/Tooltip";
import CustomCheckbox from "../../commons/form/checkbox/CustomCheckbox";
import {useComponentDimensions} from "../../commons/hooks/useComponentDimensions";

import './CategoryTable.css';

const CategoryTable = (props) => {
    const tableRef = useRef();
    const {width} = useComponentDimensions(tableRef);
    const [display, setDisplay] = useState(false);

    const toggleDisplay = () => {
        setDisplay(prevState => !prevState);
    }

    const masterCheckbox =
        <CustomCheckbox
            name={'checkAll'}
            id={'master'}
            value={undefined}
            label={'Delete All'}
            isChecked={props.allChecked}
            onChange={props.onCheckboxChange}
        />
    const columns = [masterCheckbox, 'Name', 'Description', 'Image', 'Actions']
    const style = {color: 'darkgoldenrod'}


    const renderImage = image => {
        if (image) {
            if (width < 1000) {
                return <span onClick={toggleDisplay} className={'image-span'}>Image</span>
            } else {
                return `Image`
            }
        } else {
            return <Tooltip hoveringText={'Please provide image'}><span
                style={style}>Missing Image</span></Tooltip>
        }
    }

    return (
        <SimpleTable ref={tableRef} columns={columns} items={props.categories}>
            <tbody>
            {props.categories.map(category => (
                <tr key={category.id}>
                    <td data-th=''>
                        <CustomCheckbox
                            value={category.id}
                            onChange={props.onCheckboxChange}
                            isChecked={props.allChecked}
                            resetCheckbox={props.resetCheckboxes}
                            name={category.id}
                        />
                    </td>
                    <td data-th={'Name'}>
                        {category.name}
                    </td>
                    <td data-th={'Description'}>
                        {category.description ? <span>{category.description}</span> :
                            <Tooltip hoveringText={'Please provide description'}><span style={style}>'Missing description'</span></Tooltip>}
                    </td>
                    <td data-th={'Image'}>
                        <Fragment>
                            {renderImage(category.imageUrl)}
                            <Modal displayModal={display} closeModal={toggleDisplay}>
                                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                                <img src={category.imageUrl} width={250} height={250} alt={'category image'}/>
                            </Modal></Fragment>
                    </td>
                    <td data-th={'Actions'}>
                        {props.actions(category.id)}
                    </td>
                </tr>
            ))}
            </tbody>
        </SimpleTable>
    );
};

export default CategoryTable;