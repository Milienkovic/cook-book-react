import React, {useEffect, useState} from 'react';
import './ScrollButton.css';

const ScrollButton = props => {
    const [buttonUp, setButtonUp] = useState(false);
    const [buttonDown, setButtonDown] = useState(false);
    const [scrollPosition, setScrollPosition] = useState(0);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll, false);

        return () => {
            window.removeEventListener('scroll', handleScroll, false);
        }
    }, []);

    const handleScroll = () => {
        setScrollPosition(window.pageYOffset);
    };


    const checkScroll = () => {
        if (scrollPosition >= 0 && scrollPosition < props.reference.current.offsetTop - 150) {
            setButtonDown(true);
            setButtonUp(false);
        } else if (scrollPosition > props.reference.current.offsetTop - 650) {
            setButtonDown(false);
            setButtonUp(true);
        } else {
            setButtonDown(false);
            setButtonUp(false);
        }
    };

    useEffect(() => {
        checkScroll();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [scrollPosition]);

    const scrollTo = () => {
        window.scrollTo({top: props.reference.current.offsetTop - 150, behavior: 'smooth'})
    };

    return (
        <div>
            <div className=''>
                {buttonUp &&
                <button className='scrollButton' onClick={scrollTo}>
                    <i className="far fa-arrow-alt-circle-up fa-3x text-white"/>
                </button>}
                {buttonDown &&
                <button className='scrollButton' onClick={scrollTo}>
                    <i className="far fa-arrow-alt-circle-down fa-3x text-white"/>
                </button>}
            </div>
        </div>
    );
}

export default ScrollButton;