import React from 'react';
import './SearchBar.css';

const SearchBar = (props) => {

    return (
        <div className="search--bar__container">
            <input id='search-btn' type='checkbox'/>
            <label htmlFor='search-btn' onClick={props.cancelSearch}>Show search bar</label>
            <input id='search-bar'
                   type='text'
                   value={props.initialValue}
                   placeholder='Search...'
                   onChange={props.searchHandler}
            />
        </div>
    );
};

export default SearchBar;