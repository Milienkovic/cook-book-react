import {useCallback, useReducer} from 'react';

const INPUT_CHANGE = 'INPUT_CHANGE';
const SET_DATA = 'SET_DATA';
const RESET = 'RESET';


const formReducer = (state, action) => {
    switch (action.type) {
        case INPUT_CHANGE:
            let isFormValid = true;
            for (const inputId in state.inputs) {
                if (!state.inputs.hasOwnProperty(inputId)) {
                    continue;
                }
                if (inputId === action.inputId) {
                    isFormValid = isFormValid && action.isValid;
                } else {
                    isFormValid = isFormValid && state.inputs[inputId].isValid;
                }
            }
            return {
                ...state,
                inputs: {
                    ...state.inputs,
                    [action.inputId]: {value: action.value, isValid: action.isValid}
                },
                isValid: isFormValid
            }
        case SET_DATA:
            return {
                inputs: action.inputs,
                isValid: action.isFormValid
            }

        case RESET:
            return {
                inputs: action.inputs,
                isValid: action.isFormValid
            }

        default:
            return state;
    }

}

export const useForm = (initialInputs, initialFormValidity) => {
    const [formState, dispatch] = useReducer(formReducer, {
        inputs: initialInputs,
        isValid: initialFormValidity
    });

    const inputHandler = useCallback((inputId, value, isValid) => {
        dispatch({type: INPUT_CHANGE, value, isValid, inputId})
    }, [])

    const setFormData = useCallback((inputData, isFormValid) => {
        dispatch({
            type: SET_DATA,
            inputs: inputData,
            isFormValid
        })
    }, [])

    const resetForm = useCallback(() => {
        dispatch({
            type: RESET,
            inputs: initialInputs,
            isValid: initialFormValidity
        })
    }, [initialFormValidity, initialInputs]);

    return [formState, inputHandler, setFormData, resetForm];
}