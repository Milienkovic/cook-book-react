import {useCallback, useReducer} from "react";
import _ from 'lodash';

const ADD = 'ADD';
const REMOVE = 'REMOVE';
const UPDATE = 'UPDATE';
const CLEAR = 'CLEAR';
const SET_ARRAY = 'SET_ARRAY';

const arrayReducer = (state, action) => {
    switch (action.type) {
        case ADD:
            let duplicateIndices = [];
            state.array.filter((item, index) => {
                if (_.isEqual(item, action.item)) {
                    duplicateIndices.push(index);
                }
                return _.isEqual(item, action.item);
            });
            if (duplicateIndices.length > 0) {
                return {
                    ...state,
                    duplicate: {
                        isDuplicate: true,
                        indices: duplicateIndices
                    },
                };
            } else {
                return {
                    ...state,
                    array: [
                        ...state.array,
                        action.item],
                    duplicate: {
                        isDuplicate: false,
                        indices: []
                    }
                };
            }

        case REMOVE:
            const temp = [...state.array];
            return {
                ...state,
                array: temp.filter((item, index) => index !== action.index),
                duplicate: {
                    isDuplicate: false,
                    indices: []
                }
            };

        case UPDATE:
            const tempArray = [...state.array];
            let indices = [];
            tempArray.filter((i, index) => {
                if (_.isEqual(i, action.item) && index !== action.index) {
                    indices.push(index);
                }
                return (_.isEqual(i, action.item) && index !== action.index);
            });
            if (indices.length > 0) {
                return {
                    ...state,
                    duplicate: {
                        isDuplicate: true,
                        indices
                    },
                };
            } else {
                tempArray.splice(action.index, 1, action.item);
                return {
                    ...state,
                    array: tempArray,
                    duplicate: {
                        isDuplicate: false,
                        indices: []
                    }
                };
            }

        case CLEAR:
            return {
                ...state,
                array: [],
                duplicate: {
                    isDuplicate: false,
                    indices: []
                }
            };

        case SET_ARRAY:
            return {
                ...state,
                array: action.array,
                duplicate: {
                    isDuplicate: false,
                    indices: []
                }
            };

        default:
            return state;
    }
}

export const useArray = (initArray) => {

    const [arrayState, dispatch] = useReducer(arrayReducer, {
        array: initArray,
        duplicate: {isDuplicate: false, indices: []},
    });

    const add = useCallback((item) => {
        dispatch({
            type: ADD,
            item
        });
    }, []);

    const remove = useCallback((index) => {
        dispatch({
            type: REMOVE,
            index
        });
    }, []);

    const update = useCallback((index, item) => {
        dispatch({
            type: UPDATE,
            index,
            item
        });
    }, []);

    const setArray = useCallback((array) => {
        dispatch({
            type: SET_ARRAY,
            array
        });
    }, []);

    const clear = useCallback(() => {
        dispatch({type: CLEAR})
    }, []);


    return {arrayState, add, update, remove, clear, setArray};
}