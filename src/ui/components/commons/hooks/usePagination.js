export const usePagination = (currentPage, itemsPerPage, collection) => {
    if (collection) {
        const endIndex = currentPage * itemsPerPage;
        const startIndex = endIndex - itemsPerPage;
        return collection.slice(startIndex, endIndex);
    } else
        return [];
}