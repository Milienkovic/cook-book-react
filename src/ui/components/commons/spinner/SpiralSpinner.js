import React from 'react';

import './SpiralSpinner.css';

const SpiralSpinner = () => {
    return (
        <div className={'spiral--spinner__container'}>
            <div className="spiral--spinner__loading">
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
                <div className="spiral--spinner__circle"/>
            </div>
        </div>
    );
};

export default SpiralSpinner;