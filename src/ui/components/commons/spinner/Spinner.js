import React, {Fragment} from 'react';
import './Spinner.css';
import Backdrop from "../modal/backdrop/Backdrop";

const Spinner = (props) => {
    return (
        <Fragment>
            <div className='spinner-container'>
                <div className='loader'>
                    Loading...
                </div>
            </div>
            <Backdrop display={props.display}/>
        </Fragment>
    );
};

export default Spinner;