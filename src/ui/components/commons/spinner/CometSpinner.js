import React from 'react';

import './CometSpinner.css';

const CometSpinner = () => {
    return (
        <div className={'comet--container'}>
            <div className="comet--loader">
                <div className="comet--face">
                    <div className="comet--circle"/>
                </div>
                <div className="comet--face">
                    <div className="comet--circle"/>
                </div>
            </div>
        </div>
    );
};

export default CometSpinner;