import React from 'react';
import {Redirect, Route} from "react-router-dom";
import {useSelector} from "react-redux";
import AppUrls from "../../../../utils/urls/appUrls";

const AuthenticatedRoute = ({component: Component,  ...otherProps}) => {
    const {isAuth} = useSelector(state=>({
        isAuth: !!state.auth.token && state.auth.roles
    }));

    return (
        <Route {...otherProps}
               render={props =>
                   isAuth ? <Component{...props}/>
                       : <Redirect to={AppUrls.HOME}/>
               }/>

    );
};

export default AuthenticatedRoute;