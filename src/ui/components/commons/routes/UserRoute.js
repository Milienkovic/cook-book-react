import React from 'react';
import {Redirect, Route} from "react-router-dom";
import {HOME} from "../../../../utils/urls/appUrls";
import {useSelector} from "react-redux";

const UserRoute = ({component: Component,  ...otherProps}) => {
    const {isUser} = useSelector(state =>({
        isUser: state.auth.roles && state.auth.roles.includes('USER'),
    }));

    return (
        <Route {...otherProps}
               render={props =>
                   isUser ? <Component {...props}/>
                       : <Redirect to={HOME}/>}/>

    );
};

export default UserRoute;