import React, {Fragment} from 'react';
import {Redirect, Route} from "react-router-dom";
import {useSelector} from "react-redux";
import AppUrls from "../../../../utils/urls/appUrls";

const AdminRoute = ({component: Component, ...otherProps}) => {
    const {isAdmin} = useSelector(state=>({
        isAdmin: state.auth.roles && state.auth.roles.includes('ADMIN')
    }));

    const renderComponent = () => {
        return (
            <Route {...otherProps}
                   render={props =>
                       isAdmin ?
                           <Component {...props}/>
                           : <Redirect to={AppUrls.HOME}/>
                   }
            />
        );
    }
    return (
        <Fragment>
            {renderComponent()}
        </Fragment>
    );
};

export default AdminRoute;