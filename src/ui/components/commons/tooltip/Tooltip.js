import React, {useState} from 'react';
import "./Tooltip.css";
const Tooltip = props => {
    const [isHovering, setIsHovering] = useState(false);

    const toggleIsHovering = () =>{
        setIsHovering(prevState => !prevState);
    };
    return (
        <div className='tooltip-container'
             onMouseEnter={toggleIsHovering}
             onMouseLeave={toggleIsHovering}>
            {props.children}
            {isHovering && <p className='tool-tip-text'>{props.hoveringText}</p>}
        </div>
    );
};

export default Tooltip;