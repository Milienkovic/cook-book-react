import React, {Component} from 'react';
import './Progress.css';

const Progress = props => {
    return (
        <div className='upload-progress-bar'>
            <div className='upload-progress'
                 style={{width: props.progress + '%'}}>
            </div>
        </div>
    );
}

export default Progress;