import React, {Fragment, useEffect, useState} from 'react';
import './ProgressBar.css';
import Backdrop from "../../../modal/backdrop/Backdrop";

const ProgressBar = (props) => {
    const [offset, setOffset] = useState(440);

    useEffect(() => {
        setOffset(440 - (440 * props.percentage) / 100);
    }, [offset, props.percentage]);

    return (
        <Fragment>
            <Backdrop display={props.display}/>
            <div className='progress-container'>
                <div className='progress-card'>
                    <div className='progress-box'>
                        <div className='progress-percent'>
                            <svg>
                                <circle cx='70' cy='70' r='70'/>
                                <circle cx='70' cy='70' r='70' style={{strokeDashoffset: `${offset}`}}/>
                            </svg>
                            <div className='progress-number'>
                                <h2>{props.percentage.toFixed(1)}<span>%</span></h2>
                            </div>
                        </div>
                        <h2 className='text'>{props.text}</h2>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default ProgressBar;