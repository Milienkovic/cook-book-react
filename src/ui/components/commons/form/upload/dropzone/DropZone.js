import React, {useRef, useState} from 'react';
import uploadImage from '../../../../../../assets/images/cloud_upload-24px.svg';
import './DropZone.css';

const DropZone = props => {
    const fileInputRef = useRef();
    const [highlight, setHighlight] = useState(false);

    const onFileDialog = () => {
        if (props.disabled) return;
        setHighlight(true);
        fileInputRef.current.click();
    }

    const onChange = event => {
        if (props.disabled) return;
        if (props.onFileAdded) {
            props.onFileAdded(event.target.files[0]);
        }
        setHighlight(false);
    }

    return (
        <div className={`dropzone ${highlight ? 'highlight' : ''}`}
             onClick={onFileDialog}
             style={{cursor: props.disabled ? 'default' : 'pointer'}}>
            <img
                alt='upload'
                className='upload-icon'
                src={uploadImage}
            />
            <input
                type='file'
                className={'file-input'}
                onChange={onChange}
                ref={fileInputRef}
            />
            <span className={'dropzone--text'}>{props.text}</span>
        </div>
    );
}

export default DropZone;