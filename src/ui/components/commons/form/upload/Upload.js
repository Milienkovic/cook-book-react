import React from 'react';
import DropZone from "./dropzone/DropZone";
import './Upload.css';

const Upload = props => {
    const renderActions = () => {
        if (!!props.file) {
            return (
                <button
                    className='upload-button'
                    onClick={props.onImageRemoved}>Clear</button>
            );
        }
    }
    
    return (
        <div className='upload-container' >
            <div className='upload-card' style={{backgroundImage:`url(${props.backgroundImage})`}}>
                <span className='title'>{props.title}</span>
                <div className='upload'>
                    <div className='upload-content'>
                        <DropZone
                            onFileAdded={props.onImageAdded}
                            disabled={!!props.file}
                            text={props.text}
                        />
                    </div>
                    {props.file && <div className='files'>
                        <div className='upload-row'>
                            <span className='filename'>{props.file.name}</span>
                        </div>
                    </div>}
                    <div className='file-actions'>{renderActions()}</div>
                </div>
            </div>
        </div>
    );
}

export default Upload;