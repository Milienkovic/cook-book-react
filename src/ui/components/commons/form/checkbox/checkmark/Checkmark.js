import React from 'react';
import './Checkmark.css';

const Checkmark = (props) => {
    return (
        <div className={'checkmark__container'}>
            <i className={'cbx__title'}>{props.title}</i>
            <div className='cbx-container'>
                <input className="inp-cbx" id="cbx" type="checkbox"
                       style={{display: "none"}}
                       checked={props.isChecked}
                       onChange={props.onChange}/>
                <label className="cbx" htmlFor="cbx">
                <span>
                    <svg viewBox="0 0 12 9">
                      <polyline points="1 5 4 8 11 1"/>
                    </svg>
                </span>
                    <span>{props.content}</span>
                </label>

            </div>
        </div>
    );
};

export default Checkmark;