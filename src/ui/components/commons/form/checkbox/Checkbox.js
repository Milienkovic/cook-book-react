import React, {Fragment, useEffect, useState} from 'react';
import "./Checkbox.css";

const Checkbox = (props) => {
    const [checked, setChecked] = useState(false);
    const {value, name, onChange, label, isChecked, resetCheckbox} = props;

    const toggleChecked = () => {
        setChecked(!checked);
    };

    useEffect(() => {
        if (isChecked || resetCheckbox) {
            setChecked(false);
        }
    }, [isChecked, resetCheckbox]);

    let checkedBox = isChecked ? isChecked : checked;

    if (name === 'checkAll') {
        checkedBox = isChecked;
    }

    return (
        <Fragment>
            <div className="custom-control custom-switch">
                <input
                    id={`cb-${value}`}
                    type="checkbox"
                    className="custom-control-input success"
                    value={value}
                    checked={checkedBox}
                    onChange={onChange}
                    onClick={toggleChecked}
                    disabled={isChecked && name !== 'checkAll'}
                    name={name}
                />
                <label className="custom-control-label font-weight-bold"
                       htmlFor={`cb-${value}`}>
                    <i className='text-sm-center font-italic font-weight-light'>{label}</i>
                </label>
            </div>
        </Fragment>
    );
};

export default Checkbox;