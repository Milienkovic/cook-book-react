import React, {Fragment, useEffect, useState} from 'react';
import './CustomCheckbox.css';
const CustomCheckbox = (props) => {
    const [checked, setChecked] = useState(false);
    const {value, name, onChange, isChecked, resetCheckbox} = props;

    const toggleChecked = () => {
        setChecked(prevState => !prevState);
    };

    useEffect(() => {
        if (isChecked || resetCheckbox) {
            setChecked(false);
        }
    }, [isChecked, resetCheckbox]);

    let checkedBox = isChecked ? isChecked : checked;

    if (name === 'checkAll') {
        checkedBox = isChecked;
    }
    return (
        <Fragment>
            <div className='checkbox'>
                <label className='checkbox__container'>
                    <input className='checkbox__toggle'
                           id={`cb-${value}`}
                           type='checkbox'
                           value={value}
                           name={name}
                           onChange={onChange}
                           checked={checkedBox}
                           disabled={isChecked && name!=='checkAll'}
                           onClick={toggleChecked}
                    />

                    <span className='checkbox__checker'/>
                    <span className='checkbox__cross'/>
                    <span className='checkbox__ok'/>
                    <svg className='checkbox__bg' space='preserve' style={{enableBackground:'new 0 0 110 43.76'}}
                         version='1.1' viewBox='0 0 110 43.76'>
                        <path className='shape'
                              d='M88.256,43.76c12.188,0,21.88-9.796,21.88-21.88S100.247,0,88.256,0c-15.745,0-20.67,12.281-33.257,12.281,S38.16,0,21.731,0C9.622,0-0.149,9.796-0.149,21.88s9.672,21.88,21.88,21.88c17.519,0,20.67-13.384,33.263-13.384,S72.784,43.76,88.256,43.76z'/>
                    </svg>
                </label>
            </div>
        </Fragment>
    );
};

export default CustomCheckbox;