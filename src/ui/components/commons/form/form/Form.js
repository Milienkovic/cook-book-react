import React from 'react';

import './Form.css';

const Form = props => {
    return (
        <div className="form--container">
            <form onSubmit={props.onSubmit} className="custom--form">
            <h1 className="text--center">{props.title}</h1>
                <h3>{props.subtitle}</h3>
                {props.children}
                <div className="form--submit">
                    {props.formSubmitAction? props.formSubmitAction
                    : <button className="btn btn--primary btn--block"
                            disabled={props.disabled}>{props.buttonText || 'Submit'}</button>}
                </div>
            </form>
        </div>
    );
};

export default Form;