import React, {Fragment, useEffect, useState} from 'react';
import './CollapsibleSection.css';

const FormSection = (props) => {
    const [active, setActive] = useState(props.isActive);

    useEffect(() => {
        if (!props.isToggable)
            setActive(true);
    }, [props.isToggable]);

    const toggleActive = () => {
        if (props.isToggable)
            setActive(prevState => !prevState);
    }

    return (
        <Fragment>
            <div className={`collapsible--section`}>
                <div className={`collapsible--section__header ${active ? 'active' : ''}`}
                     onClick={toggleActive}>
                    {props.sectionNumber && <span>{props.sectionNumber}</span>}
                    <p className={'section__name'}>{props.sectionName}</p>
                </div>
                <div className={'collapsible--content'}
                     style={{
                         height: `${active ? '100%' : '0'}`,
                     }}>
                    <div className="collapsible--content__inner--wrap">
                        {props.children}
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default FormSection;