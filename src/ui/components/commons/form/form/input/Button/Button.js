import React from 'react';
import {Link} from "react-router-dom";

import './Button.css';

const Button = props => {

    const getClassNames = () =>{
        let className = ['button',`button--${props.size || 'default'}`]
        if (props.inverse)
            className.push('button--inverse')
        if (props.danger)
            className.push('button--danger')
        if (props.primary)
            className.push('button--primary')
        if (props.success)
            className.push('button--success')
        if (props.dark)
            className.push('button--dark')
        if (props.warning)
            className.push('button--warning')
        if (props.className)
            className.push(props.className)
        return className;
    }

    const className = getClassNames().join(' ');
    if (props.href) {
        return <a
            className={className}
            href={props.href}
            style={props.style}
        >{props.children}</a>
    }

    if (props.to) {
        return <Link
            className={className}
            style={props.style}
            to={props.to}
            exact={props.exact}
        >{props.children}</Link>
    }
    return (
        <button
            className={className}
            style={props.style}
            onClick={props.onClick}
            disabled={props.disabled}
            type={props.type}
        >
            {props.children}
        </button>
    );
};

export default Button;