import React from 'react';
import './Dropdown.css';

const Dropdown = props => {

    return (
        <div className="dropdown--box">
            <label className="dropdown--select" htmlFor="dropdown">
                <select id="dropdown"
                        className={props.className}
                        disabled={props.disabled}
                        name={props.name}
                        onChange={props.onDropdownChange}
                        defaultValue={props.defaultValue}>
                    <option disabled>
                        Select an option
                    </option>
                    {props.dropdowns.map(d =>
                        <option key={d} value={d} >{d}</option>
                    )}
                </select>
            </label>
        </div>
    );
};

export default Dropdown;