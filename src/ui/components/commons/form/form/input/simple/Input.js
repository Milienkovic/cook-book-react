import React, {Fragment, useEffect, useReducer} from 'react';
import Dropdown from "../dropdown/Dropdown";
import ImageUpload from "./ImageUpload";
import Checkmark from "../../../checkbox/checkmark/Checkmark";

import '../../Form.css';

const inputReducer = (state, action) => {
    switch (action.type) {
        case 'CHANGE':
            return {
                ...state,
                value: action.value,
                isValid: true
            }
        case 'TOUCH':
            return {
                ...state,
                isTouched: true,
            }
        case 'FOCUS':
            return {
                ...state,
                isFocused: true
            }
        case 'CLEAR_FOCUS':
            return {
                ...state,
                isFocused: false
            }
        default:
            return state;
    }
}

const Input = props => {
    const [inputState, dispatch] = useReducer(inputReducer, {
        value: props.initialValue || '',
        isValid: props.initialIsValid || true,
        isTouched: false,
        isFocused: false,
        hasValue: false
    });

    useEffect(() => {
        if (inputState.value) {
            dispatch({type: 'FOCUS'});
        }
    }, [inputState.value]);

    const {id, onInput} = props;
    const {value, isValid} = inputState;

    const changeHandler = event => {
        const value = props.element === 'checkmark' || props.element === 'checkbox' ? event.target.checked : event.target.value
        dispatch({
            type: 'CHANGE',
            value: value,
            validators: props.validators
        })
    }

    const touchHandler = (event) => {
        dispatch({type: 'TOUCH'});
        if (event.target.value) {
            dispatch({type: 'FOCUS'});
        } else {
            dispatch({type: 'CLEAR_FOCUS'});
        }
    }

    const focusHandler = () => {
        dispatch({type: 'FOCUS'});
    }

    useEffect(() => {
        onInput(id, value, isValid);
    }, [id, isValid, onInput, value]);

    const getInputClassNames = () =>{
        const classNames =['form--input'];
        if (props.fieldError)
            classNames.push('error')
        if (props.className)
            classNames.push(props.className)
        if (value)
            classNames.push('filled')
        return classNames;
    }

    const inputElement = <input
        className={getInputClassNames().join(' ')}
        style={props.style}
        id={props.id}
        autoFocus={props.autofocus}
        type={props.type}
        min={props.min}
        name={props.name}
        placeholder={props.placeholder}
        disabled={props.disabled}
        onChange={changeHandler}
        onBlur={touchHandler}
        onFocus={focusHandler}
        value={inputState.value}
    />;

    const textAreaElement = <textarea
        className={getInputClassNames().join(' ')}
        style={props.style}
        id={props.id}
        autoFocus={props.autofocus}
        disabled={props.disabled}
        rows={props.rows || 3}
        name={props.name}
        placeholder={props.placeholder}
        onChange={changeHandler}
        value={inputState.value}
        onBlur={touchHandler}
        onFocus={focusHandler}
    />;

    const dropdownElement = <Dropdown
        id={props.id}
        style={props.style}
        className={getInputClassNames().join(' ')}
        name={props.name}
        autoFocus={props.autofocus}
        onDropdownChange={changeHandler}
        value={inputState.value}
        disabled={props.disabled}
        dropdowns={props.dropdowns}
        defaultValue={props.initialValue}
        onBlur={touchHandler}
        onFocus={focusHandler}
    />

    const imageUploadElement = <ImageUpload
        id={props.id}
        className={getInputClassNames().join(' ')}
        backgroundImage={props.backgroundImage}
        text={props.text}
        fieldError={props.fileError}
        onImageAdded={props.onImageAdded}
        file={props.file}
        onImageRemoved={props.onImageRemoved}
        onBlur={touchHandler}
        onFocus={focusHandler}
        value={inputState.value.name}
    />

    const checkmarkElement = <Checkmark
        title={props.title}
        content={props.content}
        className={`${props.fieldError && 'error'} ${props.className} form--input ${value && 'filled'}`}
        style={props.style}
        disabled={props.disabled}
        autoFocus={props.autofocus}
        id={props.id}
        name={props.name}
        isChecked={inputState.value}
        onBlur={touchHandler}
        onFocus={focusHandler}
        value={inputState.value}
        onChange={changeHandler}/>

    const label = props.fieldError && props.label !== undefined ?
        "* " + props.label + " : " + props.fieldError :
        props.label;

    const getClassNames= ()=>{
        const classNames = ['form--group'];
        if (props.className)
            classNames.push(props.className)
        if (inputState.isFocused)
            classNames.push('focused')
        if (props.fieldError)
            classNames.push('error')
        return classNames;
    }

    const wrappedElement =
        <div style={props.style}
            className={getClassNames().join(' ')}>
            <label htmlFor={props.id} className="form--label">{label}</label>
            {props.element === 'input' && inputElement}
            {props.element === 'textarea' && textAreaElement}
            {props.element === 'dropdown' && dropdownElement}
            {props.element === 'image' && imageUploadElement}
            {props.element === 'checkmark' && checkmarkElement}
        </div>;

    return (
        <Fragment>
            {wrappedElement}
        </Fragment>
    );
};


export default Input;