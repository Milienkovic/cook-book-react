import React, {Fragment} from 'react';
import Styles from "../../../../../../../utils/styles/styles";
import Upload from "../../../upload/Upload";
import './../../../upload/Upload.css';

const ImageUpload = ({id, text, title, label, onImageAdded, file, onImageRemoved, fieldError, backgroundImage}) => {
    const _getLabel = () => {
        if (fieldError) {
            return '* ' + label + ' : ' + fieldError;
        }
        return label;
    }
    return (
        <Fragment>
            <div className={'input__group'}>
                <label style={fieldError ? Styles.fieldErrorStyle('red', '14px') : null}>{_getLabel()}
                </label>
                <Upload
                    title={title}
                    id={id}
                    text={text}
                    onImageAdded={onImageAdded}
                    file={file}
                    backgroundImage={backgroundImage}
                    onImageRemoved={onImageRemoved}/>
            </div>
        </Fragment>
    );
};

export default ImageUpload;