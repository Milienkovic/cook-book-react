import React, {useEffect, useState} from 'react';
import DrawerToggleButton from "../sideDrawer/drawerToggleButton/DrawerToggleButton";
import NavigationItems from "../navigationItems/NavigationItems";
import {Link} from "react-router-dom";
import classNames from "classnames";
import AppUrls from "../../../../../utils/urls/appUrls";

import './Toolbar.css';

const Toolbar = (props) => {
    const [scrolled, setScrolled] = useState(false);
    const style = classNames('toolbar', {
        "shrink": scrolled
    });

    const handleScrolled = () => {
        if (window.pageYOffset > 150) {
            setScrolled(true);
        } else {
            setScrolled(false);
        }
    };
    useEffect(() => {
        handleScrolled();
        window.addEventListener('scroll', handleScrolled);
        return () => {
            window.removeEventListener('scroll', handleScrolled);
        }
    }, []);

    const renderBanner = () =>{
        return(
            <div className="banner--container">
                <div className="banner">
                    <div className="banner--text">
                        <div className="banner--heading">
                            Welcome to the Cook Book
                        </div>
                        <div className="banner--sub__heading">
                            The place where cooking starts!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    return (
        <div>
            <header className={style}>
                <DrawerToggleButton clicked={props.drawerToggleClicked}/>
                <div className='logo'><Link to={AppUrls.HOME}><span>Logo</span> Here </Link></div>
                <div className='spacer'/>
                <div className='desktop-only'>
                    <nav>
                        <NavigationItems isAuth={props.isAuth} clicked={()=>{}}/>
                    </nav>
                </div>
            </header>
            {renderBanner()}
        </div>
    );
};

export default (Toolbar);