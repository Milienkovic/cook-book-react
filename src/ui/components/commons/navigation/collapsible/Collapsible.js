import React from 'react';
import './Collapsible.css';

const Collapsible = (props) => {
    return (
        <div className="wrapper">
            <ul>
                <li>
                    <input type="checkbox" id="list-item-1" className='collapsible-checkbox'/>
                    <label htmlFor="list-item-1" className="first">{props.title}</label>
                    <ul className={'action-list'}>
                        {props.children}
                    </ul>
                </li>
            </ul>
        </div>
    );
};

export default Collapsible;