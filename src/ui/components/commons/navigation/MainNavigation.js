import React, {Fragment, useState} from 'react';
import Backdrop from "../modal/backdrop/Backdrop";
import SideDrawer from "./sideDrawer/SideDrawer";
import NavigationItems from "./navigationItems/NavigationItems";
import Toolbar from "./toolbar/Toolbar";

import './MainNavigation.css';

const MainNavigation = () => {
    const [sideDrawerOpen, setSideDrawerOpen] = useState(false);

    const openSideDrawerHandler = () => {
        setSideDrawerOpen(true);
    }
    const closeSideDrawerHandler = () => {
        setSideDrawerOpen(false);
    }
    return (
        <Fragment>
            {sideDrawerOpen && <Backdrop display={sideDrawerOpen} clicked={closeSideDrawerHandler}/>}
            <SideDrawer show={sideDrawerOpen} onClick={closeSideDrawerHandler}>
                <nav className={'side-drawer__nav'}><NavigationItems/></nav>
            </SideDrawer>
            <Toolbar drawerToggleClicked={openSideDrawerHandler}/>
        </Fragment>
    );
};

export default MainNavigation;