import React, {Fragment} from 'react';
import {NavLink} from "react-router-dom";
import './NavigationItem.css';

const NavigationItem = (props) => {
    return (
        <Fragment>
            <li className='navigation-item'>
                <NavLink activeClassName='navigation-item__active'
                         exact={props.exact}
                         to={props.link}
                         onClick={props.clicked? props.clicked : ()=>{}}>
                    {props.children}
                </NavLink>
            </li>
        </Fragment>
    );
};

export default NavigationItem;