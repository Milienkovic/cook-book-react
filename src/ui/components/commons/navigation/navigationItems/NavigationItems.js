import React, {Fragment} from 'react';
import NavigationItem from "./navigationItem/NavigationItem";
import {logout} from "../../../../../store/actions/auth/authActions";
import {useDispatch, useSelector} from "react-redux";
import AppUrls from "../../../../../utils/urls/appUrls";
import Svgs from "../../../../../utils/svgs/svgs";

import './NavigationItems.css';

const NavigationItems = (props) => {
    const {clicked} = props;
    const dispatch = useDispatch();
    const {isAdmin, isAuth} = useSelector(state=>({
        isAdmin: state.auth.roles && state.auth.roles.includes('ADMIN'),
        isAuth: !!state.auth.token
    }));

    const renderAuthenticatedContent = () => (
        !isAuth ?
            <Fragment>
                <NavigationItem link={AppUrls.LOGIN}
                                exact
                                clicked={clicked}>
                    Login
                </NavigationItem>

            </Fragment>
            :
            <Fragment>
                <NavigationItem link={AppUrls.USER_RECIPES}>My Recipes</NavigationItem>
                <NavigationItem link={AppUrls.CURRENT_USER} clicked={clicked}>
                    <i className={'user-wrapper'}>{Svgs.userIcon} Me</i>
                </NavigationItem>
                <NavigationItem
                    exact
                    link={AppUrls.LOGOUT}
                    clicked={() => {
                        clicked();
                        dispatch(logout());
                    }}>
                    Logout
                </NavigationItem>
            </Fragment>
    );

    const renderAdminContent = () => {
        if (isAdmin) {
            return (
                <Fragment>
                    <NavigationItem
                        clicked={clicked}
                        link={AppUrls.ADMIN_PANEL}>
                        Admin Panel
                    </NavigationItem>
                </Fragment>
            )
        }
    }
    return (
        <ul className='navigation-items'>
            {renderAdminContent()}
            {renderAuthenticatedContent()}
            <div className={'actions'}>
            </div>
        </ul>
    );
};

export default NavigationItems;