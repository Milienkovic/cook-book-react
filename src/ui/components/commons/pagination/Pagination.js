import React, {Fragment, useEffect, useState} from 'react';
import Svgs from "../../../../utils/svgs/svgs";
import './Pagination.css';

const Pagination = props => {
    const [pageIndex, setPageIndex] = useState(props.currentPage || 1);
    const [prevPageDisabled, setPrevPageDisabled] = useState(false);
    const [nextPageDisabled, setNextPageDisabled] = useState(false);
    const [pageNumbers, setPageNumbers] = useState([]);

    const numberOfPages = Math.ceil(props.itemsNumber / props.itemsPerPage);
    const {setCurrentPage} = props;

    useEffect(() => {
        const pageNumbers = [];
        for (let i = 0; i < numberOfPages; i++) {
            pageNumbers.push(i + 1);
        }
        setPageNumbers(pageNumbers);
        setPageIndex(1);
    }, [numberOfPages]);

    useEffect(() => {
        if (pageIndex <= 1) {
            setPrevPageDisabled(true);
            setNextPageDisabled(false);
        }
        if (pageIndex >= numberOfPages) {
            setNextPageDisabled(true);
            setPrevPageDisabled(false);
        }

        if (pageIndex > 1 && pageIndex < numberOfPages) {
            if (prevPageDisabled) setPrevPageDisabled(false);
            if (nextPageDisabled) setNextPageDisabled(false);
        }
        setCurrentPage(pageIndex);
    }, [nextPageDisabled, numberOfPages, pageIndex, prevPageDisabled, setCurrentPage]);

    const nextPage = () => {
        if (pageIndex < numberOfPages) {
            setPageIndex(prevState => prevState + 1);
        }
    }

    const prevPage = () => {
        if (pageIndex > 1) {
            setPageIndex(prevState => prevState - 1);
        }
    }

    const firstPage = () => {
        setPageIndex(1);
    }

    const lastPage = () => {
        setPageIndex(numberOfPages);
    }

    const renderPageIndices = () => {
        if (numberOfPages >= 0 && numberOfPages <= 5) {
            return (
                <Fragment>
                    {pageNumbers.map(pageNumber =>
                        <li className={`paginationItem ${pageIndex === pageNumber ? 'active-page' : ''}`}
                            key={pageNumber}
                            onClick={() => {
                                setPageIndex(pageNumber);
                            }}
                        >{pageNumber}
                        </li>
                    )}
                </Fragment>

            )
        } else {
            let startIndex = pageIndex;
            const end = startIndex + 4;
            const endIndex = end < numberOfPages ? end : numberOfPages;
            if (numberOfPages - 4 <= startIndex) {
                startIndex = numberOfPages - 4;
            }
            let indices = [startIndex];
            for (let i = startIndex + 1; i <= endIndex; i++) {
                indices.push(i);
            }

            return (
                <Fragment>
                    {indices.map(pageNumber =>
                        <li className={`paginationItem ${pageIndex === pageNumber ? 'active-page' : ''}`}
                            key={pageNumber}
                            onClick={() => {
                                setPageIndex(pageNumber);
                            }}
                        >{pageNumber}
                        </li>
                    )}
                </Fragment>
            );
        }
    }

    return (
        <Fragment>
            {numberOfPages > 0 && <div className={'pagination-container'}>
                <ul className={'pagination-list'}>
                    <Fragment>
                        {pageIndex > 4 && <li className={`pagination-item ${prevPageDisabled && 'disabled-arrow'}`}
                                              onClick={firstPage}>{Svgs.backwardIcon}</li>}
                        {pageIndex > 1 && <li className={`paginationItem ${prevPageDisabled && 'disabled-arrow'}`}
                                              onClick={prevPage}
                        >{Svgs.arrowBackIcon}</li>}
                    </Fragment>
                    {renderPageIndices()}
                    <Fragment>
                        {pageIndex < numberOfPages &&
                        <li className={`paginationItem ${nextPageDisabled ? 'disabled-arrow' : ''}`}
                            onClick={nextPage}>{Svgs.arrowForwardIcon}</li>}
                        {pageIndex < numberOfPages - 4 &&
                        <li className={`paginationItem ${nextPageDisabled && 'disabled-arrow'}`}
                            onClick={lastPage}>{Svgs.forwardIcon}</li>}
                    </Fragment>
                </ul>
            </div>}
        </Fragment>
    );
};

export default Pagination;