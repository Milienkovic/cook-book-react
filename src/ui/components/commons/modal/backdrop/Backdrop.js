import React from 'react';
import ReactDOM from 'react-dom';
import './Backdrop.css';

const Backdrop = (props) => {
    const content = props.display && <div
        className="backdrop"
        onClick={props.clicked}
    />

    return ReactDOM.createPortal(content, document.getElementById('backdrop-hook'));
};

export default Backdrop;