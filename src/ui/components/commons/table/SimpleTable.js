import React from 'react';
import './SimpleTable.css';

const SimpleTable = (props) => {
    return (
        <div className={'table-container'}>
            <table className="rwd-table">
                <thead>
                <tr>
                    {props.columns.map(col => (
                        <th key={col}>{col}</th>
                    ))}
                </tr>
                </thead>
                {props.children}
            </table>
        </div>
    );
};

export default SimpleTable;