import React from 'react';

const Star = (props) => {
    const getFillColor = () => {
        if (props.fullStar) {
            return '#FFD700';
        } else if (props.halfStar) {
            return `url(#grad-${props.id})`;
        } else
            return '#a9a9a9';
    }
    const fillColor = getFillColor();

    return (
        <svg height="24" viewBox="0 0 24 24" width="24">
            {props.halfStar && <defs>
                <linearGradient id={`grad-${props.id}`}>
                    <stop offset="0%" stopColor="gold"/>
                    <stop offset={`${props.filledPercentage}%`} stopColor="gold"/>
                    <stop offset={`${props.filledPercentage}%`} stopColor="darkgrey"/>
                    <stop offset="100%" stopColor="darkgrey"/>
                </linearGradient>
            </defs>}
            <path d="M0 0h24v24H0z" fill="none"/>
            <path
                fill={fillColor}
                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"
            />
            <path d="M0 0h24v24H0z" fill="none"/>
        </svg>
    );
};

export default Star;