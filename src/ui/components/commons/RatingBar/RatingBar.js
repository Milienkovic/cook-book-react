import React, {useEffect, useState} from 'react';

import './RatingBar.css';

const RatingBar = (props) => {
    const {onChange} = props;
    const [rating, setRating] = useState(0);
    const [selected, setSelected] = useState(false);
    const [hovered, setHovered] = useState(false);
    const [hoveredValue, setHoveredValue] = useState(0);

    useEffect(() => {
        onChange(rating);
    }, [rating, onChange]);

    const onClickHandler = event => {
        let rating;
        if (event.target.id) {
            rating = event.target.id;
        } else {
            rating = 0;
        }
        setRating(rating);
        setSelected(true);
        props.onClick();
    }

    const onHoverHandler = event => {
        setHoveredValue(event.target.id);
        setHovered(prevState => !prevState);
    }

    const classNames = (value) => {
        let className = 'rating--item__not--selected';
        if (selected && rating >= value)
            className = 'rating--item__selected';
        if (hovered && hoveredValue <= rating && className === 'rating--item__selected')
            className = 'rating--item__selected--after--hovered';
        if (hovered && hoveredValue > value)
            className = 'rating--item__selected--before--hovered';
        return className
    }
    const renderRatingBar = () => {
        let ratingItems = [];
        let className = 'rating--item__not--selected';
        for (let i = 5; i > 0; i--) {
            className = classNames(i);
            ratingItems.push(
                <svg id={i}
                     key={i}
                     visibility='visible'
                     pointerEvents='all'
                     cursor={'pointer'}
                     onClick={onClickHandler}
                     onMouseEnter={onHoverHandler}
                     onMouseLeave={onHoverHandler}
                     className={className}
                     xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 51 48">
                    <path id={i} d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"/>
                </svg>
            )
        }
        return ratingItems;
    }
    return (
        <div className={'rating--bar'} onClick={onClickHandler} id={0}>
            {renderRatingBar()}
        </div>
    );
};

export default RatingBar;