import React from 'react';
import Svgs from "../../../../utils/svgs/svgs";
import Button from "../form/form/input/Button/Button";

import './RateContainer.css';

const RateContainer = (props) => {
    return (
        <div className={'rate--container'}>
            {props.display && <div className={'rate--button__container'}>
                <span className={'rate--button__close'} onClick={props.close}>{Svgs.close}</span>
                <Button onClick={props.onButtonClick}>Rate Recipe</Button>
            </div>}
            {props.children}
        </div>
    );
};

export default RateContainer;