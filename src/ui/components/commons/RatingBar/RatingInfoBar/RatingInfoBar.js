import React from 'react';
import Star from "../Star/Star";

import './RatingInfoBar.css';

const RatingInfoBar = ({rating}) => {
        const stars = Math.floor(rating);
        const decimals = (rating - stars).toFixed(2);
        const filledPercentage = decimals * 100;


        const renderStars = () => {
            const starsArray = [];

            for (let i = 0; i < 5; i++) {
                if (i < stars) {
                    starsArray.push(<Star fullStar key={i} id={i}/>)
                }
                if (i === stars && decimals > 0.09) {
                    starsArray.push(<Star halfStar filledPercentage={filledPercentage} key={i} id={i}/>)
                }
                if (i > stars) {
                    starsArray.push(<Star emptyStar key={i} id={i}/>)
                }
            }
            return starsArray;
        }

        return (
            <div className="rating--stars__container">
                {renderStars()}
            </div>
        );
    }
;

export default RatingInfoBar;