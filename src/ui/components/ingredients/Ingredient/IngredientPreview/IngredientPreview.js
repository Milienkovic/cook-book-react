import React, {useEffect, useRef} from 'react';
import './IngredientPreview.css';

const IngredientPreview = (props) => {
    const {isDuplicate, hasError} = props;
    const animatedRef = useRef();

    useEffect(() => {
        const animationEndCallback = (e) => {
            animatedRef.current.removeEventListener('animationend', animationEndCallback);
            animatedRef.current.classList.remove('duplicate--entry');
        }

        if (animatedRef.current) {
            if (isDuplicate.isDuplicate && isDuplicate.indices.includes(props.index)) {
                animatedRef.current.addEventListener('animationend', animationEndCallback);
                animatedRef.current.classList.add('duplicate--entry');
            }
        }

    }, [isDuplicate.indices, isDuplicate.isDuplicate, props.index]);

    const getIngredientItemClasses = () => {
        const innerContainerClasses = ['ingredient--preview__item'];
        if (hasError) {
            innerContainerClasses.push('has--error');
        }
        return innerContainerClasses;
    }


    return (
        <div className={'ingredient--preview__container'}>
            <div ref={animatedRef}
                 className={'ingredient--inner__container'}>
                <div className={getIngredientItemClasses().join(' ')} id={props.id}>
                    <span className={'ingredient--name'}>{props.ingredient.name.value}</span>
                    <span className={'ingredient--amount'}>
                        <span className={'ingredient--amount__value'}>{props.ingredient.amount.value} </span>
                        <span className={'ingredient--unit'}>{props.ingredient.uom.value}</span>
                    </span>
                    {props.actions}
                </div>
            </div>
        </div>
    );
};

export default IngredientPreview;