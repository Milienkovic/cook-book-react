import React, {useEffect, useState} from 'react';
import {units} from "../../../../utils/enums/unitsOfMeasurement";
import Input from "../../commons/form/form/input/simple/Input";
import Button from "../../commons/form/form/input/Button/Button";
import {useForm} from "../../commons/hooks/useForm";
import CometSpinner from "../../commons/spinner/CometSpinner";

const initState = {
    name: {
        value: '',
        isValid: true
    },
    amount: {
        value: '',
        isValid: true
    },
    uom: {
        value: 'KILOGRAM',
        isValid: true
    }
}
const IngredientForm = (props) => {

        const [formState, inputHandler, , resetForm] = useForm(props.initValue || initState, true);
        const [disabled, setDisabled] = useState(true);
        const disabledButton = (disabled && !props.add) || (props.add && !formState.inputs.name.value);
        const [submitting, setSubmitting] = useState(false);
        const {add, updating, initValue, fieldErrors, index, isDuplicate} = props;

        useEffect(() => {
            if (submitting) {
                if (add) {
                    props.addIngredient(formState.inputs);
                    resetForm();
                } else if (updating) {
                    props.updateIngredient(props.index, formState.inputs);
                }

                setTimeout(() => {
                    setSubmitting(false);
                }, 1000)
            }
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [add, updating, resetForm, submitting]);

        const submitHandler = event => {
            event.preventDefault();
            setSubmitting(true);
            setDisabled(true);
        }

        const fieldError = (fieldName) => {
            console.log('keys')
            fieldErrors && console.log(Object.keys(fieldErrors))
            if (fieldErrors && fieldErrors.hasOwnProperty(index)) {
                let errorMessage = '';
                fieldErrors[index].filter(error => {
                    if (error.field === fieldName) {
                        errorMessage = error.message;
                    }
                    return error;
                });
                return errorMessage;
            }


            if (isDuplicate.isDuplicate && isDuplicate.indices.includes(index)) {
                return "Duplicate entry!";
            }
        }

        return (
            <div className={'ingredient--form__container'}
                 onMouseEnter={() => setDisabled(false)}
                 onMouseLeave={() => setDisabled(true)}>
                <form className={'ingredient--form'} onSubmit={submitHandler}>
                    {submitting ? <CometSpinner/> :
                        <div className={`ingredient--item ${props.modalView && 'ingredient--item__modal'}`} id={props.id}>
                            <Input id={'name'}
                                   style={props.modalView && {width: '100%'}}
                                   name={'name'}
                                   element={'input'}
                                   autofocus={props.add}
                                   type={'text'}
                                   label={'Ingredient'}
                                   onInput={inputHandler}
                                   fieldError={fieldError('name')}
                                   initialValue={(!props.add && initValue.name.value) || (props.add && formState.inputs.name.value)}
                            />
                            <Input id={'amount'}
                                   style={props.modalView && {width: '100%'}}
                                   name={'amount'}
                                   element={'input'}
                                   type={'number'}
                                   min={1}
                                   label={'Amount'}
                                   fieldError={fieldError('amount')}
                                   onInput={inputHandler}
                                   initialValue={(!props.add && initValue.amount.value) || formState.inputs.amount.value}
                            />
                            <Input id={'uom'}
                                   style={props.modalView && {width: '100%'}}
                                   onInput={inputHandler}
                                   initialValue={(!props.add && initValue.uom.value) || formState.inputs.uom.value}
                                   name={'uom'}
                                   fieldError={fieldError('unitOfMeasurement')}
                                   element={'dropdown'}
                                   dropdowns={units}
                            />
                            <Button
                                success={props.add}
                                warning={props.updating}
                                disabled={disabledButton}> {props.actionText} </Button>
                        </div>}
                </form>
            </div>
        );
    }
;

export default IngredientForm;