import React, {Fragment, useEffect, useState} from 'react';
import Button from "../commons/form/form/input/Button/Button";
import IngredientForm from "./Ingredient/IngredientForm";
import {useArray} from "../commons/hooks/useArray";
import IngredientPreview from "./Ingredient/IngredientPreview/IngredientPreview";
import Svgs from "../../../utils/svgs/svgs";
import Modal from "../commons/modal/Modal";
import {useParams} from 'react-router-dom';
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {fetchRecipeIngredients} from "../../../store/actions/recipes/ingredients/ingredientsActions";
import Spinner from "../commons/spinner/Spinner";

import './Ingredients.css';

const Ingredients = props => {
    const {onIngredientsChange, updating, fetched, onFetch, ingredients} = props;
    const dispatch = useDispatch();
    const {fetchedIngredients, loading, fieldErrors} = useSelector(state => ({
        fetchedIngredients: state.ingredients.collection,
        loading: state.ingredients.loading,
        fieldErrors: state.ingredients.fieldErrors,
    }), shallowEqual);
    const {
        arrayState,
        add: addIngredient,
        update: updateIngredient,
        remove: removeIngredient,
        setArray
    } = useArray((props.ingredients || []));
    const [displayModal, setDisplayModal] = useState(false);
    const [index, setIndex] = useState();
    const recipeId = useParams().recipeId;

    useEffect(() => {
        if ((updating && !fetched)) {
            dispatch(fetchRecipeIngredients(recipeId));
            onFetch(true);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    useEffect(() => {
        console.log(fetchedIngredients)
    }, [fetchedIngredients]);

    useEffect(() => {
        //todo sets values on each load
        if (updating && fetchedIngredients && fetched && !ingredients.length && fetchedIngredients.length) {
            setArray([...fetchedIngredients.map(ing => {
                return {
                    id: {value: ing.id, isValid: true},
                    name: {value: ing.name, isValid: true},
                    amount: {value: ing.amount, isValid: true},
                    uom: {value: ing.unitOfMeasurement, isValid: true}
                }
            })]);
        }
    }, [fetched, fetchedIngredients, ingredients, setArray, updating]);

    useEffect(() => {
        onIngredientsChange(arrayState.array);
    }, [arrayState.array, onIngredientsChange])

    //todo close modal on update
    const updateHandler = (index) => {
        setIndex(index);
        setDisplayModal(true);
    }

    const cancelUpdateHandler = () => {
        setDisplayModal(false);
    }

    const renderIngredients = () => {
        if (arrayState.array.length === 0) {
            return <h1>No ingredients available</h1>
        }

        if ((updating && fetchedIngredients && fetched) || arrayState.array.length > 0) {
            return <div className={'ingredient--items__container'}>
                <div className={'ingredient--items__inner--container'}>

                    {arrayState.array.map((ingredient, index) =>
                        <IngredientPreview key={index}
                                           index={index}
                                           hasError={fieldErrors && fieldErrors.hasOwnProperty(index)}
                                           isDuplicate={arrayState.duplicate}
                                           ingredient={ingredient} actions={
                            <div className={'ingredient--item__actions'}>
                                <Button onClick={() => updateHandler(index)}
                                        warning>{Svgs.pencilIcon}</Button>
                                <Button onClick={() => removeIngredient(index)} danger>{Svgs.deleteIcon}</Button>
                            </div>
                        }/>
                    )}
                </div>
            </div>
        }
    }

    const renderAddIngredientForm = () => {
        return <IngredientForm
            id={-1}
            addIngredient={addIngredient}
            add
            actionText={'Add'}
            initValue={null}
            index={-1}
            fieldErrors={fieldErrors}
            isDuplicate={arrayState.duplicate}
        />
    }

    if (loading || (updating && !fetchedIngredients)) {
        return <Spinner display={loading}/>
    }

    return (
        <Fragment>
            <div className={'ingredients--container'}>
                {renderAddIngredientForm()}
                {renderIngredients()}
            </div>
            <Modal displayModal={displayModal}
                   warning
                   closeModal={cancelUpdateHandler}
                   header={'Edit IngredientForm Info'}
                   footer={<div className={'ingredient--item__actions'}>
                       <Button onClick={cancelUpdateHandler} dark>Cancel</Button>
                   </div>}>
                <IngredientForm
                    updating
                    modalView
                    id={index}
                    index={index}
                    updateIngredient={updateIngredient}
                    isDuplicate={arrayState.duplicate}
                    actionText={Svgs.pencilIcon}
                    fieldErrors={fieldErrors}
                    initValue={arrayState.array[index]}/>
            </Modal>
        </Fragment>
    );
};

export default Ingredients;