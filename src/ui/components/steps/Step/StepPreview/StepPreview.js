import React, {useEffect, useRef} from 'react';

import './StepPreview.css';

const StepPreview = props => {
    const {step, isDuplicate, index, hasError} = props;

    const animatedRef = useRef();
    const getStepPreviewContainerClasses = () => {
        const stepPreviewContainerClasses = ['step--preview__container'];
        if (hasError) {
            stepPreviewContainerClasses.push('has--error');
        }
        return stepPreviewContainerClasses;
    }
    useEffect(() => {
        const animationEndHandler = () => {
            animatedRef.current.removeEventListener('animationend', animationEndHandler);
            animatedRef.current.classList.remove('duplicate--entry')
        }
        if (animatedRef.current && isDuplicate.isDuplicate && isDuplicate.indices.includes(index)) {
            animatedRef.current.addEventListener('animationend', animationEndHandler);
            animatedRef.current.classList.add('duplicate--entry');
        }

    }, [index, isDuplicate.indices, isDuplicate.isDuplicate]);


    return (
        <div className={getStepPreviewContainerClasses().join(' ')} ref={animatedRef}>
            <span className={'step--number'}>{step.stepNumber.value}</span>
            <span className={'step--description'}>
                {step.description.value}
            </span>
            {step.mandatory.value && <span className={'step--mandatory'}/>}
            <footer>{props.actions}</footer>
        </div>
    );
};

export default StepPreview;