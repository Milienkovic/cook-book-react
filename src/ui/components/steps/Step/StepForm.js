import React, {useEffect, useState} from 'react';
import Input from "../../commons/form/form/input/simple/Input";
import {useForm} from "../../commons/hooks/useForm";
import Button from "../../commons/form/form/input/Button/Button";
import CometSpinner from "../../commons/spinner/CometSpinner";

import './StepForm.css';

const initState = {
    stepNumber: {
        value: '',
        isValid: true
    },
    description: {
        value: '',
        isValid: true
    },
    mandatory: {
        value: false,
        isValid: true
    }
}

const StepForm = props => {
    const [formState, inputHandler, , resetForm] = useForm(props.initValue || initState, true);
    const [submitted, setSubmitted] = useState(false);

    const {add, update, addStep, updateStep, index, fieldErrors, isDuplicate} = props;
    const submitHandler = event => {
        event.preventDefault();
        setSubmitted(true);
    }

    useEffect(() => {
        if (submitted) {
            if (add) {
                addStep(formState.inputs);
            } else if (update) {
                updateStep(index, formState.inputs);
            }
            setTimeout(() => {
                setSubmitted(false);
            }, 1000)
        }

        resetForm();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [add, index, submitted, update]);

    const fieldError = fieldName => {
        if (fieldErrors && fieldErrors.hasOwnProperty(index)) {
            let errorMessage = '';
            fieldErrors[index].filter(error => {
                if (error.field === fieldName) {
                    errorMessage = error.message;
                }
                return error;
            });
            return errorMessage;
        }

        if (isDuplicate && isDuplicate.indices.includes(index)) {
            return 'Duplicate entry!';
        }
    }

    return (
        <div className={'step--form__container'}>
            {submitted ? <CometSpinner/>
                :
                <form onSubmit={submitHandler} className={'step--form'}>
                    <Input
                        id={'stepNumber'}
                        name={'stepNumber'}
                        onInput={inputHandler}
                        autofocus={props.add}
                        type={'number'}
                        min={1}
                        element={'input'}
                        label={'Step Number: '}
                        initialValue={formState.inputs.stepNumber.value}
                        fieldError={fieldError('stepNo')}
                    />
                    <Input
                        id={'description'}
                        name={'description'}
                        onInput={inputHandler}
                        element={'textarea'}
                        label={'Description: '}
                        initialValue={formState.inputs.description.value}
                        fieldError={fieldError('description')}
                    />
                    <Input
                        id={'mandatory'}
                        name={'mandatory'}
                        onInput={inputHandler}
                        element={'checkmark'}
                        label={"Mandatory: "}
                        content={'Is Step Mandatory?'}
                        fieldError={fieldError('mandatory')}
                        initialValue={formState.inputs.mandatory.value}
                    />
                    <div className={'Step--form__submit'}>
                        <Button warning={update}>{add ? 'Add' : "Update"}</Button>
                    </div>
                </form>}
        </div>
    );
};

export default StepForm;