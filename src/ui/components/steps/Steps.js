import React, {Fragment, useEffect, useState} from 'react';
import {useArray} from "../commons/hooks/useArray";
import StepForm from "./Step/StepForm";
import StepPreview from "./Step/StepPreview/StepPreview";
import Button from "../commons/form/form/input/Button/Button";
import Svgs from "../../../utils/svgs/svgs";
import Modal from "../commons/modal/Modal";
import {useParams} from 'react-router-dom';
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import Spinner from "../commons/spinner/Spinner";
import {fetchRecipeSteps} from "../../../store/actions/recipes/steps/stepsActions";

import './Steps.css';

const Steps = props => {
    const dispatch = useDispatch();
    const {loading, fetchedSteps, fieldErrors} = useSelector(state => ({
        loading: state.steps.loading,
        fetchedSteps: state.steps.collection,
        fieldErrors: state.steps.fieldErrors
    }), shallowEqual);

    const {
        arrayState: steps,
        add: addStep,
        update: updateStep,
        remove: removeStep,
        setArray
    } = useArray(props.steps || []);
    const [displayModal, setDisplayModal] = useState(false);
    const [index, setIndex] = useState();
    const recipeId = useParams().recipeId;

    const {onStepsChange, updating, fetched, onFetch} = props;
    useEffect(() => {
        console.log(steps.array);
    }, [steps.array])

    useEffect(() => {
        if ((updating && !fetched)) {
            dispatch(fetchRecipeSteps(recipeId));
            onFetch(true);
        }
    }, [dispatch, fetched, fetchedSteps, onFetch, recipeId, updating]);

    useEffect(() => {
        if (updating && fetchedSteps && fetched && !props.steps.length && fetchedSteps.length) {
            setArray([...fetchedSteps.map(step => {
                return {
                    id: {
                        value: step.id,
                        isValid: true
                    },
                    stepNumber: {
                        value: +step.stepNo,
                        isValid: true
                    },
                    description: {
                        value: step.description,
                        isValid: true
                    },
                    mandatory: {
                        value: step.mandatory,
                        isValid: true
                    }
                }
            })]);
        }
    }, [props.steps, fetched, fetchedSteps, props.steps.length, setArray, updating]);

    useEffect(() => {
        onStepsChange(steps.array);
    }, [steps, onStepsChange]);

    const displayModalHandler = (index) => {
        setDisplayModal(true);
        setIndex(index);
    }

    const cancelModalHandler = () => {
        setDisplayModal(false);
    }

    const renderSteps = () => {
        if (steps.array.length === 0) {
            return <h1>No steps available</h1>
        }

        if ((updating && fetchedSteps && fetched) || steps.array.length > 0) {
            return steps.array.map((step, index) =>
                <StepPreview
                    step={step}
                    key={index}
                    index={index}
                    id={index}
                    isDuplicate={steps.duplicate}
                    fieldErrors={fieldErrors}
                    hasError={fieldErrors && fieldErrors.hasOwnProperty(index)}
                    actions={
                        <Fragment>
                            <Button onClick={() => displayModalHandler(index)}
                                    warning>{Svgs.pencilIcon}</Button>
                            <Button onClick={() => removeStep(index)} danger>{Svgs.deleteIcon}</Button>
                        </Fragment>}
                />)
        }
    }

    if (loading || (updating && !fetchedSteps)) {
        return <Spinner display={loading}/>
    }

    return (
        <div className={'steps--container'}>
            <div className={'steps--inner--container'}>
                <StepForm addStep={addStep}
                                        add
                                        isDuplicate={steps.duplicate}/>
                <div className={'steps--preview'}>
                    <div className={'steps--preview__inner--container'}>
                        {renderSteps()}
                    </div>
                </div>
                <Modal displayModal={displayModal}
                       closeModal={cancelModalHandler}
                       warning
                       header={'Edit Step\'s Info'}
                       footer={<Button dark onClick={cancelModalHandler}>Cancel</Button>}>
                    <div className={'step--form__modal'}>
                        <StepForm
                            update
                            index={index}
                            updateStep={updateStep}
                            initValue={steps.array[index]}
                            fieldErrors={fieldErrors}
                            isDuplicate={steps.duplicate}
                        />
                    </div>
                </Modal>
            </div>
        </div>
    );
};

export default Steps;