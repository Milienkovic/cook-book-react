export class Complexity {

    static VERY_EASY = 'VERY_EASY';
    static EASY = 'EASY';
    static EASY_MEDIUM = 'EASY_MEDIUM';
    static MEDIUM = 'MEDIUM';
    static MEDIUM_HARD = 'MEDIUM_HARD';
    static HARD = 'HARD';
    static VERY_HARD = 'VERY_HARD';

    static complexities=[Complexity.VERY_EASY, Complexity.EASY, Complexity.EASY_MEDIUM, Complexity.MEDIUM, Complexity.MEDIUM_HARD, Complexity.HARD, Complexity.VERY_HARD];

    static complexityColor = complexity => {
        switch (complexity) {
            case this.VERY_EASY:
                return {color: 'greenyellow'}
            case this.EASY:
                return {color: 'green'}
            case this.EASY_MEDIUM:
                return {color: 'darkolivegreen'}
            case this.MEDIUM:
                return {color: 'orange'}
            case this.MEDIUM_HARD:
                return {color: 'orangered'}
            case this.HARD:
                return {color: 'red'}
            case this.VERY_HARD:
                return {color: 'darkred'}
            default:
                return {color: 'white'};
        }
    }
}