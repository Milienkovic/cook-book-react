import axios from 'axios';
import Tokens from "./tokens";

const instance = axios.create({});

const requestHandler = async request =>  {
    if (isHandlerEnabled(request)) {
        if (Tokens.isAuthTokenExpired()) {
            await Tokens.getNewTokenPromise()
                .then(res => {
                    console.log('check tokens')
                    console.log(res.headers.authorization===Tokens.getAuthToken())
                })
                .catch(err => Promise.reject(err));
        }
    }
    request.headers['Authorization'] = Tokens.getAuthToken();
    return request;
}

const isHandlerEnabled = (config = {}) => !!config.hasOwnProperty('handled') && config.handled;

instance.interceptors.request.use(request => requestHandler(request));

export default (instance);