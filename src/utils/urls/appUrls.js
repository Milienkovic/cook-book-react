export default class AppUrls {
    static HOME = '/';
    static ADMIN_PANEL = '/adminPanel';
    static CATEGORIES_BOARD = '/categoriesBoard';
    static EDIT_CATEGORY = '/editCategory/:id';
    static EDIT_CATEGORY_APP_URL = (id) => `/editCategory/${id}`;
    static CREATE_CATEGORY = '/createCategory';

    static USER_RECIPES = '/userRecipes'

    static LOGIN = "/login";
    static LOGOUT = "/logout";
    static REGISTRATION = "/registration";
    static VERIFY_ACCOUNT = "/registration/confirm";
    static REQUEST_NEW_VERIFICATION_TOKEN = '/requestToken';
    static REQUEST_RESET_PASSWORD_TOKEN = '/changePasswordRequest';
    static CONFIRM_PASSWORD_RESET_REQUEST = '/confirmReset';
    static CHANGE_PASSWORD_REQUEST = '/changePassword';
    static UPDATE_PASSWORD_URL = token => `/updatePassword?token=${token}`;
    static UPDATE_PASSWORD = '/updatePassword'

    static USERS_BOARD = '/usersBoard';
    static CURRENT_USER = '/users/me';

    static CATEGORY_RECIPES = `/:id/recipes`;
    static CATEGORY_RECIPES_URL = id => `/${id}/recipes`;

    static RECIPE_VIEW = '/categories/:categoryId/recipes/:recipeId';
    static RECIPE_VIEW_URL = (categoryId, recipeId) => `/categories/${categoryId}/recipes/${recipeId}`;
    static CREATE_RECIPE = '/:categoryId/createRecipe';
    static CREATE_RECIPE_URL = categoryId => `/${categoryId}/createRecipe`;
    static EDIT_RECIPE = '/recipes/:recipeId';
    static EDIT_RECIPE_URL = id => `/recipes/${id}`;

    static RECIPE_INGREDIENTS = '/:recipeId/ingredients';
    static RECIPE_INGREDIENTS_URL = recipeId => `/${recipeId}/ingredients`;
    static RECIPE_STEPS = '/:recipeId/steps';
    static RECIPE_STEPS_URL = recipeId => `${recipeId}/steps`;
}