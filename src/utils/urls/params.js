export default class Params {
    static getParamFromUrl = (url, param) => {
        const params = new URL(url).searchParams;
        return +params.get(param);
    }

    static getParamFromSearchParams = (searchParams, param) => {
        if (searchParams.charAt(0) === '?') {
            return new URLSearchParams(searchParams.substring(1)).get(param);
        }
        return new URLSearchParams(searchParams).get(param);
    }
}
