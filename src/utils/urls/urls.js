export default class Urls {
    static Auth = class {
        static loginUrl = () => '/login';
        static registrationUrl = () => '/registration';
        static confirmRegistrationUrl = token => `/registration/confirm?token=${token}`;
        static newRegistrationTokenUrl = email => `/registration/resendRegistrationToken?email=${email}`;
        static exchangeAuthTokenUrl = (refreshToken) => `/exchangeToken?token=${refreshToken}`;
        static exchangeChangePasswordPrivilegeTokenUrl = (refreshToken) => `/exchangePrivilegeToken?token=${refreshToken}`;
        static resetPasswordTokenUrl = email => `/resetPassword?email=${email}`;
        static changePasswordUrl = (id, token) => `/changePassword?id=${id}&token=${token}`;
        static updatePasswordUrl = token => `/updatePassword?token=${token}`;
    }

    static Categories = class {
        static categoriesUrl = () => '/categories';
        static categoryUrl = id => `/categories?id=${id}`;
        static uploadCategoryImageUrl = id => `/upload?category=${id}`;
        static deleteCategoriesUrl = () => '/categories/all';
    }

    static Users = class {
        static usersUrl = () => '/users';
        static updateUserUrl = id => `/users?id=${id}`;
        static getUserByEmailUrl = () => `/users/me`;
    }

    static Recipes = class {
        static getRecipe = id => `/recipes?id=${id}`;
        static getRecipePreview = id => `/recipes/preview?id=${id}`;
        static createRecipe = categoryId => `/recipes?category=${categoryId}`;
        static updateRecipe = id => `/recipes?id=${id}`;
        static deleteRecipe = id => `/recipes?id=${id}`;
        static getCategoryRecipes = categoryId => `/recipes?category=${categoryId}`;
        static getCategoryRecipesByComplexity = (categoryId, complexity) => `/recipes?category=${categoryId}&complexity=${complexity}`
        static getUserRecipes = () => `/recipes/mine`;
        static getUserRecipesByCategory = categoryId => `/recipes/mine?category=${categoryId}`;
        static deleteRecipes = () => '/recipes/all';
        static rateRecipe = recipeId => `/recipes/rate?id=${recipeId}`;
        static uploadRecipeImageUrl = id => `/upload?recipe=${id}`;
    }

    static Ingredients = class {
        static getIngredient = id => `/ingredients?id=${id}`;
        static getIngredientsByRecipe = recipeId => `/ingredients?recipe=${recipeId}`
        static updateIngredient = id => `/ingredients?id=${id}`;
        static updateIngredients = recipeId => `/ingredients/all?recipe=${recipeId}`;
        static createIngredient = recipeId => `/ingredients?recipe=${recipeId}`;
        static deleteIngredient = id => `/ingredients?id=${id}`;
        static deleteIngredients = recipeId => `/ingredients/all?recipe=${recipeId}`;
        static createIngredients = recipeId => `/ingredients/all?recipe=${recipeId}`;
    }

    static Steps = class {
        static getStep = id => `/steps?id=${id}`;
        static getStepsByRecipe = recipeId => `/steps?recipe=${recipeId}`;
        static createStep = recipeId => `/steps?recipe=${recipeId}`;
        static updateStep = id => `/steps?id=${id}`;
        static deleteStep = id => `/steps?id=${id}`;
        static deleteSteps = recipeId => `/steps/all?recipe=${recipeId}`;
        static createSteps = recipeId => `/steps/all?recipe=${recipeId}`;
        static updateSteps = recipeId => `/steps/all?recipe=${recipeId}`;
    }

    static Comments = class{
        static getComment = commentId => `/comments?id=${commentId}`;
        static getRecipeComments = recipeId => `/comments/recipe?recipe=${recipeId}`;
        static commentRecipe = recipeId => `/comments?recipe=${recipeId}`;
        static updateComment = commentID => `/comments?id=${commentID}`;
        static deleteComment = commentID => `/comments?id=${commentID}`;
        static likeComment = commendId => `/comments/like?comment=${commendId}`;
        static replyToComment = commentId => `/replies?comment=${commentId}`;
    }
}