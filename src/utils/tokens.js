import axios from 'axios';
import Cookies from 'universal-cookie';
import jwtDecode from 'jwt-decode';
import Urls from "./urls/urls";

export default class Tokens {
    static AUTH_TOKEN = 'authToken';
    static REFRESH_TOKEN = 'refresh-token';
    static cookies = new Cookies();

    static getNewTokenPromise = () => {
        return new Promise((resolve, reject) => {
            axios.post(Urls.Auth.exchangeAuthTokenUrl(Tokens.getRefreshToken()))
                .then(response => {
                    const {authorization} = response.headers;
                    Tokens.storeAuthToken(authorization);
                    resolve(response);
                })
                .catch(error => {
                    //todo logout
                    Tokens.clearTokens();
                    reject(error);
                })
        })
    }

    static getAuthToken = () => {
        return localStorage.getItem(Tokens.AUTH_TOKEN) || null;
    }
    static getRefreshToken = () => {
        const refreshToken = this.cookies.get(Tokens.REFRESH_TOKEN);
        if (refreshToken) {
            const decodedRefreshToken = atob(refreshToken);
            const index = decodedRefreshToken.indexOf('$', 0);
            return decodedRefreshToken.substring(index + 1);
        }
        return null;
    }

    static storeAuthToken = token => {
        localStorage.setItem(Tokens.AUTH_TOKEN, token);
    }

    static getDecodedAuthToken = () => {
        return jwtDecode(Tokens.getAuthToken());
    }

    static decodeAuthToken = token =>{
        return jwtDecode(token);
    }
    static clearTokens = () => {
        localStorage.removeItem(Tokens.AUTH_TOKEN);
        this.cookies.remove(Tokens.REFRESH_TOKEN);
    }

    static isAuthTokenExpired = () => {
        const decodedToken = jwtDecode(Tokens.getAuthToken());
        const now = Date.now();
        return decodedToken.exp < now / 1000;
    }

    static calculateExpiryDate = expiresInMilliseconds => new Date(new Date().getMilliseconds() + expiresInMilliseconds)

    static addAuthentication = () => {
        return {
            headers: {
                Authorization: Tokens.getAuthToken()
            }
        }
    }

    static isAuthenticated = () => {
        return !!Tokens.getAuthToken();
    }
}