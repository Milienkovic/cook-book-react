export const updateObject = (oldObject, updatedProps) => {
    return {
        ...oldObject,
        ...updatedProps
    }
};

export const scrollTo = (ref) => ref ? window.scrollTo({top: ref.current.offsetTop - 150, behavior: "smooth"}) : null;

