export default class Paginations {
    static pageItems = (currentPage, itemsPerPage, collection) => {
        const endIndex = currentPage * itemsPerPage;
        const startIndex = endIndex - itemsPerPage;
        return collection.slice(startIndex, endIndex);
    }

    static numberOfPages = (itemsPerPage, numberOfItems) => {
        return Math.ceil(numberOfItems / itemsPerPage);
    }
}