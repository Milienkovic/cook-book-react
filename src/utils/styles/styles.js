export default class Styles {
    static mainErrorStyle = (color, fontSize) =>({
        color:`${color}`,
        fontSize:`${fontSize}`,
    });

    static fieldErrorStyle = (color, fontSize) =>({
        color: color,
        fontSize: fontSize
    })
}