import AuthTypes from "../actions/auth/authTypes";
import GeneralTypes from "../actions/general/generalTypes";

const initState = {
    token: null,
    expiresIn: null,
    roles: null,
    loading: false,
    error: null,
    fieldErrors: null,
    message: null,
};

const actionStart = (state, action) => {
    return {
        ...state,
        error: null,
        fieldErrors: null,
        loading: true,
        message: null
    }
};

const actionFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        fieldErrors: action.fieldErrors,
        loading: false
    }
};
const authStart = (state, action) => actionStart(state, action);

const authSuccess = (state, action) => {
    return {
        ...state,
        token: action.token,
        expiresIn: action.expiresIn,
        roles: action.roles,
        loading: false,
    }
};

const authFail = (state, action) => actionFail(state, action);

const registrationStart = (state, action) => actionStart(state, action);

const registrationSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        message: action.message
    }
};

const changePasswordRequestStart = (state, action) => actionStart(state, action);

const changePasswordRequestSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        message: action.message
    }
}

const changePasswordRequestFail = (state, action) => actionFail(state, action)

const registrationFail = (state, action) => actionFail(state, action);

const updatePasswordStart = (state, action) => actionStart(state, action);

const updatePasswordSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        message: action.message
    }
}
const updatePasswordFail = (state, action) => actionFail(state, action);

const authLogout = (state, action) => {
    return {
        ...state,
        token: null,
        expiresIn: null,
        roles: null,
        message: null,
    }
};

const verifyAccountStart = (state, action) => actionStart(state, action);

const verifyAccountSuccess = (state, action) => {
    return {
        ...state,
        message: action.message,
        loading: false
    }
}

const verifyAccountFail = (state, action) => actionFail(state, action);

const requestNewVerificationTokenStart = (state, action) => actionStart(state, action)

const requestNewVerificationTokenSuccess = (state, action) => {
    return {
        ...state,
        message: action.message,
        loading: false
    }
}
const requestNewVerificationTokenFail = (state, action) => actionFail(state, action);

const purgeErrors = (state, action) => {
    return {
        ...state,
        error: null,
        fieldErrors: null,
        message: null
    }
};

const requestNewPasswordTokenStart = (state, action) => actionStart(state, action);

const requestNewPasswordTokenSuccess = (state, action) => {
    return {
        ...state,
        message: action.message,
        loading: false
    }
}
const requestNewPasswordTokenFail = (state, action) => actionFail(state, action);

export default function authReducer(state = initState, action) {
    switch (action.type) {
        /** auth */
        case AuthTypes.AUTH_START:
            return authStart(state, action);
        case AuthTypes.AUTH_SUCCESS:
            return authSuccess(state, action);
        case AuthTypes.AUTH_FAIL:
            return authFail(state, action);
        case AuthTypes.AUTH_LOGOUT:
            return authLogout(state, action);
        /** registration */
        case AuthTypes.REGISTRATION_START:
            return registrationStart(state, action);
        case AuthTypes.REGISTRATION_SUCCESS:
            return registrationSuccess(state, action);
        case AuthTypes.REGISTRATION_FAIL:
            return registrationFail(state, action);
        /** verify account */
        case AuthTypes.VERIFY_ACCOUNT_START:
            return verifyAccountStart(state, action);
        case AuthTypes.VERIFY_ACCOUNT_SUCCESS:
            return verifyAccountSuccess(state, action);
        case AuthTypes.VERIFY_ACCOUNT_FAIL:
            return verifyAccountFail(state, action);
        /** new verification token */
        case AuthTypes.REQUEST_NEW_VERIFICATION_TOKEN_START:
            return requestNewVerificationTokenStart(state, action);
        case AuthTypes.REQUEST_NEW_VERIFICATION_TOKEN_SUCCESS:
            return requestNewVerificationTokenSuccess(state, action);
        case AuthTypes.REQUEST_NEW_VERIFICATION_TOKEN_FAIL:
            return requestNewVerificationTokenFail(state, action);
        /** new password token */
        case AuthTypes.REQUEST_NEW_PASSWORD_TOKEN_START:
            return requestNewPasswordTokenStart(state, action);
        case AuthTypes.REQUEST_NEW_PASSWORD_TOKEN_SUCCESS:
            return requestNewPasswordTokenSuccess(state, action);
        case AuthTypes.REQUEST_NEW_PASSWORD_TOKEN_FAIL:
            return requestNewPasswordTokenFail(state, action)
        /** change password request */
        case AuthTypes.CHANGE_PASSWORD_REQUEST_START:
            return changePasswordRequestStart(state, action);
        case AuthTypes.CHANGE_PASSWORD_REQUEST_SUCCESS:
            return changePasswordRequestSuccess(state, action);
        case AuthTypes.CHANGE_PASSWORD_REQUEST_FAIL:
            return changePasswordRequestFail(state, action);
        /** update password */
        case AuthTypes.UPDATE_PASSWORD_START:
            return updatePasswordStart(state, action);
        case AuthTypes.UPDATE_PASSWORD_SUCCESS:
            return updatePasswordSuccess(state, action);
        case AuthTypes.UPDATE_PASSWORD_FAIL:
            return updatePasswordFail(state, action);
        case GeneralTypes.PURGE_ERRORS:
            return purgeErrors(state, action);
        default:
            return state;
    }
}