import IngredientTypes from "../actions/recipes/ingredients/ingredientTypes";
import ingredientTypes from "../actions/recipes/ingredients/ingredientTypes";
import GeneralTypes from "../actions/general/generalTypes";

const initState = {
    collection: null,
    data: null,
    loading: false,
    deleted: null,
    error: null,
    fieldErrors: null,
    message: null,
}

const actionStart = (state, action) => {
    return {
        ...state,
        error: null,
        fieldErrors: null,
        message: null,
        loading: true
    }
}

const actionSuccess = (state, action, propName, propValue) => {
    return {
        ...state,
        [propName]: propValue,
        loading: false
    }
}

const actionFail = (state, action) => {
    return {
        ...state,
        loading: false,
        error: action.error,
        fieldErrors: action.fieldErrors,
    }
}

const fetchIngredientStart = (state, action) => actionStart(state, action);
const fetchIngredientSuccess = (state, action) => actionSuccess(state, action, 'data', action.ingredient);
const fetchIngredientFail = (state, action) => actionFail(state, action);

const fetchRecipeIngredientsStart = (state, action) => actionStart(state, action);
const fetchRecipeIngredientsSuccess = (state, action) => actionSuccess(state, action, 'collection', action.ingredients);
const fetchRecipeIngredientsFail = (state, action) => actionFail(state, action);

const mutateIngredientStart = (state, action) => actionStart(state, action);
const mutateIngredientSuccess = (state, action) => actionSuccess(state, action, 'data', action.ingredient);
const deleteIngredientSuccess = (state, action) => actionSuccess(state, action, 'deleted', action.id);
const mutateIngredientFail = (state, action) => actionFail(state, action);

const createIngredientsStart = (state, action) => actionStart(state, action);
const createIngredientsSuccess = (state, action) => actionSuccess(state, action, 'message', action.message);
const createIngredientsFail = (state, action) => actionFail(state, action);

const deleteIngredientsStart = (state, action) => actionStart(state, action);
const deleteIngredientsSuccess = (state, action) => actionSuccess(state, action, 'deleted', action.indices);
const deleteIngredientsFail = (state, action) => actionFail(state, action);

export function ingredientsReducer(state = initState, action) {

    switch (action.type) {
        case IngredientTypes.FETCH_INGREDIENT_START:
            return fetchIngredientStart(state, action);

        case IngredientTypes.FETCH_INGREDIENT_SUCCESS:
            return fetchIngredientSuccess(state, action);

        case ingredientTypes.FETCH_INGREDIENT_FAIL:
            return fetchIngredientFail(state, action);

        case IngredientTypes.FETCH_RECIPE_INGREDIENTS_START:
            return fetchRecipeIngredientsStart(state, action);

        case IngredientTypes.FETCH_RECIPE_INGREDIENTS_SUCCESS:
            return fetchRecipeIngredientsSuccess(state, action);

        case IngredientTypes.FETCH_RECIPE_INGREDIENTS_FAIL:
            return fetchRecipeIngredientsFail(state, action);

        case IngredientTypes.MUTATE_INGREDIENT_START:
            return mutateIngredientStart(state, action);

        case IngredientTypes.CREATE_INGREDIENT_SUCCESS:
        case IngredientTypes.UPDATE_INGREDIENT_SUCCESS:
            return mutateIngredientSuccess(state, action);

        case IngredientTypes.DELETE_INGREDIENT_SUCCESS:
            return deleteIngredientSuccess(state, action);

        case IngredientTypes.MUTATE_INGREDIENT_FAIL:
            return mutateIngredientFail(state, action);

        case IngredientTypes.CREATE_INGREDIENTS_START:
            return createIngredientsStart(state, action);

        case IngredientTypes.CREATE_INGREDIENTS_SUCCESS:
        case IngredientTypes.UPDATE_INGREDIENTS_SUCCESS:
            return createIngredientsSuccess(state, action);

        case IngredientTypes.CREATE_INGREDIENTS_FAIL:
            return createIngredientsFail(state, action);

        case IngredientTypes.DELETE_INGREDIENTS_START:
            return deleteIngredientsStart(state, action);

        case IngredientTypes.DELETE_INGREDIENTS_SUCCESS:
            return deleteIngredientsSuccess(state, action);

        case IngredientTypes.DELETE_INGREDIENTS_FAIL:
            return deleteIngredientsFail(state, action);

        case GeneralTypes.PURGE_ERRORS:
            return {
                ...state,
                error: null,
                fieldErrors: null,
                message: null
            }

        default:
            return {
                ...state,
            }

    }
}