import UserTypes from "../actions/users/userTypes";
import _ from 'lodash';
import GeneralTypes from "../actions/general/generalTypes";

const initState = {
    loading: false,
    data: null,
    collection: null,
    error: null,
    fieldErrors: null,
    message: null,
    deleted: null,
    manipulated: false
}


const _actionStart = (state, action) => {
    return {
        ...state,
        loading: true,
        error: null,
        fieldErrors: null,
        message: null,
        deleted: null,
    }
}

const _actionFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        fieldErrors: action.fieldErrors,
        loading: false
    }
}

const _fetchActionSuccess = (state, action, propName, propValue) => {
    return {
        ...state,
        [propName]: propValue,
        loading: false,
        manipulated: false
    }
}

const _mutateActionSuccess = (state, action, propName, propValue) =>{
    return{
        ...state,
        [propName]: propValue,
        loading: false,
        manipulated: true
    }
}

const fetchUserStart = (state, action) => _actionStart(state, action);
const fetchUserFail = (state, action) => _actionFail(state, action);
const fetchUsersStart = (state, action) => _actionStart(state, action);
const fetchUsersFail = (state, action) => _actionFail(state, action);
const mutateUserStart = (state, action) => _actionStart(state, action);
const mutateUserFail = (state, action) => _actionFail(state, action);
const mutateUserSuccess = (state, action) => _mutateActionSuccess(state, action, 'message', action.message);
const fetchUserSuccess = (state, action) => _fetchActionSuccess(state, action, 'data', action.user);
const fetchUsersSuccess = (state, action) => _fetchActionSuccess(state, action, 'collection', _.mapKeys(action.users, 'id'));
const deleteUserStart = (state, action) => _actionStart(state, action);

const deleteUserSuccess = (state, action) => {
    return {
        ...state,
        message: action.message,
        loading: false,
        deleted: action.email,
        manipulated: true
        //todo remove deleted user
    }
};
const deleteUserFail = (state, action) => _actionFail(state, action);

const purgeErrors = (state, action) =>{
        return {
            ...state,
            error: null,
            fieldErrors: null,
            message: null
        }
}

export function usersReducer(state = initState, action) {
    switch (action.type) {
        case UserTypes.FETCH_USER_START:
            return fetchUserStart(state, action);

        case UserTypes.FETCH_USER_SUCCESS:
            return fetchUserSuccess(state, action);
        case UserTypes.FETCH_USER_FAIL:
            return fetchUserFail(state, action);

        case UserTypes.FETCH_USERS_START:
            return fetchUsersStart(state, action);

        case UserTypes.FETCH_USERS_SUCCESS:
            return fetchUsersSuccess(state, action);

        case UserTypes.FETCH_USERS_FAIL:
            return fetchUsersFail(state, action);

        case UserTypes.MUTATE_USER_START:
            return mutateUserStart(state, action);

        case UserTypes.MUTATE_USER_SUCCESS:
            return mutateUserSuccess(state, action);

        case UserTypes.MUTATE_USER_FAIL:
            return mutateUserFail(state, action);

        case UserTypes.DELETE_USER_START:
            return deleteUserStart(state, action);

        case UserTypes.DELETE_USER_SUCCESS:
            return deleteUserSuccess(state, action);

        case UserTypes.DELETE_USER_FAIL:
            return deleteUserFail(state, action);
        case GeneralTypes.PURGE_ERRORS:
            return purgeErrors(state, action);

        default:
            return state;
    }
}