import {CommentTypes} from "../actions/comments/commentTypes";
import _ from 'lodash';

const initState = {
    loading: false,
    collection: null,
    data: null,
    message: null,
    error: null,
    fieldErrors: null
}

const actionStart = (state, action) => {
    return {
        ...state,
        loading: true,
        message: null,
        error: null,
        fieldErrors: null
    }
}

const actionSuccess = (state, action, propName, propValue) => {
    return {
        ...state,
        loading: false,
        [propName]: propValue
    }
}

const actionFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        fieldErrors: action.fieldErrors,
        loading: false,
    }
}

const fetchCommentStart = (state, action) => actionStart(state, action);
const fetchCommentSuccess = (state, action) => actionSuccess(state, action, 'data', action.comment);
const fetchCommentFail = (state, action) => actionFail(state, action);

const fetchCommentsStart = (state, action) => actionStart(state, action);
const fetchCommentsSuccess = (state, action) => actionSuccess(state, action, 'collection', _.mapKeys('id', action.comments));
const fetchCommentsFail = (state, action) => actionFail(state, action);

const mutateCommentStart = (state, action) => actionStart(state, action);
const mutateCommentSuccess = (state, action) => actionSuccess(state, action, 'message', action.message);
const deleteCommentSuccess = (state, action) => actionSuccess(state, action, 'deleted', action.id);
const mutateCommentFail = (state, action) => actionFail(state, action);
const likeCommentSuccess = (state, action) => actionSuccess(state, action, 'message', action.message);

export function commentsReducer(state = initState, action) {

    switch (action.type) {
        case CommentTypes.FETCH_COMMENT_START:
            return fetchCommentStart(state, action);
        case CommentTypes.FETCH_COMMENT_SUCCESS:
            return fetchCommentSuccess(state, action);
        case CommentTypes.FETCH_COMMENT_FAIL:
            return fetchCommentFail(state, action);
        case CommentTypes.FETCH_COMMENTS_START:
            return fetchCommentsStart(state, action);
        case CommentTypes.FETCH_COMMENTS_SUCCESS:
            return fetchCommentsSuccess(state, action);
        case CommentTypes.FETCH_COMMENTS_FAIL:
            return fetchCommentsFail(state, action);
        case CommentTypes.MUTATE_COMMENT_START:
            return mutateCommentStart(state, action);
        case CommentTypes.CREATE_COMMENT_SUCCESS:
        case CommentTypes.UPDATE_COMMENT_SUCCESS:
            return mutateCommentSuccess(state, action);
        case CommentTypes.MUTATE_COMMENT_FAIL:
            return mutateCommentFail(state, action);
        case CommentTypes.DELETE_COMMENT_SUCCESS:
            return deleteCommentSuccess(state, action);
        case CommentTypes.LIKE_COMMENT_SUCCESS:
            return likeCommentSuccess(state, action);
        default:
            return state;
    }

}