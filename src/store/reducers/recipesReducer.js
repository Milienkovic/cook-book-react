import RecipeTypes from "../actions/recipes/recipeTypes";
import _ from 'lodash';

const initState = {
    collection: null,
    data: null,
    fieldErrors: null,
    error: null,
    loading: false,
    deleting: false,
    message: false,
    manipulated: false,
    mineCollection: null,
}

const actionStart = (state, action) => {
    return {
        ...state,
        error: null,
        fieldErrors: null,
        message: null,
        loading: true,
    }
}

const actionFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        fieldErrors: action.fieldErrors,
        loading: false
    }
}

const actionSuccess = (state, action, propName, propValue, isManipulated = false) => {
    return {
        ...state,
        [propName]: propValue,
        loading: false,
        manipulated: isManipulated,
    }
}


const fetchRecipeStart = (state, action) => actionStart(state, action);
const fetchRecipeSuccess = (state, action) => actionSuccess(state, action, 'data', action.recipe);
const fetchRecipeFail = (state, action) => actionFail(state, action);

const mutateRecipeStart = (state, action) => actionStart(state, action);
const mutateRecipeFail = (state, action) => actionFail(state, action);
const mutateRecipeSuccess = (state, action) => actionSuccess(state, action, 'data', action.recipe, true);
const deleteRecipeSuccess = (state, action) => actionSuccess(state, action, 'deleted', action.id, true);
const deleteRecipesSuccess = (state, action) => actionSuccess(state, action, 'deleted', action.indices, true);

const fetchUserRecipesStart = (state, action) => actionStart(state, action);
const fetchUserRecipesSuccess = (state, action) => actionSuccess(state, action, 'mineCollection', _.mapKeys(action.recipes, 'id'));
const fetchUserRecipesFail = (state, action) => actionFail(state, action);

const fetchCategoryRecipesStart = (state, action) => actionStart(state, action);
const fetchCategoryRecipesSuccess = (state, action) => actionSuccess(state, action, 'collection', _.mapKeys(action.recipes, 'id'));
const fetchCategoryRecipesFail = (state, action) => actionFail(state, action);

const fetchCategoryRecipesByComplexityStart = (state, action) => actionStart(state, action);
const fetchCategoryRecipesByComplexitySuccess = (state, action) => actionSuccess(state, action, 'collection', _.mapKeys(action.recipes, 'id'));
const fetchCategoryRecipesByComplexityFail = (state, action) => actionFail(state, action);

const fetchUserRecipesByCategoryStart = (state, action) => actionStart(state, action);
const fetchUserRecipesByCategorySuccess = (state, action) => actionSuccess(state, action, 'mineCollection', _.mapKeys(action.recipes, 'id'));
const fetchUserRecipesByCategoryFail = (state, action) => actionFail(state, action);

const rateRecipeStart = (state, action) => actionStart(state, action);
const rateRecipeSuccess = (state, action) => actionSuccess(state, action, 'message', action.message, true);
const rateRecipeFail = (state, action) => actionFail(state, action);

export function recipesReducer(state = initState, action) {

    switch (action.type) {

        case RecipeTypes.FETCH_RECIPE_START:
            return fetchRecipeStart(state, action);

        case RecipeTypes.FETCH_RECIPE_SUCCESS:
        case RecipeTypes.FETCH_RECIPE_PREVIEW_SUCCESS:
            return fetchRecipeSuccess(state, action);

        case RecipeTypes.FETCH_RECIPE_FAIL:
            return fetchRecipeFail(state, action);

        case RecipeTypes.MUTATE_RECIPE_START:
            return mutateRecipeStart(state, action);

        case RecipeTypes.CREATE_RECIPE_SUCCESS:
        case RecipeTypes.UPDATE_RECIPE_SUCCESS:
            return mutateRecipeSuccess(state, action);

        case RecipeTypes.DELETE_RECIPE_SUCCESS:
            return deleteRecipeSuccess(state, action);

        case RecipeTypes.DELETE_RECIPES_SUCCESS:
            return deleteRecipesSuccess(state, action);

        case RecipeTypes.MUTATE_RECIPE_FAIL:
            return mutateRecipeFail(state, action);

        case RecipeTypes.FETCH_USER_RECIPES_START:
            return fetchUserRecipesStart(state, action);

        case RecipeTypes.FETCH_USER_RECIPES_SUCCESS:
            return fetchUserRecipesSuccess(state, action);

        case RecipeTypes.FETCH_USER_RECIPES_FAIL:
            return fetchUserRecipesFail(state, action);

        case RecipeTypes.FETCH_CATEGORY_RECIPES_START:
            return fetchCategoryRecipesStart(state, action);

        case RecipeTypes.FETCH_CATEGORY_RECIPES_SUCCESS:
            return fetchCategoryRecipesSuccess(state, action);

        case RecipeTypes.FETCH_CATEGORY_RECIPES_FAIL:
            return fetchCategoryRecipesFail(state, action);

        case RecipeTypes.FETCH_CATEGORY_RECIPES_BY_COMPLEXITY_START:
            return fetchCategoryRecipesByComplexityStart(state, action);

        case RecipeTypes.FETCH_CATEGORY_RECIPES_BY_COMPLEXITY_SUCCESS:
            return fetchCategoryRecipesByComplexitySuccess(state, action);

        case RecipeTypes.FETCH_CATEGORY_RECIPES_BY_COMPLEXITY_FAIL:
            return fetchCategoryRecipesByComplexityFail(state, action);

        case RecipeTypes.FETCH_USER_RECIPES_BY_CATEGORY_START:
            return fetchUserRecipesByCategoryStart(state, action);

        case RecipeTypes.FETCH_USER_RECIPES_BY_CATEGORY_SUCCESS:
            return fetchUserRecipesByCategorySuccess(state, action);

        case RecipeTypes.FETCH_USER_RECIPES_BY_CATEGORY_FAIL:
            return fetchUserRecipesByCategoryFail(state, action);

        case RecipeTypes.RATE_RECIPE_START:
            return rateRecipeStart(state, action);

        case RecipeTypes.RATE_RECIPE_SUCCESS:
            return rateRecipeSuccess(state, action);

        case RecipeTypes.RATE_RECIPE_FAIL:
            return rateRecipeFail(state, action);

        case RecipeTypes.CLEAR_RECIPE:
            return {
                ...state,
                data: null
            }
        default:
            return state;
    }
}