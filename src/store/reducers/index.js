import {combineReducers} from "redux";
import authReducer from "./authReducer";
import {categoriesReducer} from "./categoriesReducer";
import {filesReducer} from "./filesReducer";
import {usersReducer} from "./usersReducer";
import {recipesReducer} from "./recipesReducer";
import {stepsReducer} from "./stepsReducer";
import {ingredientsReducer} from "./ingredientsReducer";
import {commentsReducer} from "./commentsReducer";

export const rootReducer = combineReducers({
    auth: authReducer,
    categories: categoriesReducer,
    files: filesReducer,
    users: usersReducer,
    recipes: recipesReducer,
    steps: stepsReducer,
    ingredients: ingredientsReducer,
    comments: commentsReducer,

});
