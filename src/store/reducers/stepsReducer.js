import StepTypes from "../actions/recipes/steps/stepTypes";
import GeneralTypes from "../actions/general/generalTypes";

const initState = {
    loading: false,
    collection: null,
    data: null,
    message: null,
    error: null,
    fieldErrors: null,
    deleted: null,
}

const actionStart = (state, action) => {
    return {
        ...state,
        error: null,
        fieldErrors: null,
        message: null,
        loading: true
    }
}

const actionFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        fieldErrors: action.fieldErrors,
        loading: false
    }
}

const actionSuccess = (state, action, propName, propValue) => {
    return {
        ...state,
        [propName]: propValue,
        loading: false
    }
}

const fetchStepStart = (state, action) => actionStart(state, action);
const fetchStepSuccess = (state, action) => actionSuccess(state, action, 'data', action.step);
const fetchStepFail = (state, aciton) => actionFail(state, aciton);

const fetchRecipeStepsStart = (state, action) => actionStart(state, action);
const fetchRecipeStepsSuccess = (state, action) => actionSuccess(state, action, 'collection', action.steps);
const fetchRecipeStepsFail = (state, action) => actionFail(state, action);

const mutateStepStart = (state, action) => actionStart(state, action);
const mutateStepSuccess = (state, action) => actionSuccess(state, action, 'data', action.step);
const deleteStepSuccess = (state, action) => actionSuccess(state, action, 'deleted', action.id);
const mutateStepFail = (state, action) => actionFail(state, action);

const createStepsStart = (state, action) => actionStart(state, action);
const createStepsSuccess = (state, action) => actionSuccess(state, action, 'message', action.message);
const createStepsFail = (state, action) => actionFail(state, action);

const deleteStepsStart = (state, action) => actionStart(state, action);
const deleteStepsSuccess = (state, action) => actionSuccess(state, action, 'deleted', action.indices);
const deleteStepsFail = (state, action) => actionFail(state, action);


export function stepsReducer(state = initState, action) {

    switch (action.type) {
        case StepTypes.FETCH_STEP_START:
            return fetchStepStart(state, action);

        case StepTypes.FETCH_STEP_SUCCESS:
            return fetchStepSuccess(state, action);

        case StepTypes.FETCH_STEP_FAIL:
            return fetchStepFail(state, action);

        case StepTypes.FETCH_RECIPE_STEPS_START:
            return fetchRecipeStepsStart(state, action);

        case StepTypes.FETCH_RECIPE_STEPS_SUCCESS:
            return fetchRecipeStepsSuccess(state, action);

        case StepTypes.FETCH_RECIPE_STEPS_FAIL:
            return fetchRecipeStepsFail(state, action);

        case StepTypes.MUTATE_STEP_START:
            return mutateStepStart(state, action);

        case StepTypes.CREATE_STEP_SUCCESS:
        case StepTypes.UPDATE_STEP_SUCCESS:
            return mutateStepSuccess(state, action);

        case StepTypes.DELETE_STEP_SUCCESS:
            return deleteStepSuccess(state, action);

        case StepTypes.MUTATE_STEP_FAIL:
            return mutateStepFail(state, action);

        case StepTypes.CREATE_STEPS_START:
            return createStepsStart(state, action);

        case StepTypes.CREATE_STEPS_SUCCESS:
        case StepTypes.UPDATE_STEPS_SUCCESS:
            return createStepsSuccess(state, action);

        case StepTypes.CREATE_STEPS_FAIL:
            return createStepsFail(state, action);

        case StepTypes.DELETE_STEPS_START:
            return deleteStepsStart(state, action);

        case StepTypes.DELETE_STEPS_SUCCESS:
            return deleteStepsSuccess(state, action);

        case StepTypes.DELETE_STEPS_FAIL:
            return deleteStepsFail(state, action);

        case GeneralTypes.PURGE_ERRORS:
            return {
                ...state,
                error: null,
                fieldErrors: null,
                message: null
            }
        default:
            return {
                ...state
            }
    }
}