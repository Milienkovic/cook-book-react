import GeneralTypes from "../actions/general/generalTypes";

const initState = {
    uploading: false,
    message: null,
    error: null,
    uploadProgress: 0
}

export function filesReducer(state = initState, action) {
    switch (action.type) {
        case GeneralTypes.FILE_UPLOAD_START:
            return {
                ...state,
                error: null,
                uploading: true,
                message: null,
                uploadProgress: 0
            };
        case GeneralTypes.FILE_UPLOAD_SUCCESS:
            return {
                ...state,
                uploading: false,
                message: action.message,
                uploadProgress: 100
            };
        case GeneralTypes.FILE_UPLOAD_FAIL:
            return {
                ...state,
                error: action.error,
                uploading: false,
                uploadProgress: 0
            };

        case GeneralTypes.FILE_UPLOAD_PROGRESS:{
            return {
                ...state,
                uploading: true,
                uploadProgress: action.uploadProgress,
                message: 'Uploading in progress...'
            }
        }
        case GeneralTypes.PURGE_ERRORS:{
            return {
                ...state,
                error: null,
                message: null
            }
        }
        default:
            return state;
    }

}