import CategoryTypes from "../actions/categories/categoryTypes";
import _ from "lodash";
import GeneralTypes from "../actions/general/generalTypes";

const categoryInitState = {
    collection: null,
    data: null,
    error: null,
    fieldErrors: null,
    loading: false,
    deleted: null,
    message: null,
    manipulated: false,
};

const _actionStart = (state, action) => {
    return {
        ...state,
        error: null,
        fieldErrors: null,
        loading: true,
        message: null,
    }
}

const _actionFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        fieldErrors: action.fieldErrors,
        loading: false
    }
}

const fetchCategoryStart = (state, action) => _actionStart(state, action);

const fetchCategorySuccess = (state, action) => {
    return {
        ...state,
        data: action.category,
        loading: false,
        manipulated: false
    }
}
const fetchCategoryFail = (state, action) => _actionFail(state, action);

const mutateCategoryStart = (state, action) => _actionStart(state, action);

const mutateCategorySuccess = (state, action) => {
    return {
        ...state,
        data: action.category,
        loading: false,
        manipulated: true
    }
}

const deleteCategorySuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        deleted: action.id,
        collection: _.omit(state.collection, action.id),
        manipulated: true
    }
}
const mutateCategoryFail = (state, action) => _actionFail(state, action);

const fetchCategoriesStart = (state, action) => _actionStart(state, action);
const fetchCategoriesSuccess = (state, action) => {
    return {
        ...state,
        collection: _.mapKeys(action.categories, 'id'),
        loading: false,
        manipulated: false
    }
}
const fetchCategoriesFail = (state, action) => _actionFail(state, action);

const purgeErrors = (state, action) => {
    return {
        ...state,
        error: null,
        fieldErrors: null,
        message: null
    }
}
const deleteCategoriesSuccess = (state, action) => {
    return {
        ...state,
        deleted: action.indices.indices,
        collection: _.omit(state.collection, action.indices.indices),
        manipulated: true
    }
}

const clearCategory = (state, action) => {
    return {
        ...state,
        data: null
    }
}

export function categoriesReducer(state = categoryInitState, action) {
    switch (action.type) {

        case CategoryTypes.FETCH_CATEGORY_START:
            return fetchCategoryStart(state, action);

        case CategoryTypes.FETCH_CATEGORY_FAIL:
            return fetchCategoryFail(state, action);

        case CategoryTypes.FETCH_CATEGORY_SUCCESS:
            return fetchCategorySuccess(state, action);

        case CategoryTypes.MUTATE_CATEGORY_START:
            return mutateCategoryStart(state, action);

        case CategoryTypes.MUTATE_CATEGORY_FAIL:
            return mutateCategoryFail(state, action);

        case CategoryTypes.CREATE_CATEGORY_SUCCESS:
        case CategoryTypes.UPDATE_CATEGORY_SUCCESS:
            return mutateCategorySuccess(state, action);

        case CategoryTypes.DELETE_CATEGORY_SUCCESS:
            return deleteCategorySuccess(state, action);

        case CategoryTypes.FETCH_CATEGORIES_SUCCESS:
            return fetchCategoriesSuccess(state, action);

        case CategoryTypes.FETCH_CATEGORIES_FAIL:
            return fetchCategoriesFail(state, action);

        case CategoryTypes.FETCH_CATEGORIES_START:
            return fetchCategoriesStart(state, action);

        case CategoryTypes.DELETE_CATEGORIES_SUCCESS:
            return deleteCategoriesSuccess(state, action);

        case CategoryTypes.CLEAR_CATEGORY:
            return clearCategory(state, action);

        case CategoryTypes.CATEGORY_MANIPULATED:
            return {
                ...state,
                manipulated: true
            }

        case GeneralTypes.PURGE_ERRORS:
            return purgeErrors(state, action);

        default:
            return {...state}
    }
}

