import axios from './../../../utils/axios-instance';
import UserTypes from "./userTypes";
import Urls from "../../../utils/urls/urls";
import {logout} from "../auth/authActions";

const _actionStart = type => {
    return {
        type: type,
    }
}

const _actionFail = (type, error, fieldErrors) => {
    return {
        type: type,
        error: error,
        fieldErrors: fieldErrors
    }
}

const _fetchActionSuccess = (type, propName, propValue) => {
    return {
        type: type,
        [propName]: propValue
    }
}
const fetchUserStart = () => _actionStart(UserTypes.FETCH_USER_START);

const fetchUserSuccess = user => _fetchActionSuccess(UserTypes.FETCH_USER_SUCCESS, 'user', user);

const fetchUserFail = (error) => _actionFail(UserTypes.FETCH_USER_FAIL, error, null);

const fetchUsersStart = () => _actionStart(UserTypes.FETCH_USERS_START);

const fetchUsersSuccess = users => _fetchActionSuccess(UserTypes.FETCH_USERS_SUCCESS, 'users', users);

const fetchUsersFail = error => _actionFail(UserTypes.FETCH_USERS_FAIL, error, null);

const mutateUserStart = () => _actionStart(UserTypes.MUTATE_USER_START);

const mutateUserSuccess = message => _fetchActionSuccess(UserTypes.MUTATE_USER_SUCCESS, 'message', message);

const mutateUserFail = (error, fieldErrors) => _actionFail(UserTypes.MUTATE_USER_FAIL, error, fieldErrors);

const deleteUserStart = () => _actionStart(UserTypes.DELETE_USER_START);

const deleteUserSuccess = (message, email) => {
    return {
        type: UserTypes.DELETE_USER_SUCCESS,
        email: email,
        message: message
    }
}

const deleteUserFail = (error, fieldErrors) => _actionFail(UserTypes.DELETE_USER_FAIL, error, fieldErrors);

export const fetchUser = email => {
    return dispatch => {
        dispatch(fetchUserStart());
        axios.post(Urls.Users.getUserByEmailUrl(), {email: email}, {handled: true})
            .then(res => {
                console.log(res.data)
                dispatch(fetchUserSuccess(res.data))
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(fetchUserFail(err.response.data.message || err.response.data.error))
            })
    }
}

export const updateUser = (id, user) => {
    return dispatch => {
        dispatch(mutateUserStart());
        axios.put(Urls.Users.updateUserUrl(id), user, {handled: true})
            .then(res => {
                console.log(res.data);
                dispatch(mutateUserSuccess(res.data.message));
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(mutateUserFail(err.response.data.message || err.response.data.error, err.response.data.errors))
            })
    }
}

export const deleteUser = email => {
    return dispatch => {
        dispatch(deleteUserStart());
        axios.delete(Urls.Users.usersUrl(), {data: {email: email}, handled: true})
            .then(res => {
                console.log(res.data)
                dispatch(deleteUserSuccess(res.data.message, email));
                dispatch(logout());
            })
            .catch(err => {
                console.log(err.response.data.message)
                dispatch(deleteUserFail(err.response.data.message || err.response.data.error, null))
            })
    }
}

export const fetchUsers = () => {
    return dispatch => {
        dispatch(fetchUsersStart());
        axios.post(Urls.Users.usersUrl(), {},{handled: true})
            .then(res => {
                console.log(res.data);
                dispatch(fetchUsersSuccess(res.data));
            })
            .catch(err => {
                console.log(err.response.data.message);
                dispatch(fetchUsersFail(err.response.data.message));
            })
    }
}