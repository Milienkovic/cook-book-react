export default class UserTypes {
    static FETCH_USER_START='FETCH_USER_START';
    static FETCH_USER_SUCCESS='FETCH_USER_SUCCESS';
    static FETCH_USER_FAIL='FETCH_USER_FAIL';
    static FETCH_USERS_START='FETCH_USERS_START';
    static FETCH_USERS_SUCCESS='FETCH_USERS_SUCCESS';
    static FETCH_USERS_FAIL='FETCH_USERS_FAIL';
    static MUTATE_USER_START='MUTATE_USER_START';
    static MUTATE_USER_SUCCESS='MUTATE_USER_SUCCESS';
    static MUTATE_USER_FAIL='MUTATE_USER_FAIL';
    static DELETE_USER_START='DELETE_USER_START';
    static DELETE_USER_SUCCESS='DELETE_USER_SUCCESS';
    static DELETE_USER_FAIL='DELETE_USER_FAIL';
}