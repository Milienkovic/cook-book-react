import CategoryTypes from "./categoryTypes";
import axios from './../../../utils/axios-instance';
import {fileUpload} from "../general/generalActions";
import Urls from "../../../utils/urls/urls";
import Params from "../../../utils/urls/params";
import AppUrls from "../../../utils/urls/appUrls";


const _actionStart = type => {
    return {
        type: type,
    }
}
const _actionFail = (type, error, fieldErrors) => {
    return {
        type: type,
        error: error,
        fieldErrors: fieldErrors,
    }
}

const _fetchSuccessAction = (type, propName, propValue) => {
    return {
        type: type,
        [propName]: propValue,
    }
}

const _deleteCategoryAction = (type, propName, propValue) => {
    return {
        type: type,
        [propName]: propValue,
    }
}
const fetchCategoryStart = () => _actionStart(CategoryTypes.FETCH_CATEGORY_START);

const fetchCategoryFail = (error) => _actionFail(CategoryTypes.FETCH_CATEGORY_FAIL, error, null);

const fetchCategorySuccess = (category) => _fetchSuccessAction(CategoryTypes.FETCH_CATEGORY_SUCCESS, 'category', category);

export const fetchCategory = id => {
    return dispatch => {
        dispatch(fetchCategoryStart());
        axios.get(Urls.Categories.categoryUrl(id))
            .then(res => {
                dispatch(fetchCategorySuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchCategoryFail(err.response.data.message))
            });
    }
};

const mutateCategoryStart = () => _actionStart(CategoryTypes.MUTATE_CATEGORY_START);

const mutateCategoryFail = (error, fieldErrors) => _actionFail(CategoryTypes.MUTATE_CATEGORY_FAIL, error, fieldErrors);

const mutateCategorySuccess = (type, category) => _fetchSuccessAction(type, 'category', category);

const deleteCategorySuccess = (id) => _deleteCategoryAction(CategoryTypes.DELETE_CATEGORY_SUCCESS, 'id', id);

const deleteCategoriesSuccess = indices => _deleteCategoryAction(CategoryTypes.DELETE_CATEGORIES_SUCCESS, 'indices', indices);

const clearCategory = () => _actionStart(CategoryTypes.CLEAR_CATEGORY);

export const categoryManipulated = () => _actionStart(CategoryTypes.CATEGORY_MANIPULATED);

export const addCategory = (category, history) => {
    return dispatch => {
        dispatch(mutateCategoryStart());
        const {name, description, image} = category;
        axios.post(Urls.Categories.categoriesUrl(), {name: name, description: description}, {handled: true})
            .then(response => {
                const {location} = response.headers;
                const id = Params.getParamFromUrl(location, 'id');
                dispatch(mutateCategorySuccess(CategoryTypes.CREATE_CATEGORY_SUCCESS, response.data));
                if (image) {
                    let formData = new FormData();
                    formData.append('file', image)
                    dispatch(fileUpload(formData, Urls.Categories.uploadCategoryImageUrl(id), history, AppUrls.CATEGORIES_BOARD));
                } else {
                    history.push(AppUrls.CATEGORIES_BOARD);
                }
            })
            .catch(err => {
                dispatch(mutateCategoryFail(err.response.data.message ? err.response.data.message : err.response.data.error, err.response.data.errors))
            });
    }
};

export const updateCategory = (id, category, history) => {
    return dispatch => {
        dispatch(mutateCategoryStart());
        const {name, description, image} = category;
        axios.put(Urls.Categories.categoryUrl(id), {name: name, description: description}, {handled: true})
            .then(response => {
                dispatch(mutateCategorySuccess(CategoryTypes.UPDATE_CATEGORY_SUCCESS, response.data));
                if (image) {
                    let formData = new FormData();
                    formData.append('file', image);
                    dispatch(fileUpload(formData, Urls.Categories.uploadCategoryImageUrl(id), history, AppUrls.CATEGORIES_BOARD));
                } else {
                    history.push(AppUrls.CATEGORIES_BOARD);
                }
            })
            .catch(err => {
                dispatch(mutateCategoryFail(err.response.data.message ? err.response.data.message : err.response.data.error, err.response.data.errors))
            });

    }
};

export const deleteCategory = id => {
    return dispatch => {
        dispatch(mutateCategoryStart());
        axios.delete(Urls.Categories.categoryUrl(id))
            .then(_ => {
                dispatch(deleteCategorySuccess(id));
            })
            .catch(err => {
                mutateCategoryFail(err.response.data, undefined);
            });
    }
};

const fetchCategoriesStart = () => _actionStart(CategoryTypes.FETCH_CATEGORIES_START);

const fetchCategoriesSuccess = (categories) => _fetchSuccessAction(CategoryTypes.FETCH_CATEGORIES_SUCCESS, 'categories', categories);

const fetchCategoriesFail = (error) => _actionFail(CategoryTypes.FETCH_CATEGORIES_FAIL, error, null);

export const fetchCategories = () => {
    return dispatch => {
        dispatch(fetchCategoriesStart());
        axios.get(Urls.Categories.categoriesUrl())
            .then(response => {
                dispatch(fetchCategoriesSuccess(response.data));
            })
            .catch(err => {
                dispatch(fetchCategoriesFail(err.response.data))
            });
    }
};

export const deleteCategories = indices => {
    return dispatch => {
        axios.delete(Urls.Categories.deleteCategoriesUrl(), {'data': indices, handled: true})
            .then(_ => {
                dispatch(deleteCategoriesSuccess(indices));
            })
            .catch(err => {
                dispatch(mutateCategoryFail(err.response.data, null))
            });
    }
};

export const purgeCategory = () => {
    return dispatch => {
        dispatch(clearCategory());
    }
}