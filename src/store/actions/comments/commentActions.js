import axios from '../../../utils/axios-instance';
import Urls from "../../../utils/urls/urls";
import {CommentTypes} from "./commentTypes";

const actionStart = type => {
    return {
        type
    }
}

const actionSuccess = (type, propName, propValue) => {
    return {
        type,
        [propName]: propValue
    }
}

const actionFail = (type, error, fieldErrors = null) => {
    return {
        type,
        error,
        fieldErrors
    }
}

const fetchCommentStart = () => actionStart(CommentTypes.FETCH_COMMENT_START);
const fetchCommentSuccess = comment => actionSuccess(CommentTypes.FETCH_COMMENT_SUCCESS, 'comment', comment);
const fetchCommentFail = (error) => actionFail(CommentTypes.FETCH_COMMENT_FAIL, error);

const fetchRecipeCommentsStart = () => actionStart(CommentTypes.FETCH_COMMENTS_START);
const fetchRecipeCommentsSuccess = comments => actionSuccess(CommentTypes.FETCH_COMMENTS_SUCCESS, 'comments', comments)
const fetchRecipeCommentsFail = error => actionFail(CommentTypes.FETCH_COMMENTS_FAIL, error);

const mutateRecipeCommentStart = () => actionStart(CommentTypes.MUTATE_COMMENT_START);
const mutateRecipeCommentSuccess = (type, message) => actionSuccess(type, 'message', message);
const deleteRecipeCommentSuccess = (commentId) => actionSuccess(CommentTypes.DELETE_COMMENT_SUCCESS, 'id', commentId);
const mutateRecipeCommentFail = (error, fieldErrors) => actionFail(CommentTypes.MUTATE_COMMENT_FAIL, error, fieldErrors);
const likeCommentSuccess = message => actionSuccess(CommentTypes.LIKE_COMMENT_SUCCESS, 'message', message);
const replyCommentSuccess = message => actionSuccess(CommentTypes.REPLY_COMMENT_SUCCESS, 'message', message);

export const fetchComment = commentId => {
    return dispatch => {
        dispatch(fetchCommentStart());
        axios.get(Urls.Comments.getComment(commentId))
            .then(res => {
                dispatch(fetchCommentSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchCommentFail(err.response.data.message || 'Something went wrong!'));
            });
    }
}

export const fetchRecipeComments = recipeId => {
    return dispatch => {
        dispatch(fetchRecipeCommentsStart());
        axios.get(Urls.Comments.getRecipeComments(recipeId))
            .then(res => {
                dispatch(fetchRecipeCommentsSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchRecipeCommentsFail(err.response.data.message || 'Something went wrong'));
            });
    }
}

export const addComment = (recipeId, comment) => {
    return dispatch => {
        dispatch(mutateRecipeCommentStart());
        axios.post(Urls.Comments.commentRecipe(recipeId), comment)
            .then(res => {
                dispatch(mutateRecipeCommentSuccess(CommentTypes.CREATE_COMMENT_SUCCESS, res.data));
            })
            .catch(err => {
                dispatch(mutateRecipeCommentFail(err.response.data.message || 'Something went wrong!', err.response.data.errors));
            });
    }
}

export const updateComment = (commentId, comment) => {
    return dispatch => {
        dispatch(mutateRecipeCommentStart());
        axios.put(Urls.Comments.updateComment(commentId), comment, {handled: true})
            .then(res => {
                dispatch(mutateRecipeCommentSuccess(CommentTypes.UPDATE_COMMENT_SUCCESS, res.data));
            })
            .catch(err => {
                dispatch(mutateRecipeCommentFail(err.response.data.message || 'Something went wrong', err.response.data.errors));
            });
    }
}

export const deleteComment = (commentId) => {
    return dispatch => {
        dispatch(mutateRecipeCommentStart());
        axios.delete(Urls.Comments.deleteComment(commentId), {handled: true})
            .then(res => {
                dispatch(deleteRecipeCommentSuccess(commentId));
            })
            .catch(err => {
                dispatch(mutateRecipeCommentFail(err.response.data.message || 'Something went wrong', err.response.data.errors));
            });
    }
}

export const likeComment = (commentId, like) => {
    return dispatch => {
        dispatch(mutateRecipeCommentStart());
        axios.put(Urls.Comments.likeComment(commentId), like)
            .then(res => {
                dispatch(likeCommentSuccess(res.data));
            })
            .catch(err => {
                dispatch(mutateRecipeCommentFail(err.response.data.message || 'Something went wrong', err.response.data.errors));
            })
    }
}

export const replyComment = (commentId, reply) => {
    return dispatch => {
        dispatch(mutateRecipeCommentStart());
        axios.post(Urls.Comments.replyToComment(commentId), reply)
            .then(res => {
                dispatch(replyCommentSuccess(res.data));
            })
            .catch(err => {
                dispatch(mutateRecipeCommentFail(err.response.data.message || 'Something went wrong', err.response.data.errors));
            });
    }
}