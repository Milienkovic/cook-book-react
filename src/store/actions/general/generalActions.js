import GeneralTypes from "./generalTypes";
import axios from './../../../utils/axios-instance';

export const purgeErrors = () => {
    return {
        type: GeneralTypes.PURGE_ERRORS,
    }
};

const fileUploadStart = () => {
    return {
        type: GeneralTypes.FILE_UPLOAD_START,
    }
}

const fileUploadSuccess = (message) => {
    return {
        type: GeneralTypes.FILE_UPLOAD_SUCCESS,
        message: message,
    }
}

const fileUploadProgress = percentage => {
    return {
        type: GeneralTypes.FILE_UPLOAD_PROGRESS,
        uploadProgress: percentage,
    }
}

const fileUploadFail = (error, fieldErrors) => {
    return {
        type: GeneralTypes.FILE_UPLOAD_FAIL,
        error: error,
    }
}

const _calculatePercentage = progressEvent => {
    return (progressEvent.loaded / progressEvent.total) * 100
}

export const fileUpload = (file, url, history, redirectUrl) => {
    console.log(history)
    console.log(redirectUrl)
    return dispatch => {
        dispatch(fileUploadStart());
        axios.post(url, file, {
            handled: true, onUploadProgress: progressEvent => {
                console.log(progressEvent.loaded / progressEvent.total * 100 + "%");
                dispatch(fileUploadProgress(_calculatePercentage(progressEvent)));
            }
        })
            .then(response => {
                console.log(response.data);
                dispatch(fileUploadSuccess(`Image ${response.data.fileName} has been uploaded`));
                if (history)
                    history.push(redirectUrl);
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(fileUploadFail(err.response.data.message, err.response.data.errors))
            });
    }
}