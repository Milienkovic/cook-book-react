import axios from './../../../../utils/axios-instance';
import IngredientTypes from "./ingredientTypes";
import Urls from "../../../../utils/urls/urls";

const actionStart = type => {
    return {
        type
    }
}

const actionSuccess = (type, propName, propValue) => {
    return {
        type,
        [propName]: propValue
    }
}

const actionFail = (type, error, fieldErrors) => {
    return {
        type,
        error,
        fieldErrors
    }
}

const fetchIngredientStart = () => actionStart(IngredientTypes.FETCH_INGREDIENT_START);
const fetchIngredientSuccess = ingredient => actionSuccess(IngredientTypes.FETCH_INGREDIENT_SUCCESS, 'ingredient', ingredient);
const fetchIngredientFail = error => actionFail(IngredientTypes.FETCH_INGREDIENT_FAIL, error, null);

const fetchRecipeIngredientsStart = () => actionStart(IngredientTypes.FETCH_RECIPE_INGREDIENTS_START);
const fetchRecipeIngredientsSuccess = ingredients => actionSuccess(IngredientTypes.FETCH_RECIPE_INGREDIENTS_SUCCESS, 'ingredients', ingredients);
const fetchRecipeIngredientsFail = error => actionFail(IngredientTypes.FETCH_RECIPE_INGREDIENTS_FAIL, error, null);

const mutateIngredientStart = () => actionStart(IngredientTypes.MUTATE_INGREDIENT_START);
const mutateIngredientSuccess = (type, ingredient) => actionSuccess(type, 'ingredient', ingredient);
const deleteIngredientSuccess = id => actionSuccess('id', id);
const mutateIngredientFail = (error, fieldErrors) => actionFail(IngredientTypes.MUTATE_INGREDIENT_FAIL, error, fieldErrors);

const createIngredientsSuccess = message => actionSuccess(IngredientTypes.CREATE_INGREDIENTS_SUCCESS, 'message', message);
const updateIngredientsSuccess = message => actionSuccess(IngredientTypes.UPDATE_INGREDIENTS_SUCCESS, 'message', message);
const deleteIngredientsSuccess = indices => actionSuccess(IngredientTypes.DELETE_INGREDIENTS_SUCCESS, 'indices', indices);

export const fetchIngredient = id => {
    return dispatch => {
        dispatch(fetchIngredientStart());
        axios.get(Urls.Ingredients.getIngredient(id))
            .then(res => {
                console.log(res.data);
                dispatch(fetchIngredientSuccess(res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(fetchIngredientFail(err.response.data.message || 'Something went wrong. Please try again!'));
            });
    }
}

export const fetchRecipeIngredients = recipeId => {
    return dispatch => {
        dispatch(fetchRecipeIngredientsStart());
        axios.get(Urls.Ingredients.getIngredientsByRecipe(recipeId))
            .then(res => {
                console.log(res.data);
                dispatch(fetchRecipeIngredientsSuccess(res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(fetchRecipeIngredientsFail(err.response.data.message || 'Something went wrong. Please try again!'));
            });
    }
}

export const addIngredient = (recipeId, ingredient) => {
    return dispatch => {
        dispatch(mutateIngredientStart());
        axios.post(Urls.Ingredients.createIngredient(recipeId), ingredient)
            .then(res => {
                console.log(res.data);
                dispatch(mutateIngredientSuccess(IngredientTypes.CREATE_INGREDIENT_SUCCESS, res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(mutateIngredientFail(err.response.data.message, err.response.data.errors));
            })
    }
}

export const addIngredients = (recipeId, ingredients) => {
    return dispatch => {
        dispatch(mutateIngredientStart());
        axios.post(Urls.Ingredients.createIngredients(recipeId), ingredients, {handled: true})
            .then(res => {
                console.log(res.data);
                dispatch(createIngredientsSuccess(res.data));
            })
            .catch(err => {
                console.log(err.response);
                dispatch(mutateIngredientFail(err.response.data.message || 'Something went wrong!', err.response.data.errors));
            });
    }
}

export const updateIngredient = (id, ingredient) => {
    return dispatch => {
        dispatch(mutateIngredientStart());
        axios.put(Urls.Ingredients.updateIngredient(id), ingredient)
            .then(res => {
                console.log(res.data);
                dispatch(mutateIngredientSuccess(IngredientTypes.UPDATE_INGREDIENT_SUCCESS, res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(mutateIngredientFail(err.response.data.message || 'Something went wrong. Please try again!', err.response.data.errors));
            });
    }

}

export const updateIngredients = (recipeId, ingredients) => {
    return dispatch => {
        dispatch(mutateIngredientStart());
        axios.put(Urls.Ingredients.updateIngredients(recipeId), ingredients, {handled: true})
            .then(res => {
                dispatch(updateIngredientsSuccess(res.data));
            })
            .catch(err => {
                dispatch(mutateIngredientFail(err.response.data.message || 'Something went wrong', err.response.data.errors));
            });
    }
}

export const deleteIngredient = id => {
    return dispatch => {
        dispatch(mutateIngredientStart());
        axios.delete(Urls.Ingredients.deleteIngredient(id))
            .then(res => {
                console.log(res.data);
                dispatch(deleteIngredientSuccess(id));
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(mutateIngredientFail(err.response.data.message || 'Something went wrong. Please try again!', null))
            })
    }
}

export const deleteIngredients = (recipeId, indices) => {
    return dispatch => {
        dispatch(mutateIngredientStart());
        axios.delete(Urls.Ingredients.deleteIngredients(recipeId), {data: indices, handled: true})
            .then(res => {
                console.log(res.data);
                dispatch(deleteIngredientsSuccess(indices));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(mutateIngredientFail(err.response.data.message || 'Something went wrong!'));
            });
    }
}