/* eslint-disable no-unused-vars */
import RecipeTypes from "./recipeTypes";
import axios from './../../../utils/axios-instance';
import Urls from "../../../utils/urls/urls";
import {fileUpload} from "../general/generalActions";
import AppUrls from "../../../utils/urls/appUrls";
import Params from "../../../utils/urls/params";
import App from "../../../App";

const actionStart = type => {
    return {
        type
    }
}

const actionFail = (type, error, fieldErrors) => {
    return {
        type,
        error,
        fieldErrors
    }
}

const actionSuccess = (type, propName, propValue) => {
    return {
        type,
        [propName]: propValue
    }
}

const fetchRecipeStart = () => actionStart(RecipeTypes.FETCH_RECIPE_START);
const fetchRecipeSuccess = recipe => actionSuccess(RecipeTypes.FETCH_RECIPE_SUCCESS, 'recipe', recipe);
const fetchRecipePreviewSuccess = recipe => actionSuccess(RecipeTypes.FETCH_RECIPE_PREVIEW_SUCCESS, 'recipe', recipe);
const fetchRecipeFail = (error) => actionFail(RecipeTypes.FETCH_RECIPE_FAIL, error, null);

const mutateRecipeStart = () => actionStart(RecipeTypes.MUTATE_RECIPE_START);
const mutateRecipeSuccess = (type, recipe) => actionSuccess(type, 'data', recipe)
const mutateRecipeFail = (error, fieldErrors) => actionFail(RecipeTypes.MUTATE_RECIPE_FAIL, error, fieldErrors);

const deleteRecipeSuccess = id => actionSuccess(RecipeTypes.DELETE_RECIPE_SUCCESS, 'deleted', id);

const fetchUserRecipesStart = () => actionStart(RecipeTypes.FETCH_USER_RECIPES_START);
const fetchUserRecipesSuccess = recipes => actionSuccess(RecipeTypes.FETCH_USER_RECIPES_SUCCESS, 'recipes', recipes);
const fetchUserRecipesFail = error => actionFail(RecipeTypes.FETCH_USER_RECIPES_FAIL, error, null);

const fetchUserRecipesByCategoryStart = () => actionStart(RecipeTypes.FETCH_USER_RECIPES_BY_CATEGORY_START);
const fetchUserRecipesByCategorySuccess = recipes => actionSuccess(RecipeTypes.FETCH_USER_RECIPES_BY_CATEGORY_SUCCESS, 'recipes', recipes);
const fetchUserRecipesByCategoryFail = error => actionFail(RecipeTypes.FETCH_USER_RECIPES_BY_CATEGORY_FAIL, error, null);

const fetchCategoryRecipesStart = () => actionStart(RecipeTypes.FETCH_CATEGORY_RECIPES_START);
const fetchCategoryRecipesSuccess = recipes => actionSuccess(RecipeTypes.FETCH_CATEGORY_RECIPES_SUCCESS, 'recipes', recipes);
const fetchCategoryRecipesFail = error => actionFail(RecipeTypes.FETCH_CATEGORY_RECIPES_FAIL, error, null);

const fetchCategoryRecipesByComplexityStart = () => actionStart(RecipeTypes.FETCH_CATEGORY_RECIPES_BY_COMPLEXITY_START);
const fetchCategoryRecipesByComplexitySuccess = recipes => actionSuccess(RecipeTypes.FETCH_CATEGORY_RECIPES_BY_COMPLEXITY_SUCCESS, 'recipes', recipes);
const fetchCategoryRecipesByComplexityFail = error => actionFail(RecipeTypes.FETCH_CATEGORY_RECIPES_BY_COMPLEXITY_FAIL, error, null);

const rateRecipeStart = () => actionStart(RecipeTypes.RATE_RECIPE_START);
const rateRecipeSuccess = message => actionSuccess(RecipeTypes.RATE_RECIPE_SUCCESS, 'message', message);
const rateRecipeFail = error => actionFail(RecipeTypes.RATE_RECIPE_FAIL, error, null);

const purgeRecipe = () => actionStart(RecipeTypes.CLEAR_RECIPE);

export const fetchRecipe = (id, isPreview = false) => {
    return dispatch => {
        const url = isPreview ? Urls.Recipes.getRecipePreview(id) : Urls.Recipes.getRecipe(id);
        dispatch(fetchRecipeStart());
        axios.get(url)
            .then(res => {
                console.log(res);
                if (isPreview) {
                    dispatch(fetchRecipePreviewSuccess(res.data));
                } else {
                    dispatch(fetchRecipeSuccess(res.data));
                }
            })
            .catch(err => {
                dispatch(fetchRecipeFail(err.response.data.message || 'Something went wrong!'))
            })
    }
}


export const addRecipe = (categoryId, recipe, history) => {
    const {name, complexity, image, visible, ingredients, steps} = recipe;
    const recipeObject = {name, complexity, visible, ingredients, steps};

    return dispatch => {
        dispatch(mutateRecipeStart());
        axios.post(Urls.Recipes.createRecipe(categoryId), recipeObject, {handled: true})
            .then(res => {
                const {location} = res.headers;
                const recipeId = Params.getParamFromUrl(location, 'id');
                console.log(`recipeId:${recipeId}`)
                console.log(res.data)
                dispatch(mutateRecipeSuccess(RecipeTypes.CREATE_RECIPE_SUCCESS, res.data));

                if (image) {
                    let formData = new FormData();
                    formData.append('file', image);
                    dispatch(fileUpload(formData, Urls.Recipes.uploadRecipeImageUrl(recipeId), history, AppUrls.HOME));
                } else {
                    history.push(AppUrls.HOME);
                }
            })
            .catch(err => {
                dispatch(mutateRecipeFail(err.response.data.message || 'Something went wrong!', err.response.data.errors));
            })
    }
}


export const updateRecipe = (id, recipe, history) => {
    console.log('recipe');
    console.log(recipe);
    const {name, complexity, image, visible, ingredients, steps} = recipe;
    const recipeObject = {name, complexity, visible, ingredients, steps};
    return dispatch => {
        dispatch(mutateRecipeStart());
        axios.put(Urls.Recipes.updateRecipe(id), recipeObject, {handled: true})
            .then(res => {
                console.log(res.data);
                dispatch(mutateRecipeSuccess(RecipeTypes.UPDATE_RECIPE_SUCCESS, res.data));
                if (image) {
                    let formData = new FormData();
                    formData.append('file', image);
                    dispatch(fileUpload(formData, Urls.Recipes.uploadRecipeImageUrl(id), history, AppUrls.USER_RECIPES));
                } else {
                    history.push(AppUrls.USER_RECIPES);
                }
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(mutateRecipeFail(err.response.data.message || 'Something went wrong!', err.response.data.errors));
            })
    }
}

export const deleteRecipe = id => {
    return dispatch => {
        dispatch(mutateRecipeStart());
        axios.delete(Urls.Recipes.deleteRecipe(id), {handled: true})
            .then(res => {
                console.log(res.data);
                dispatch(deleteRecipeSuccess(id));
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(mutateRecipeFail(err.response.data.message || 'Something went wrong!', err.response.data.errors));
            })
    }
}

export const fetchUserRecipes = () => {
    return dispatch => {
        dispatch(fetchUserRecipesStart());
        axios.get(Urls.Recipes.getUserRecipes(), {handled: true})
            .then(res => {
                console.log(res.data);
                dispatch(fetchUserRecipesSuccess(res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(fetchUserRecipesFail(err.response.data.message));
            })
    }
}

export const fetchUserRecipesByCategory = (categoryId) => {
    //todo
}

export const fetchCategoryRecipes = categoryId => {
    return dispatch => {
        dispatch(fetchCategoryRecipesStart());
        axios.get(Urls.Recipes.getCategoryRecipes(categoryId),)
            .then(res => {
                console.log(res.data);
                dispatch(fetchCategoryRecipesSuccess(res.data));
            })
            .catch(err => {
                console.log(err);
                dispatch(fetchCategoryRecipesFail(err.response.data.message || 'Something went wrong!'));
            })
    }
}

export const fetchCategoryRecipesByComplexity = (categoryId, complexity) => {
    return dispatch => {
        dispatch(fetchCategoryRecipesByComplexityStart());
        axios.get(Urls.Recipes.getCategoryRecipesByComplexity(categoryId, complexity))
            .then(res => {
                console.log(res.data);
                dispatch(fetchCategoryRecipesByComplexitySuccess(res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(fetchCategoryRecipesByComplexityFail(err.response.data.message || 'Something went wrong!'));
            });
    }
}

export const rateRecipe = (recipeId, rating, history, redirectUrl) => {
    return dispatch => {
        dispatch(rateRecipeStart());
        axios.put(Urls.Recipes.rateRecipe(recipeId), {rated: rating})
            .then(res => {
                console.log(res.data);
                dispatch(rateRecipeSuccess(res.data.message));
                history.push(redirectUrl);
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(rateRecipeFail(err.response.data.message || 'Something went wrong'));
            })
    }
}

export const clearRecipe = () => {
    return dispatch => {
        dispatch(purgeRecipe());
    }
}