import StepTypes from "./stepTypes";
import axios from './../../../../utils/axios-instance';
import Urls from "../../../../utils/urls/urls";

const actionStart = type => {
    return {
        type: type
    }
}

const actionSuccess = (type, propName, propValue) => {
    return {
        type,
        [propName]: propValue
    }
}

const actionFail = (type, error, fieldErrors) => {
    return {
        type,
        error,
        fieldErrors
    }
}

const fetchStepStart = () => actionStart(StepTypes.FETCH_STEP_START);
const fetchStepSuccess = step => actionSuccess(StepTypes.FETCH_RECIPE_STEPS_SUCCESS, 'step', step);
const fetchStepFail = (error) => actionFail(StepTypes.FETCH_STEP_FAIL, error);

const fetchRecipeStepsStart = () => actionStart(StepTypes.FETCH_RECIPE_STEPS_START);
const fetchRecipeStepsSuccess = steps => actionSuccess(StepTypes.FETCH_RECIPE_STEPS_SUCCESS, 'steps', steps);
const fetchRecipeStepsFail = (error) => actionFail(StepTypes.FETCH_RECIPE_STEPS_FAIL, error, null);

const mutateStepStart = () => actionStart(StepTypes.MUTATE_STEP_START);
const mutateStepSuccess = (type, step) => actionSuccess(type, 'step', step);
const deleteStepSuccess = id => actionSuccess(StepTypes.DELETE_STEP_SUCCESS, 'id', id);
const mutateStepFail = (error, fieldErrors) => actionFail(StepTypes.MUTATE_STEP_FAIL, error, fieldErrors);

const createStepsSuccess = message => actionSuccess(StepTypes.CREATE_STEPS_SUCCESS, 'message', message);
const updateStepsSuccess = message => actionSuccess(StepTypes.UPDATE_STEPS_SUCCESS, 'message', message);
const deleteStepsSuccess = (type, indices) => actionSuccess(type, 'indices', indices);

export const fetchStep = id => {
    return dispatch => {
        dispatch(fetchStepStart());
        axios.get(Urls.Steps.getStep(id))
            .then(res => {
                console.log(res.data)
                dispatch(fetchStepSuccess(res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(fetchStepFail(err.response.data.message));
            });
    }
}

export const fetchRecipeSteps = recipeId => {
    return dispatch => {
        dispatch(fetchRecipeStepsStart());
        axios.get(Urls.Steps.getStepsByRecipe(recipeId))
            .then(res => {
                console.log(res.data);
                dispatch(fetchRecipeStepsSuccess(res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(fetchRecipeStepsFail(err.response.data.message))
            });
    }
}

export const addStep = (recipeId, step) => {
    return dispatch => {
        dispatch(mutateStepStart());
        axios.post(Urls.Steps.createStep(recipeId), step)
            .then(res => {
                console.log(res.data);
                dispatch(mutateStepSuccess(StepTypes.CREATE_STEP_SUCCESS, res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(mutateStepFail(err.response.data.message, err.response.data.errors));
            });
    }
}

export const addSteps = (recipeId, steps) => {
    return dispatch => {
        dispatch(mutateStepStart());
        axios.post(Urls.Steps.createSteps(recipeId), steps, {handled: true})
            .then(res => {
                console.log(res.data);
                dispatch(createStepsSuccess(res.data));
            })
            .catch(err => {
                console.log(err.response);
                dispatch(mutateStepFail(err.response.data.message || 'Something went wrong!', err.response.data.errors));
            });
    }
}

export const updateStep = (id, step) => {
    return dispatch => {
        dispatch(mutateStepStart());
        axios.put(Urls.Steps.updateStep(id), step)
            .then(res => {
                console.log(res.data);
                dispatch(mutateStepSuccess(StepTypes.UPDATE_STEP_SUCCESS, res.data));
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(mutateStepFail(err.response.data.message, err.response.data.errors));
            });

    }
}

export const updateSteps = (recipeId, steps) => {
    return dispatch => {
        dispatch(mutateStepStart());
        axios.put(Urls.Steps.updateSteps(recipeId), steps, {handled: true})
            .then(res => {
                dispatch(updateStepsSuccess(res.data));
            })
            .catch(err => {
                dispatch(mutateStepFail(err.response.data.message || 'Something went wrong!', err.response.data.errors));
            })
    }
}

export const deleteStep = id => {
    return dispatch => {
        dispatch(mutateStepStart());
        axios.delete(Urls.Steps.deleteStep(id))
            .then(res => {
                console.log(res.data);
                dispatch(deleteStepSuccess(id));
            })
            .catch(err => {
                dispatch(mutateStepFail(err.response.data.message));
            });
    }
}

export const deleteSteps = (recipeId, indices) => {
    return dispatch => {
        dispatch(mutateStepStart());
        axios.delete(Urls.Steps.deleteSteps(recipeId, {data: indices, handled: true}))
            .then(res => {
                console.log(res.data);
                dispatch(deleteStepsSuccess(StepTypes.DELETE_STEPS_SUCCESS, indices));
            })
            .catch(err => {
                console.log(err.response.data.message);
                dispatch(mutateStepFail(err.response.data.message || 'Something went wrong!'));
            })
    }
}
