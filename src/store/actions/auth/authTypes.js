
export default class AuthTypes {
    static AUTH_START = 'AUTH_START';
    static AUTH_SUCCESS = 'AUTH_SUCCESS';
    static AUTH_FAIL = 'AUTH_FAIL';

    static EXCHANGE_JWT_TOKEN = 'EXCHANGE_JWT_TOKEN';

    static REGISTRATION_START = 'REGISTRATION_START';
    static REGISTRATION_SUCCESS = 'REGISTRATION_SUCCESS';
    static REGISTRATION_FAIL = 'REGISTRATION_FAIL';

    static REQUEST_NEW_VERIFICATION_TOKEN_START = 'REQUEST_NEW_VERIFICATION_TOKEN_START';
    static REQUEST_NEW_VERIFICATION_TOKEN_SUCCESS = 'REQUEST_NEW_VERIFICATION_TOKEN_SUCCESS';
    static REQUEST_NEW_VERIFICATION_TOKEN_FAIL = 'REQUEST_NEW_VERIFICATION_TOKEN_FAIL';

    static VERIFY_ACCOUNT_START = 'VERIFY_ACCOUNT_START';
    static VERIFY_ACCOUNT_SUCCESS = 'VERIFY_ACCOUNT_SUCCESS';
    static VERIFY_ACCOUNT_FAIL = 'VERIFY_ACCOUNT_FAIL';

    static REQUEST_NEW_PASSWORD_TOKEN_START = 'REQUEST_NEW_PASSWORD_TOKEN_START';
    static REQUEST_NEW_PASSWORD_TOKEN_SUCCESS = 'REQUEST_NEW_PASSWORD_TOKEN_SUCCESS';
    static REQUEST_NEW_PASSWORD_TOKEN_FAIL = 'REQUEST_NEW_PASSWORD_TOKEN_FAIL';

    static UPDATE_PASSWORD_START = 'UPDATE_PASSWORD_START';
    static UPDATE_PASSWORD_SUCCESS = 'UPDATE_PASSWORD_SUCCESS';
    static UPDATE_PASSWORD_FAIL = 'UPDATE_PASSWORD_FAIL';

    static CHANGE_PASSWORD_REQUEST_START = 'CHANGE_PASSWORD_REQUEST_START';
    static CHANGE_PASSWORD_REQUEST_SUCCESS = 'CHANGE_PASSWORD_REQUEST_SUCCESS';
    static CHANGE_PASSWORD_REQUEST_FAIL = 'CHANGE_PASSWORD_REQUEST_FAIL';

    static AUTH_LOGOUT = 'AUTH_LOGOUT';

}
