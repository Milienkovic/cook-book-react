import AuthTypes from "./authTypes";
import axios from 'axios';
import Urls from "../../../utils/urls/urls";
import jwtDecode from 'jwt-decode';
import Tokens from "../../../utils/tokens";

const _actionStart = type => {
    return {
        type: type
    }
}

const _actionFail = (type, error, fieldErrors) => {
    return {
        type: type,
        error: error,
        fieldErrors: fieldErrors
    }
}

const _actionSuccessMessage = (type, message) => {
    return {
        type: type,
        message: message
    }
}

export const authStart = () => _actionStart(AuthTypes.AUTH_START)

export const authSuccess = (token, expiresIn, roles) => {
    return {
        type: AuthTypes.AUTH_SUCCESS,
        token: token,
        expiresIn: expiresIn,
        roles: roles
    }
};

export const authFail = (error, fieldErrors) => _actionFail(AuthTypes.AUTH_FAIL, error, fieldErrors);


export const registrationStart = () => _actionStart(AuthTypes.REGISTRATION_START);

export const registrationSuccess = (message) => _actionSuccessMessage(AuthTypes.REGISTRATION_SUCCESS, message);

export const registrationFail = (error, fieldErrors) => _actionFail(AuthTypes.REGISTRATION_FAIL, error, fieldErrors);

export const verifyAccountStart = () => _actionStart(AuthTypes.VERIFY_ACCOUNT_START);

export const verifyAccountSuccess = message => _actionSuccessMessage(AuthTypes.VERIFY_ACCOUNT_SUCCESS, message);

export const verifyAccountFail = error => _actionFail(AuthTypes.VERIFY_ACCOUNT_FAIL, error, null);

export const requestNewPasswordTokenStart = () => _actionStart(AuthTypes.REQUEST_NEW_PASSWORD_TOKEN_START);

export const requestNewPasswordTokenSuccess = message => _actionSuccessMessage(AuthTypes.REQUEST_NEW_PASSWORD_TOKEN_SUCCESS, message);

export const requestNewPasswordTokenFail = error => _actionFail(AuthTypes.REQUEST_NEW_PASSWORD_TOKEN_FAIL, error, null);

export const requestNewVerificationTokenStart = () => _actionStart(AuthTypes.REQUEST_NEW_VERIFICATION_TOKEN_START);

export const requestNewVerificationTokenSuccess = message => _actionSuccessMessage(AuthTypes.REQUEST_NEW_VERIFICATION_TOKEN_SUCCESS, message);

export const requestNewVerificationTokenFail = error => _actionFail(AuthTypes.REQUEST_NEW_VERIFICATION_TOKEN_FAIL, error, null);

export const changePasswordRequestStart = () => _actionStart(AuthTypes.CHANGE_PASSWORD_REQUEST_START)

export const changePasswordRequestSuccess = message => _actionSuccessMessage(AuthTypes.CHANGE_PASSWORD_REQUEST_SUCCESS, message);

export const changePasswordRequestFail = error => _actionFail(AuthTypes.CHANGE_PASSWORD_REQUEST_FAIL, error, null);

export const passwordUpdateStart = () => _actionStart(AuthTypes.UPDATE_PASSWORD_START);

export const passwordUpdateSuccess = message => _actionSuccessMessage(AuthTypes.UPDATE_PASSWORD_SUCCESS, message);

export const passwordUpdateFail = (error, fieldErrors) => _actionFail(AuthTypes.UPDATE_PASSWORD_FAIL, error, fieldErrors);

export const logout = () => {
    Tokens.clearTokens();
    return {
        type: AuthTypes.AUTH_LOGOUT
    }
};

const _onAuthSuccess = (response, dispatch) => {
    const {authorization} = response.headers;
    localStorage.setItem('authToken', authorization);
    const decodedToken = jwtDecode(authorization);
    const expiryDate = Tokens.calculateExpiryDate(decodedToken.exp * 1000);
    dispatch(authSuccess(authorization, expiryDate, decodedToken.roles));
};

export const auth = (email, password) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password
        };
        axios.post(Urls.Auth.loginUrl(), authData)
            .then(response => {
                _onAuthSuccess(response, dispatch);
            })
            .catch(err => {
                dispatch(authFail(_getAuthErrorMessage(err), err.response.data.errors));
            })
    }
};

export const exchangeAuthToken = (refreshToken) => {
    return dispatch => {
        axios.post(Urls.Auth.exchangeAuthTokenUrl(refreshToken))
            .then(response => {
                console.log(response.data);
                _onAuthSuccess(response, dispatch);
            })
            .catch(error => {
                console.log(error.response);
                dispatch(authFail(_getExchangeTokenError(error), error.response.data.errors));
                dispatch(logout());
            });
    }
};

export const exchangePassPrivilegeAuthToken = (refreshToken) => {
    return dispatch => {
        axios.post(Urls.Auth.exchangeChangePasswordPrivilegeTokenUrl(refreshToken))
            .then(response => {
                _onAuthSuccess(response, dispatch)
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(authFail(_getExchangeTokenError(err), err.response.data.errors));
                dispatch(logout());
            });
    }
}

const _getExchangeTokenError = error => {
    if (error.response.status === 404) {
        return "Invalid Token!"
    } else {
        return "Something went wrong!"
    }
}

export const registration = (userData) => {
    return dispatch => {
        dispatch(registrationStart());
        axios.post(Urls.Auth.registrationUrl(), userData)
            .then(res => {
                dispatch(registrationSuccess(res.data.message))
            })
            .catch(err => {
                dispatch(registrationFail(_getRegistrationErrorMessage(err), err.response.data.errors));
            });
    }
};

export const confirmRegistration = token => {
    return dispatch => {
        dispatch(verifyAccountStart());
        axios.post(Urls.Auth.confirmRegistrationUrl(token))
            .then(response => {
                console.log(response.data)
                dispatch(verifyAccountSuccess(response.data.message))
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(verifyAccountFail("Invalid Token!"));
            });
    }
}

const _getRegistrationErrorMessage = (err) => {
    const {status} = err.response;
    if (status === 409) {
        return "Email Taken";
    } else if (status >= 400 && status < 409) {
        return "Registration Failed"
    }
};

const _getAuthErrorMessage = (err) => {
    if (err.response.status === 404) {
        return "Not Found!"
    } else {
        return "Authentication Failed"
    }
};

export const requestNewVerificationToken = email => {
    return dispatch => {
        dispatch(requestNewVerificationTokenStart());
        axios.post(Urls.Auth.newRegistrationTokenUrl(email))
            .then(response => {
                console.log(response.data)
                dispatch(requestNewVerificationTokenSuccess(response.data.message))
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(requestNewVerificationTokenFail(err.response.data.message ? err.response.data.message : err.response.data.error));
            });
    }
}

export const requestNewPassword = (email) => {
    return dispatch => {
        dispatch(requestNewPasswordTokenStart());
        axios.post(Urls.Auth.resetPasswordTokenUrl(email))
            .then(response => {
                console.log(response.data)
                dispatch(requestNewPasswordTokenSuccess(response.data.message));
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(requestNewPasswordTokenFail(err.response.data.message ? err.response.data.message : err.response.data.error));
            });
    }
};

export const changePasswordRequest = (id, token) => {
    return dispatch => {
        dispatch(changePasswordRequestStart());
        axios.post(Urls.Auth.changePasswordUrl(id, token))
            .then(response => {
                dispatch(changePasswordRequestSuccess(response.data.message));
                _onAuthSuccess(response, dispatch)
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(changePasswordRequestFail(_getChangePasswordRequestError(err)));
            });
    }
}

const _getChangePasswordRequestError = error => {
    if (error.response.status === 404) {
        return "Token not found!";
    } else {
        return "Invalid Token!"
    }
}

export const updatePassword = (passwordData, passwordToken) => {
    return dispatch => {
        dispatch(passwordUpdateStart());
        const authToken = localStorage.getItem('authToken');
        const config = {
            headers: {Authorization: authToken}
        };

        axios.post(Urls.Auth.updatePasswordUrl(passwordToken), passwordData, config)
            .then(response => {
                console.log(response.data)
                dispatch(passwordUpdateSuccess(response.data.message));
            })
            .catch(err => {
                console.log(err.response.data)
                dispatch(passwordUpdateFail(_getPasswordUpdateError(err), err.response.data.errors));
            });
    }
}

const _getPasswordUpdateError = error => {
    if (error.response.status === 403) {
        return " Invalid Credentials. Please login again! ";
    }
    return error.response.data.message;
}